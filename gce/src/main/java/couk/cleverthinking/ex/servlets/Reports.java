/**
 * (c) CleverThinking Ltd, 2013
 */
package couk.cleverthinking.ex.servlets;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.Exitem;

@SuppressWarnings("serial")
/**
 * Servlet to run one of a number of reports against the paid exitems for a given date
 * 
 * @author r
 *
 */
public class Reports extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}
	
	/**
	 * parse, validate the params
	 * 
	 * 
	 * Create an array of matching exitems
	 * 
	 * Call an appropriate method for the selected report type
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/csv");
		resp.setHeader("Content-Disposition", "attachment; filename=\"filename.csv\"");

		
		String date = req.getParameter("date");
		String reportType = req.getParameter("rt");
		String orgId = req.getParameter("o");
		
		List<Exitem> items = ofy().load().type(Exitem.class).filter("orgnId ==", orgId).filter("dateP ==", date).list();

		if ("nominal".equals(reportType)) {
			doNominal(items, req, resp, orgId);
		}
		
		if ("reimburse".equals(reportType)) {
			doReimburse(items, req, resp, orgId);
		}

		if ("vat".equals(reportType)) {
			doVat(items, req, resp, orgId);
		}

	}

	/**
	 * not sure what this will do yet. debit bank, credit vat liability
	 * 
	 * @param items
	 * @param req
	 * @param resp
	 * @param orgId 
	 * @throws IOException 
	 */
	private void doVat(List<Exitem> items, HttpServletRequest req, HttpServletResponse resp, String orgId) throws IOException {
		BigDecimal totalNet = BigDecimal.ZERO
		, totalVat= BigDecimal.ZERO, totalAmount= BigDecimal.ZERO;
		Exitem item; 
		/*
		 * iterate items and accumulate net amount into nominal codes  
		 */
		for (int i = 0; i < items.size(); i++) {
			item = items.get(i);
			totalNet = totalNet.add(item.getNet());
			totalVat = totalVat.add(item.getVat());
			totalAmount = totalNet.add(item.getAmount());			
		}
		resp.getWriter().println("Total Net,"+totalNet);
		resp.getWriter().println("Total Vat,"+totalVat);
		resp.getWriter().println("Total Amount,"+totalAmount);	
	}

	/**
	 * Simple summary of items grouped by nominal code
	 * 
	 * @param items
	 * @param req
	 * @param resp
	 * @param orgId 
	 * @throws IOException 
	 */
	private void doNominal(List<Exitem> items, HttpServletRequest req, HttpServletResponse resp, String orgId) throws IOException {
		HashMap<String, BigDecimal> nominals = new HashMap<String, BigDecimal>();
		
		Exitem item;
		BigDecimal runningTotal;
		
		/*
		 * iterate items and accumulate net amount into nominal codes  
		 */
		for (int i = 0; i < items.size(); i++) {
			item = items.get(i);
			runningTotal = nominals.get(item.getNom());
			if (runningTotal == null) {
				runningTotal = item.getNet();
			} else {
				runningTotal = item.getNet().add(runningTotal);
			}
			nominals.put(item.getNom(), runningTotal);
		}
		
		/*
		 * iterate accumulated nominals and display as csv
		 */
		Set<String> keys = nominals.keySet();
		Iterator<String> it = keys.iterator();
		
		String nom;
		while (it.hasNext()) {
			nom = it.next();
			resp.getWriter().println(nom+","+nominals.get(nom));
		}
	}

	
	/**
	 * Simple summary of item amount grouped by employee
	 * 
	 * @param items
	 * @param req
	 * @param resp
	 * @throws IOException 
	 */
	private void doReimburse(List<Exitem> items, HttpServletRequest req, HttpServletResponse resp, String orgId) throws IOException {

			/* get all the employees to do a lookup for bank details */
			List<Emp> emps = ofy().load().type(Emp.class).filter("orgnId ==", orgId).list();
			/*
			 * iterate emps and store in map  
			 */
			HashMap<String, Emp> employeeMap = new HashMap<String, Emp>();
			for (int i = 0; i < emps.size(); i++) {
				employeeMap.put(emps.get(i).getEmail(), emps.get(i));
			}

		
		
			HashMap<String, BigDecimal> employees = new HashMap<String, BigDecimal>();
			
			Exitem item;
			BigDecimal runningTotal;
			
			/*
			 * iterate items and accumulate amount into employee email  
			 */
			for (int i = 0; i < items.size(); i++) {
				item = items.get(i);
				runningTotal = employees.get(item.getEmail());
				if (runningTotal == null) {
					runningTotal = item.getAmount();
				} else {
					runningTotal = item.getAmount().add(runningTotal);
				}
				employees.put(item.getEmail(), runningTotal);
			}
			
			/*
			 * iterate accumulated employee totals and display as csv along with name and bank details
			 */
			Set<String> keys = employees.keySet();
			Iterator<String> it = keys.iterator();
			
			String email;
			while (it.hasNext()) {
				email = it.next();
				Emp emp = employeeMap.get(email);
				String output = email+","+employees.get(email);
				if (emp != null) {
					output += ","+emp.getName()+","+emp.getBank();
				}
				resp.getWriter().println(output);
			}
		}	
}
