/*
 * Copyright (c) 2013 CleverThinking Ltd
 */

package couk.cleverthinking.ex.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.tools.BaseLog;
import couk.cleverthinking.ex.tools.Constants;

@SuppressWarnings("serial")

public class Logout extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Emp cnUser = (Emp) req.getSession().getAttribute(Constants.SESSION_CNUSER);
		String email = "no logged in user";
		if (cnUser != null) {
			email = cnUser.getEmail();
		}
		BaseLog.info("[L23] logging out "+email);
		req.getSession().removeAttribute(Constants.SESSION_CNUSER);
		req.getSession().removeAttribute(Constants.SESSION_CREDENTIAL);
		req.getSession().removeAttribute(Constants.SESSION_USER_ID_KEY);
		resp.sendRedirect("/");
	}
}
/**
 * 
 * THIS IS ALL GAE LOGOUT WHICH I DON'T USE
 * 
 * 
 * This is the primary servlet, called as /b from the website
 * 
 * if not logged in, forward to a login page

 if logged in, check for record in user datastore
 - if new user, prompt with ts and cs and then do an oauth auth request. on success, get an access token then set up the default folders, generate a CnUser object, store in session, forwardt to jsp

 - if existing user, fetch ?CnUser object, store in session, return/include/redirect to jsp

 *
 * This is a protected resource in web.xml
 * 
 * @author r
 *
public class Logout extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		UserService userService = UserServiceFactory.getUserService();

		String thisURL = req.getRequestURI();

		resp.setContentType("text/html");
		if (req.getUserPrincipal() != null) {

			resp.getWriter().println(
					"<p>Hello, " + req.getUserPrincipal().getName()
							+ "!  You can <a href=\""
							+ userService.createLogoutURL(thisURL)
							+ "\">sign out</a>.</p>");
		} else {
			resp.getWriter().println(
					"<p>Please <a href=\""
							+ userService.createLoginURL(thisURL)
							+ "\">sign in</a>.</p>");
		}
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}
}
 */
