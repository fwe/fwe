/*
 * Copyright (c) 2013 CleverThinking Ltd
 */

package couk.cleverthinking.ex.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.tools.BaseLog;
import couk.cleverthinking.ex.tools.Constants;

@SuppressWarnings("serial")

public class Logger extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String loc = req.getHeader("X-AppEngine-Country") + " " + req.getHeader("X-AppEngine-CityLatLong");
		Emp cnUser = (Emp) req.getSession().getAttribute(Constants.SESSION_CNUSER);
		String email = "no logged in user";
		if (cnUser != null) {
			email = cnUser.getEmail();
		}
		BaseLog.info("[L29] logging for "+email+" step "+req.getParameter("s") + " " + loc+ " "+ req.getHeader("User-Agen"));
	}
}
