package couk.cleverthinking.ex.servlets;

import javax.servlet.http.HttpServlet;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.impl.translate.opt.BigDecimalLongTranslatorFactory;

import couk.cleverthinking.ex.model.CnUser;
import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.EmpO;
import couk.cleverthinking.ex.model.Exitem;
import couk.cleverthinking.ex.model.Fx;
import couk.cleverthinking.ex.model.Nominal;
import couk.cleverthinking.ex.model.Settings;

public class MyStartServlet extends HttpServlet {
    static {
    	System.out.println("MyStartServlet static");
        ObjectifyService.register(CnUser.class);
        ObjectifyService.register(EmpO.class);
        ObjectifyService.register(Emp.class);
        ObjectifyService.register(Fx.class);
        ObjectifyService.register(Nominal.class);
        ObjectifyService.register(Settings.class);
        ObjectifyService.register(Exitem.class);
        
        ObjectifyService.factory().getTranslators().add(new BigDecimalLongTranslatorFactory());
    }
}