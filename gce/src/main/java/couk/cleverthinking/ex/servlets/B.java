/*
 * Copyright (c) 2013 CleverThinking Ltd
 */

package couk.cleverthinking.ex.servlets;

import static com.googlecode.objectify.ObjectifyService.ofy; //OFY1

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.auth.oauth2.Credential;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.Settings;
import couk.cleverthinking.ex.oauth.CredentialMediatorB;
import couk.cleverthinking.ex.oauth.CredentialMediatorB.InvalidClientSecretsException;
import couk.cleverthinking.ex.tools.BaseLog;
import couk.cleverthinking.ex.tools.Constants;

//import couk.cleverthinking.cnw.drive.DbStorageAdapter; 

@SuppressWarnings("serial")
/**
 * 
 * This is the primary servlet, called as /b from the website
 * 

 if logged in, check for record in user datastore
 - if new user, prompt with ts and cs and then do an oauth auth request. on success, get an access token then set up the default folders, generate a CnUser object, store in session, forwardt to jsp

 - if existing user, fetch ?CnUser object, store in session, return/include/redirect to jsp

 *
 * 
 * @author r
 *
 */
public class B extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		doGet(req, resp);
	}

	@Override
	/**
	 * GET to load the app. At this point we do not know who the user is (unless he is still in a session in which case we have the 
	 * CnUser object stored in Session
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		// objectify test
		/*
		EmpO e1 = new EmpO();
		e1.setGoogleId("1234");
		e1.setEmail("1234.com");
		ofy().save().entity(e1).now();   // synchronous
		e1 = new EmpO();
		e1.setGoogleId("5678");
		e1.setEmail("5678.com");
		ofy().save().entity(e1).now();   // synchronous
		EmpO e4 = (EmpO) ofy().load().key(Key.create(EmpO.class, "1234")).now(); // works
		EmpO e2 = (EmpO) ofy().load().entity(e1).now(); // works
		//		EmpO e3 = (EmpO) ofy().load().value(e1.getGoogleId()).now();  // doesn't work
		System.out.println("retrieved "+e2.getEmail());
		//		System.out.println("retrieved "+e3.getEmail());
		System.out.println("retrieved "+e4.getEmail());
		//		EmpO e2 = ofy().load().key(e.getGoogleId()).now();
		
		List<EmpO> courses = ofy().load().type(EmpO.class).list();
		for (EmpO ex: courses) {
			System.out.println("--"+ex.getEmail());
		}
		
		
		Config.clearDownRse();
		
		
		
		
		*/

		//		ObjectifyService.begin();  v3
		//		BaseLog.severe("city="+req.getHeader("X-AppEngine-City"));
		//		BaseLog.severe("latlong="+req.getHeader("X-AppEngine-CityLatLong"));

		/*
		 * look for a demoUser  query
		 */
		if (req.getParameter("demoUser") != null) { // if we're being called with a demo user
			// is it manager or emp
			String email = "fwedemo@gmail.com";
//			if ("manager".equalsIgnoreCase(req.getParameter("demoUser"))) {
//				email = "mgrdemo@example.com";
//			}
			BaseLog.warning("[B102] demo user for " + req.getRequestURI() + " is "+email);
			// change 5, add orgn id to emp query
			Emp demoUser = null;
			List<Emp> execute = ofy().load().type(Emp.class).filter("email ==", email).list();
			try {
				demoUser = execute.get(0);
			} catch (Exception ex) {
				BaseLog.severe("[B119] no demo user for email " + email + "making a demo org");
				doMakeDemoOrg();
				execute = ofy().load().type(Emp.class).filter("email ==", email).list();
				try {
					demoUser = execute.get(0);
				} catch (Exception e2x) {
					BaseLog.severe("[B119] no demo user for email " + email + " failing and dropping through to normal processing");
				}
			}
			req.getSession().setAttribute(Constants.SESSION_CNUSER, demoUser);
		}
		
		// store the request URL in the session so OCB can use it to navigate back
		// NB. THIS CAUSES AN ISSUE WITH THE DEMO.HTML PAGE AS WE ARE REQUESTING THREE
		// DIFFERENT PAGES WITHIN THE SAME SESSION
//		req.getSession().setAttribute(Constants.SESSION_URL, req.getRequestURL()); replaced with URL in state

		BaseLog.severe("[B124] " + req.getServletPath() + " " + req.getParameter("force"));
		// we're doing a sign up, so auth (telling ocb its a signup via state) 
		if (req.getServletPath().contains("signup")) {
			doUnknownUser(req, resp, false);
		}
		if (req.getParameter("force") != null) { // if we're being called coz there is something wrong
			BaseLog.warning("[B61] force is set");
			doUnknownUser(req, resp, true); // treat it like an unknown user with force auth
			return;
		}

		if (req.getSession().getAttribute(Constants.SESSION_CNUSER) != null) {
			try {
				doKnownUser(req, resp);
			} catch (InvalidClientSecretsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			doUnknownUser(req, resp, false);
		}
	}

	/**
	 * called (probably only ever once) to make a demo organisation
	 * 
	 */
	private void doMakeDemoOrg() {
		doMakeOrg("123", "mgrdemo@example.com", "Mr Mark Manager", "Demo Company, Inc", "empdemo@example.com", "Miss Emily Employee");
	}

	private void doMakeOrg(String orgnId, String managerEmail, String managerName, String coname, String empEmail, String empName) {
		Emp mgr = new Emp();
		mgr.setEmail(managerEmail);
		mgr.setGoogleId(orgnId);
		mgr.setOrgnId(orgnId);
		mgr.setName(managerName);
		mgr.setManager(true);
		mgr.setLang("en");

		ofy().save().entity(mgr).now();

		Settings s = new Settings();
		s.setBaseiso("USD");
		s.setConame(coname);
		s.setDefaultLang("en");
		s.setOrgnId(orgnId);
		s.setKmrate(new BigDecimal("0.45"));
		s.setDefaultVatRate(new BigDecimal("0.2"));
		ofy().save().entity(s).now();

		if (empEmail != null) {
			Emp emp = new Emp();
			emp.setEmail(empEmail);
			emp.setGoogleId(orgnId + 1);
			emp.setOrgnId(orgnId);
			emp.setName(empName);
			emp.setLang("en");

			ofy().save().entity(emp).now();
		}
	}

	/**
	 * We need to authenticate the user by doing an auth request which will end
	 * up in OauthCallBackB
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	private void doUnknownUser(HttpServletRequest req, HttpServletResponse resp, boolean forceAuthorise) throws IOException {
		BaseLog.info("[B85] + unknown user");
		String force = "auto";
		if (forceAuthorise) {
			force = "force";
		}
		String u = buildTokenUrl(req, force);
		u += "&state=" + Constants.encodeState(req, ""); // uu = unknown user

		resp.sendRedirect(u);
		resp.getOutputStream().close();
	}

	/**
	 * Do the do for a user with CnUser in session object. Figure out the path
	 * (app or appdeply) and forward to app.jsp
	 * 
	 * TODO as an optimisation, we could read the JSP, substitute the CnUser and
	 * write it
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 * @throws ServletException
	 * @throws InvalidClientSecretsException
	 */
	private void doKnownUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException,
			InvalidClientSecretsException {
		BaseLog.info("[B102] + known user");
		Emp cnUser = (Emp) req.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[B111] The user is known, but there is no Cnuser record. This can happen if I deleted the user record. continuing as an unknown user");
			doUnknownUser(req, resp, true);
			return;
		}
		BaseLog.info("[B115] + user record found " + cnUser.getGoogleId());

		// if it's a real user, check credentials
		if (cnUser.getEmail().indexOf("@example.com") < 0) {
			/* check we have a stored credential with a refresh token */
			CredentialMediatorB credentialMediator = new CredentialMediatorB(req);
			Credential c = credentialMediator.getStoredCredential(cnUser.getGoogleId());
			if (c == null) {
				BaseLog.severe("[B122] The user is known, but there is no stored credential. This can happen if I deleted the user record. continuing as a new user");
				doUnknownUser(req, resp, true);
				return;
			}
			BaseLog.info("[B124] refresh token is " + c.getRefreshToken());
			if (c.getRefreshToken() == null) { // we don't have a refresh token so
												// re-prompt
				BaseLog.warning("[B127] missing refresh token for " + cnUser.getEmail() + ". forcing reauthorise");
				String u = buildTokenUrl(req, "force");
				u += "&state=" + Constants.encodeState(req, ""); // uu = unknown user
				resp.sendRedirect(u);
				return;
			}
		}

		/* if here, all is good. Check the dbversion and upgrade if necessary
		 NB this is repeated in OauthCallBackB since it is not guaranteed that the
		 user will pass through this code
		 */

		//		BaseLog.info("[B135] " + DbVersion.CURRENT_DB_VERSION + " "
		//				+ cnUser.getAllJson());
		//		if (DbVersion.CURRENT_DB_VERSION > cnUser.getDbVersion()) {
		//			DbVersion.upgrade(cnUser, new DbStorageAdapter(c, cnUser));
		//		}
		forwardToApp(req, resp);
	}

	/**
	 * Builds the appropriate app url and forwards to it
	 * 
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 */
	public void forwardToApp(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		BaseLog.severe("[B165] " + req.getServletPath());
		getServletContext().getRequestDispatcher(Constants.getUrlFromPath(req)).forward(req, resp);
	}

	public static String getOcbUrl(HttpServletRequest req) {
		String rURL = req.getRequestURL().toString();
		String[] parts = rURL.split("/");
		return parts[0] + "//" + parts[2] + "/ocb";
	}

	//
	// public static Credential ZcreateCredentialWithRefreshToken(
	// HttpTransport transport, JsonFactory jsonFactory,
	// TokenResponse tokenResponse) {
	// return new Credential.Builder(
	// BearerToken.authorizationHeaderAccessMethod())
	// .setTransport(transport)
	// .setJsonFactory(jsonFactory)
	// .setTokenServerUrl(
	// new GenericUrl(
	// "https://accounts.google.com/o/oauth2/token"))
	// .setClientAuthentication(
	// new BasicAuthentication(Constants.CLIENT_ID,
	// Constants.CLIENT_SECRET)).build()
	// .setFromTokenResponse(tokenResponse);
	// }

	// /**
	// * prompt for ts and cs, then do the oauth auth process with state=nu.
	// * Eventually we'll be in oauthcallbackB, state=nu tells us it's a new
	// user
	// * so can do the drive setup there in doNewUserPart2
	// *
	// * @param cnUser
	// * @throws IOException
	// * @throws ServletException
	// */
	// private void doNewUserPart1(Principal userPrinciple,
	// HttpServletRequest req, HttpServletResponse resp)
	// throws ServletException, IOException {
	// BaseLog.info("[B101] new user " + userPrinciple.getName());
	// String u = buildTokenUrl();
	// u += "&state=nu" + userPrinciple.getName();
	// u += "&login_hint=" + userPrinciple.getName();
	// System.out.println("login hint = "+userPrinciple.getName());
	//
	// resp.sendRedirect("/signup.html?u=" + URLEncoder.encode(u));
	// //
	// req.getRequestDispatcher("/signup.html?u="+URLEncoder.encode(u)).forward(req,
	// // resp);
	// }

	/**
	 * make a google auth URL
	 * 
	 * @param force
	 * 
	 * @see https 
	 *      ://developers.google.com/accounts/docs/OAuth2WebServer#formingtheurl
	 * @see https://developers.google.com/oauthplayground/
	 * 
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private String buildTokenUrl(HttpServletRequest req, String force) {
		String u = "https://accounts.google.com/o/oauth2/auth?";

		u += "redirect_uri=" + URLEncoder.encode(B.getOcbUrl(req));
		u += "&response_type=code";
		u += "&client_id=" + Constants.WEB_CLIENT_ID;
		u += "&approval_prompt=" + force + "&access_type=offline";
		// u +=
		// "&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.appdata+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.file+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Ftasks";
		StringBuffer sb = new StringBuffer();
		for (String scope : Constants.SCOPES) {
			sb.append(" " + scope);
		}

		u += "&scope=" + URLEncoder.encode(sb.substring(1));
		System.out.println("==GET== " + u);
		return u;
	}
}
