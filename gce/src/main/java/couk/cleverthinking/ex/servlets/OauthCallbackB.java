/*
 * Copyright (c) 2013 CleverThinking Ltd
 */

package couk.cleverthinking.ex.servlets;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.commons.codec.DecoderException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.extensions.appengine.auth.oauth2.AppEngineCredentialStore;
import com.google.api.client.repackaged.org.apache.commons.codec.DecoderException;
import com.google.appengine.api.users.User;

import couk.cleverthinking.ex.model.CnStorageGAEImplObjectifyVersion;
import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.EmpEndpoint;
import couk.cleverthinking.ex.model.Settings;
import couk.cleverthinking.ex.model.SettingsEndpoint;
import couk.cleverthinking.ex.oauth.CredentialMediatorB;
import couk.cleverthinking.ex.oauth.CredentialMediatorB.InvalidClientSecretsException;
import couk.cleverthinking.ex.tools.BaseLog;
import couk.cleverthinking.ex.tools.Constants;

@SuppressWarnings("serial")
/**
 * Called by google oauth via a browser redirect. Can be called with a variety of state variables set
 * 
 *  nufoo@bar.com = new user
 * 
 * 
 * @author r
 *
 */
public class OauthCallbackB extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		doGet(req, resp);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		BaseLog.info("[OCB50] + get " + req.getQueryString());

		// why are we here? check state
		String state = req.getParameter("state");
		if (state == null || state.length() == 0) {
			BaseLog.warning("[OCB69] Called with an empty or null state param. Nothing to do");
			return;
		}

		//		if (state.startsWith("uu")) { // followed by path eg uuappdeploy
		try {
			doUnknownUser(req, resp);
		} catch (DecoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidClientSecretsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
		//		}
	}

	/**
	 * We are here after an auth request. Need to get tokens to figure out who
	 * the user is
	 * 
	 * @param req 
	 * @param resp
	 * @throws IOException
	 * @throws DecoderException
	 * @throws ServletException
	 * @throws InvalidClientSecretsException
	 */
	private void doUnknownUser(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException, DecoderException,
			InvalidClientSecretsException {
		// check we have an authorisation code
		BaseLog.info("[OCB84] + unknown user ");

		String authcode = req.getParameter("code");
		if (req.getParameter("code") == null || req.getParameter("code").length() == 0) {
			BaseLog.severe("[OCB94] Something is wrong. here without a code parameter");
			/* try forcing an auth */
			resp.sendRedirect("/dd/b?force"); // TODO should really construct this
			resp.flushBuffer();
			resp.getWriter().close();
			return;
		}
		CredentialMediatorB credentialMediator = new CredentialMediatorB(req);

		/* make a credential */
		// g.s
		Credential credential = credentialMediator.buildEmptyCredential();

		try {
			credentialMediator.authcode2refreshtoken(req, resp, authcode, credential);
		} catch (IllegalStateException e) {
			BaseLog.severe("[OCB109] credential mediator threw " + e + " and also asked for a redirect so nothing more to do here");
			return;
		} catch (IllegalAccessException e2) {
			e2.printStackTrace();
			BaseLog.severe("[OCB111] failed to convert auth code to refresh. What to do??? "); // TODO
			resp.sendRedirect("nogrant.html");
			return;
		} catch (SocketTimeoutException e3) {
			e3.printStackTrace();
			BaseLog.severe("[OCB121] Socket timetout "); // TODO
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			doUnknownUser(req, resp);
			return;
		}

		// by here, credentialMediator holds the google user id so we can check
		// for new or existing user
		CnStorageGAEImplObjectifyVersion storage = new CnStorageGAEImplObjectifyVersion();

		Emp cnUser = storage.fetchEmpById(credentialMediator.getGoogleUserId());
		// if new user, do stuff
		if (cnUser == null) {
			BaseLog.info("[OCB132] + new user " + credentialMediator.getGoogleUserId() + " " + credentialMediator.getEmail());

			/*
			 * unline CN, new users here are invalud. They need to sign up as a manager, or be registered by a manager
			 */
			doNewOrganisation(credentialMediator.getGoogleUserId(), credentialMediator.getEmail());
//			resp.sendRedirect(Constants.getUrlFromPath(req));
			// for a new user, redirect to manager page
			resp.sendRedirect("/app/manager.html#signup");

			//			resp.sendRedirect(Constants.SIGNUP_HTML);
			return;
			//			cnUser = doNewUser(credential, credentialMediator.getGoogleUserId(), credentialMediator.getEmail(), req,
			//					resp);
		}
		//		// by the time we get here, new or old user it's the same
		//		if (DbVersion.CURRENT_DB_VERSION > cnUser.getDbVersion()) {
		//			if (DbVersion.CURRENT_DB_VERSION > cnUser.getDbVersion()) {
		//				DbVersion.upgrade(cnUser, new DbStorageAdapter(credential, cnUser));
		//			}
		//		}
		//		// find the Emp record and store it in session
		//		cnUser = new EmpEndpoint().getEmp(id)
		//		// store CnUser in session
		//		cnUser = new Emp();
		//		cnUser.setEmail(credentialMediator.getEmail());
		//		cnUser.setGoogleId(credentialMediator.getGoogleUserId());
		//		cnUser.setOrgnId(orgnId);
		BaseLog.fine("[OCB153] storing emp in session, emp email is " + cnUser.getEmail());
		req.getSession().setAttribute(Constants.SESSION_CNUSER, cnUser);

		// also store the access token so the jsps can embed it
		req.getSession().setAttribute(Constants.SESSION_ACCESSTOKEN, credential.getAccessToken());
		try {
			// this is the forward version which keeps the OCB URl
			//			getServletContext().getRequestDispatcher(Constants.getUrlFromPath(req)).forward(req, resp);
			// this is the browser redirect version - needed so the url is emp or manager
			resp.sendRedirect(Constants.getUrlFromPath(req));
			//			resp.sendRedirect("/"+req.getParameter("state").substring(2)+Constants.APP_HTML);
		} catch (IllegalStateException e) {
			BaseLog.info("[OCB139] warning, attempt to sendredirect failed, this is prob because we have already redirected so no problem.");
		}
	}

	/**
	 * Makes a new organisation with this user as the manager
	 * 
	 * @param email 
	 * @param googleId 
	 * 
	 */
	private void doNewOrganisation(String googleId, String email) {
		BaseLog.warning("[OCB177] Making new org for " + googleId + ":" + email);
		// orgn id is contact of googleid + days since 1970.
		// this allows the same googleid to create multiple orgs
		String orgnId = ""+googleId+(System.currentTimeMillis()/1000/3600/24);		
		/*
		 * make new emp
		 */
		Emp emp = new Emp();
		emp.setEmail(email);
		emp.setGoogleId(googleId);
		emp.setOrgnId(orgnId);
		emp.setManager(true);
		emp.setName("my name");
		emp.setBank("my bank details");
		User user = new User(email, "foo");
		new EmpEndpoint().insertEmp(user, emp);

		/*
		 * make a settings item
		 */
		Settings settings = new Settings();
		settings.setOrgnId(orgnId);
		settings.setDefaultLang("en"); 
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		settings.setFirstLogindate(nowAsISO);
		new SettingsEndpoint().insertSettings(user, settings);
	}

	/**
	 * Do stuff for a new user, create a CnUser record
	 * 
	 * @param credentialMediator
	 * 
	 * @param req
	 * @param resp
	 * @return 
	 * @throws IOException
	 * @throws ServletException
	 * @throws DecoderException
	 */
	private Emp doNewUser(Credential credential, String userId, String email, HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException, DecoderException {

		/* make a credential */
		// g.s
		// Credential credential = credentialMediator.getActiveCredential();

		// start making a CnUser
		Emp cnUser = new Emp();
		cnUser.setGoogleId(userId);
		cnUser.setEmail(email);
		//		cnUser.setFirstLoginDate(new Date());
		req.getSession().setAttribute(Constants.SESSION_CNUSER, cnUser);
		// store the user here because GAEStorageImpl looks for it
		CnStorageGAEImplObjectifyVersion gaeStore = new CnStorageGAEImplObjectifyVersion();
		gaeStore.insertEmp(cnUser);

		/*
		 * this bit fetches the user details. not needed but migt be useful
		 * Oauth2 oauth2; oauth2 = new Oauth2.Builder(Constants.TRANSPORT,
		 * Constants.JSON_FACTORY, credential).setApplicationName(
		 * "CleverNote").build(); Userinfo userinfo =
		 * oauth2.userinfo().get().execute();
		 * System.out.println(userinfo.getGivenName());
		 */

		// store credential in store

		BaseLog.severe("TODO this is where we use the new access token to make the default folders and store the refresh token  ");
		BaseLog.severe("TODO need to simulate some eror conditions ");

		// persist credential
		CredentialStore credentialStore = new AppEngineCredentialStore();
		credentialStore.store(cnUser.getGoogleId(), credential);

		/* use the credential to make folders */

		//		DbStorageAdapter dbs = new DbStorageAdapter(credential, cnUser);
		//		try {
		//			dbs.makeFolders(cnUser);
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		} catch (OauthExceptionWithRedirect redirect) {
		//			BaseLog.severe("[OCB201] there was a fatal problem making the folders. The original exception is "+redirect,redirect.getOriginalException());
		//			BaseLog.severe("[OCB202] we will redirect the user to  "+redirect.getPage());
		//			resp.sendRedirect(redirect.getPage());
		//			resp.getWriter().close();
		//			return cnUser;
		//		}

		// make the Cn tasklist in Tasks
		//		cnUser.setCnTasklistId(dbs.fetchTasklistId(cnUser.getGoogleId()));

		/* persist the CnUser */

		gaeStore.updateEmp(cnUser);
		return cnUser;
	}
}