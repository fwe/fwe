/**
 * (c) cleverthinking 2013
 */
package couk.cleverthinking.ex.oauth;


/**
 * represents a Token structure
 * 
 * @author r
 * 
 */
public class Tokens {
	public String access_token;
	public String id_token;
	public String expires_in;
	public String token_type;
	public String refresh_token;
}
