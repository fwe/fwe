/**
 * (c) cleverthinking 2013
 */
package couk.cleverthinking.ex.oauth;

import com.google.api.client.util.Base64;

import couk.cleverthinking.ex.tools.BaseLog;

/**
 * represents a JWT
 * 
 * @author r
 * 
 */
public class JWT {
	public JWT.Header header;
	public JWT.ClaimSet claimSet;

	public JWT(String httpContent) {
		String[] jwtParts = httpContent.split("\\.");
		// System.out.println(new String(com.google.api.client.util.Base64
		// .decodeBase64(jwtParts[0])));
		// System.out.println(new String(com.google.api.client.util.Base64
		// .decodeBase64(jwtParts[1])));
		// String part1 = new String(new Base64().decode(jwtParts[0]));

		String headerString = new String(Base64.decodeBase64(jwtParts[0]));
		String claimSetString = new String(Base64.decodeBase64(jwtParts[0]));
		com.google.gson.Gson gson = new com.google.gson.Gson();
		claimSet = gson.fromJson(claimSetString, JWT.ClaimSet.class);
		BaseLog.info("u/e="+claimSet.sub+" "+claimSet.email);;
	}

	public class Header {
	}

	/**
	 * {"iss":"accounts.google.com", "at_hash":"HK6E_P6Dh8Y93mRNtsDB1Q",
	 * "email_verified":"true", "sub":"10769150350006150715113082367",
	 * "azp":"1234987819200.apps.googleusercontent.com",
	 * "email":"jsmith@example.com",
	 * "aud":"1234987819200.apps.googleusercontent.com", "iat":1353601026,
	 * "exp":1353604926 }
	 * 
	 * @author r
	 * 
	 */
	public static class ClaimSet {
		
		public ClaimSet() {
		}
		public String iss;
		public String at_hash;
		public String email;
		public String sub;
		public String azp;
		public String aud;
		public String iat;
		public String exp;
	}
}
