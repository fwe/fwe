/*
 * Copyright (c) 2013 CleverThinking
 */

package couk.cleverthinking.ex.oauth;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.extensions.appengine.auth.oauth2.AppEngineCredentialStore;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.servlets.B;
import couk.cleverthinking.ex.tools.BaseLog;
import couk.cleverthinking.ex.tools.Constants;


public class CredentialMediatorB {
	/**
	 * The HTTP request used to make a request to this Drive application.
	 * Required so that we can manage a session for the active user, and keep
	 * track of their email address which is used to identify their credentials.
	 * We also need this in order to access a bunch of request parameters like
	 * {@code state} and {@code code}.
	 */
	private HttpServletRequest request;

	/**
	 * Scopes for which to request authorization.
	 */
	private Collection<String> scopes;

	/**
	 * Loaded data from war/WEB-INF/client_secrets.json.
	 */
	private GoogleClientSecrets secrets;

	/**
	 * CredentialStore at which Credential objects are stored.
	 */
	private CredentialStore credentialStore;

	/**
	 * extracted from the JWT/sub returned alongside the tokens
	 */
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * extracted from the JWT/sub returned alongside the tokens
	 */
	private String googleUserId;

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	/**
	 * JsonFactory to use in parsing JSON.
	 */
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	/**
	 * HttpTransport to use for external requests.
	 */
	private static final HttpTransport TRANSPORT = new NetHttpTransport();

	/**
	 * Creates a new CredentialsManager for the given HTTP request.
	 * 
	 * @param request
	 *            Request in which session credentials are stored.
	 * @param clientSecretsStream
	 *            Stream of client_secrets.json.
	 * @throws InvalidClientSecretsException
	 */
	public CredentialMediatorB(HttpServletRequest request)
			throws InvalidClientSecretsException {
		// BaseLog.fine("in CredentialMediator constructor 1");
		this.request = request;
		this.credentialStore = new AppEngineCredentialStore();
		try {
			secrets = GoogleClientSecrets.load(Constants.JSON_FACTORY,
					new StringReader(Constants.SECRETS));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * created for restlet use. dunno if it works without an httprequest
	 */
	public CredentialMediatorB() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Client information parsed from client_secrets.json.
	 */
	public GoogleClientSecrets getClientSecrets() {
		return secrets;
	}

	/**
	 * Builds an empty GoogleCredential, configured with appropriate
	 * HttpTransport, JsonFactory, and client information.
	 */
	public Credential buildEmptyCredential() {
		return new GoogleCredential.Builder().setClientSecrets(this.secrets)
				.setTransport(TRANSPORT).setJsonFactory(JSON_FACTORY).build();
	}

	/**
	 * Retrieves stored credentials for the provided email address.
	 * 
	 * @param userId
	 *            User's Google ID.
	 * @return Stored GoogleCredential if found, {@code null} otherwise.
	 * @throws IOException
	 */
	public Credential getStoredCredential(String userId) throws IOException {
		Credential credential = buildEmptyCredential();
		if (userId == null || userId.length() == 0) {
			BaseLog.severe("[CM193] trying to retrieve stored credential, but userId is null!!!!");
			return null;
		}
		if (credentialStore.load(userId, credential)) {
			return credential;
		}
		return null;
	}

	/**
	 * Deletes stored credentials for the provided email address.
	 * 
	 * @param userId
	 *            User's Google ID.
	 * @throws IOException
	 */
	private void deleteStoredCredential(String userId) throws IOException {
		if (userId != null) {
			Credential credential = getStoredCredential(userId);
			credentialStore.delete(userId, credential);
		}
	}

	/**
	 * Saves a credential
	 * 
	 * @param userId
	 *            User's Google ID.
	 * @param credential
	 *            a Credential object
	 * @throws IOException
	 */
	public void storeCredential(String userId, Credential credential)
			throws IOException {
		if (userId != null && credential != null) {
			credentialStore.store(userId, credential);
		} else {
			BaseLog.severe("[CMB188] attempt to store credential with null userId or credential "
					+ userId + "," + credential);
		}
	}

	/**
	 * uses the redirect urls from client_secrets (or my stringified version
	 * thereof) whether dev or prod and if we're running in a version to figure
	 * out the appropriate oauthcallback url
	 * 
	 * explained another way, I can't have a hardcoded callbackUrl since it will
	 * change depending on whether I'm running dev or prod, and which prod
	 * version
	 * 
	 * So this takes the current URL scheme and host and appends the callback
	 * path
	 * 
	 * @return url
	 */
	private String getMungedRedirectUrl() {
		String requestUrl = request.getRequestURL().toString();
		String callbackUrlPath = secrets.getWeb().getRedirectUris().get(0);
		String munged = requestUrl.replaceFirst("://", ":::")
				.replaceFirst("/.*", callbackUrlPath)
				.replaceFirst(":::", "://");
		return munged;
	}

	/**
	 * Exchange an authorization code for a credential.
	 * 
	 * @param authorizationCode
	 *            Authorization code to exchange for OAuth 2.0 credentials.
	 * @return Credential representing the upgraded authorizationCode.
	 * @throws CodeExchangeException
	 *             An error occurred.
	 * @throws TokenResponseException
	 */
	private Credential exchangeCode(String authorizationCode)
			throws CodeExchangeException, TokenResponseException {
		// Talk to Google and upgrade the given authorization code to an access
		// token and hopefully a refresh token.
		try {
			String redirectUrl = getMungedRedirectUrl();
			BaseLog.fine("[CM225] exchangeCode about to get accesstoken & refreshtoken for authcode="
					+ authorizationCode + " redirectUrl=" + redirectUrl);
			// https://accounts.google.com/o/oauth2/revoke?token={token}
			GoogleAuthorizationCodeTokenRequest request = new GoogleAuthorizationCodeTokenRequest(
					TRANSPORT, JSON_FACTORY, secrets.getWeb().getClientId(),
					secrets.getWeb().getClientSecret(), authorizationCode,
					redirectUrl);
			BaseLog.fine("GoogleAuthorizationCodeTokenRequest is " + request);

			GoogleTokenResponse response = request.execute(); // TODO xyzzy??
																// what is
																// xyzzy?
			// secrets.getWeb().getRedirectUris().get(urlindex)).execute();
			BaseLog.fine("[CM231] redirect url (fromclient_Secrets.json) set to "
					+ redirectUrl);
			BaseLog.fine("[CM232] response is a=" + response.getAccessToken()
					+ " r=" + response.getRefreshToken());
			BaseLog.fine("[CM232] response prettyprint "
					+ response.toPrettyString());
			return buildEmptyCredential().setFromTokenResponse(response);
		} catch (TokenResponseException e) {
			if (e.getDetails() != null) {
				BaseLog.severe("Error: " + e.getDetails().getError());
				if (e.getDetails().getErrorDescription() != null) {
					BaseLog.severe(e.getDetails().getErrorDescription());
				}
				if (e.getDetails().getErrorUri() != null) {
					BaseLog.severe(e.getDetails().getErrorUri());
				}
			} else {
				BaseLog.severe(e.getMessage());
			}
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw new CodeExchangeException();
		}
	}

	/**
	 * Uses a stored refreshtoken to retrieve an access token
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public TokenResponse refreshAccessToken(HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		String userId = (String) req.getSession().getAttribute(
				Constants.SESSION_USER_ID_KEY);
		try {
			// get the user ID from the session context
			// retrieve the stored credential
			Credential c = getStoredCredential(userId);
			// make a refresh request using the retrieved refreshtoke and stuff
			// from client-secrets.json
			String refreshToken = c.getRefreshToken();
			String clientId = secrets.getDetails().getClientId();
			String clientSecret = secrets.getDetails().getClientSecret();
			System.out.println("[CM235] r=" + refreshToken + " cid=" + clientId
					+ " cs=" + clientSecret);
			// make the call to google
			TokenResponse response = new GoogleRefreshTokenRequest(
					new NetHttpTransport(), new JacksonFactory(), refreshToken,
					clientId, clientSecret).execute();

			System.out.println("[CM240] Access token: "
					+ response.getAccessToken());
			// put the new accesstoken into the credential
			c.setAccessToken(response.getAccessToken());
			c.setExpiresInSeconds(response.getExpiresInSeconds());
			// c.setExpirationTimeMilliseconds(response.) should I store the
			// refresh time. where is it?
			// and store it
			credentialStore.store(userId, c);
			// return teh access token as a string. The caller will prob wrap it
			// into a json object
			return response;
		} catch (TokenResponseException e) {
			if (e.getDetails() != null) {
				BaseLog.warning("[CM336] Error: " + e.getDetails().getError());
				if (e.getDetails().getError() != null
						&& e.getDetails().getError().indexOf("invalid_grant") > -1) {
					BaseLog.info("deleting stored credential for " + userId
							+ " and retrying");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					deleteStoredCredential(userId);
					refreshAccessToken(req, resp);
				}
				if (e.getDetails().getErrorDescription() != null) {
					System.err.println(e.getDetails().getErrorDescription());
				}
				if (e.getDetails().getErrorUri() != null) {
					System.err.println(e.getDetails().getErrorUri());
				}
			} else {
				System.err.println(e.getMessage());
			}
		}
		return null;
	}

	/**
	 * Send a request to the UserInfo API to retrieve user e-mail address
	 * associated with the given credential.
	 * 
	 * @param credential
	 *            Credential to authorize the request.
	 * @return User's e-mail address.
	 * @throws NoUserIdException
	 *             An error occurred, and the retrieved email address was null.
	 */
	public Userinfoplus getUserInfo(Credential credential) throws NoUserIdException {
		Userinfoplus userInfo = null;
		// Create a user info service, and make a request to get the user's
		// info.
		Oauth2 userInfoService = new Oauth2.Builder(TRANSPORT, JSON_FACTORY,
				credential).build();
		try {
			userInfo = userInfoService.userinfo().get().execute();
			if (userInfo == null) {
				throw new NoUserIdException();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userInfo;
	}

	/**
	 * Retrieve the authorization URL to authorize the user with the given email
	 * address.
	 * 
	 * @param emailAddress
	 *            User's e-mail address.
	 * @return Authorization URL to redirect the user to.
	 * @throws UnsupportedEncodingException
	 */
	private String getAuthorizationUrl(String emailAddress)
			throws UnsupportedEncodingException {
		String redirectUrl = getMungedRedirectUrl();

		GoogleAuthorizationCodeRequestUrl urlBuilder = new GoogleAuthorizationCodeRequestUrl(
				secrets.getWeb().getClientId(), redirectUrl, scopes)
				.setAccessType("offline").setApprovalPrompt("auto");
		BaseLog.fine("[CM424] getAuthorizationUrl for " + emailAddress
				+ " state=" + request.getParameter("state"));

		// Propagate through the current state parameter, so that when the
		// user gets redirected back to our app, they see the file(s) they
		// were originally supposed to see before we realized we weren't
		// authorized.
		if (request.getParameter("state") != null) {
			// System.out.println("");
			urlBuilder.set("state", request.getParameter("state"));
		} else {
			String hostUrl = request.getRequestURL() + "?"
					+ request.getQueryString();
			urlBuilder.set("state", URLEncoder.encode(hostUrl, "UTF-8"));
		}
		if (emailAddress != null) {
			urlBuilder.set("user_id", emailAddress);
		}
		return urlBuilder.build();
	}

	/**
	 * Deletes the credential of the active session.
	 * 
	 * @throws IOException
	 */
	public void deleteActiveCredential() throws IOException {
		String userId = (String) request.getSession().getAttribute(
				Constants.SESSION_USER_ID_KEY);
		this.deleteStoredCredential(userId);
	}

	/**
	 * Retrieve credentials using the provided authorization code.
	 * 
	 * This function exchanges the authorization code for an access token and
	 * queries the UserInfo API to retrieve the user's e-mail address. If a
	 * refresh token has been retrieved along with an access token, it is stored
	 * in the application database using the user's e-mail address as key. If no
	 * refresh token has been retrieved, the function checks in the application
	 * database for one and returns it if found or throws a
	 * NoRefreshTokenException with the authorization URL to redirect the user
	 * to.
	 * 
	 * @return Credential containing an access and refresh token.
	 * @throws NoRefreshTokenException
	 *             No refresh token could be retrieved from the available
	 *             sources.
	 * @throws IOException
	 */
	public Credential getActiveCredential() throws NoRefreshTokenException,
			IOException {
		String userId = (String) request.getSession().getAttribute(
				Constants.SESSION_USER_ID_KEY);

		// added to try to find user Id frmo appengine, coz currently it's often
		// null
		// which causes a re-authorisation dialogue
		// if (userId == null) {
		// BaseLog.warning("request.getSession().getAttribute(USER_ID_KEY) is null");
		// UserService userService = UserServiceFactory.getUserService();
		// User user = userService.getCurrentUser();
		// userId = user.getUserId();
		// BaseLog.info("gae gave us " + user.getEmail() + " "
		// + user.getUserId());
		// }

		Credential credential = null;
		try {
			// Only bother looking for a Credential if the user has an existing
			// session with their email address stored.
			BaseLog.fine("in getActiveCredential. userid is " + userId);
			if (userId != null) {
				credential = getStoredCredential(userId);
			}

			// No Credential was stored for the current user or no refresh token
			// is
			// available.
			// If an authorizationCode is present, upgrade it into an
			// access token and hopefully a refresh token.
			if ((credential == null || credential.getRefreshToken() == null)
					&& request.getParameter("code") != null) {
				System.out.println("getActiveCredential 1 " + credential);

				credential = exchangeCode(request.getParameter("code"));
				if (credential != null) {
					System.out.println("getActiveCredential 2 " + credential);
					System.out.println(credential.getAccessToken() + " r="
							+ credential.getRefreshToken());
					Userinfoplus userInfo = getUserInfo(credential);
					userId = userInfo.getId();
					request.getSession().setAttribute(
							Constants.SESSION_USER_ID_KEY, userId);
					request.getSession().setAttribute(
							Constants.SESSION_EMAIL_KEY, userInfo.getEmail());
					// Sometimes we won't get a refresh token after upgrading a
					// code.
					// This won't work for our app, because the user can land
					// directly
					// at our app without first visiting Google Drive.
					// Therefore,
					// only bother to store the Credential if it has a refresh
					// token.
					// If it doesn't, we'll get one below.
					if (credential.getRefreshToken() != null) {
						credentialStore.store(userId, credential);
						System.out.println("Storing crdential " + credential);
					}
				}
			}

			// if (credential == null || credential.getRefreshToken() == null) {
			if (credential == null) { // we can proceed without a refresh token
				System.out.println("getActiveCredential 3");

				// No refresh token has been retrieved.
				// Start a "fresh" OAuth 2.0 flow so that we can get a refresh
				// token.
				String email = (String) request.getSession().getAttribute(
						Constants.SESSION_EMAIL_KEY);
				String authorizationUrl = getAuthorizationUrl(email);
				BaseLog.info("[CM551] Starting a new auth flow with "
						+ authorizationUrl);
				throw new NoRefreshTokenException(authorizationUrl);
			}
		} catch (CodeExchangeException e) {
			// The code the user arrived here with was bad. This pretty much
			// never
			// happens. In a production application, we'd either redirect the
			// user
			// somewhere like a home page, or show them a vague error mentioning
			// that they probably didn't arrive to our app from Google Drive.
			BaseLog.severe("[CM545] Got a codeExchange exception  " + e + " "
					+ e.getCause(), e);
			e.printStackTrace();
		} catch (NoUserIdException e) {
			// This is bad because it means the user either denied us access
			// to their email address, or we couldn't fetch it for some reason.
			// This is unrecoverable. In a production application, we'd show the
			// user a message saying that we need access to their email address
			// to work.
			e.printStackTrace();
		}
		return credential;
	}

	/**
	 * Exception thrown when no refresh token has been found.
	 */
	@SuppressWarnings("serial")
	public static class NoRefreshTokenException extends Exception {

		/**
		 * Authorization URL to which to redirect the user.
		 */
		private String authorizationUrl;

		/**
		 * Construct a NoRefreshTokenException.
		 * 
		 * @param authorizationUrl
		 *            The authorization URL to redirect the user to.
		 */
		public NoRefreshTokenException(String authorizationUrl) {
			this.authorizationUrl = authorizationUrl;
		}

		/**
		 * @return Authorization URL to which to redirect the user.
		 */
		public String getAuthorizationUrl() {
			return authorizationUrl;
		}
	}

	/**
	 * Exception thrown when client_secrets.json is missing or invalid.
	 */
	@SuppressWarnings("serial")
	public static class InvalidClientSecretsException extends Exception {
		/**
		 * Construct an InvalidClientSecretsException with a message.
		 * 
		 * @param message
		 *            Message to escalate.
		 */
		public InvalidClientSecretsException(String message) {
			super(message);
		}
	}

	/**
	 * Exception thrown when no email address could be retrieved.
	 */
	@SuppressWarnings("serial")
	private static class NoUserIdException extends Exception {
	}

	/**
	 * Exception thrown when a code exchange has failed.
	 */
	@SuppressWarnings("serial")
	private static class CodeExchangeException extends Exception {
	}

	/**
	 * 
	 * This is the cnw6 2013 method to convert an auth code to an access/refresh token
	 * 
	 * @param req
	 * @param authcode
	 * @param credential
	 * @throws IOException
	 * @throws IllegalAccessException
	 */
	public void authcode2refreshtoken(HttpServletRequest req, HttpServletResponse resp,String authcode,
			Credential credential) throws IOException, IllegalAccessException {

		/* get access token, refersh token and user id */

		URL url = new URL("https://accounts.google.com/o/oauth2/token");
		HTTPRequest request = new HTTPRequest(url, HTTPMethod.POST);
		request.getFetchOptions().setDeadline(20d); // set 20 sec timeout 
		request.setHeader(new HTTPHeader("Content-Type",
				"application/x-www-form-urlencoded"));
		@SuppressWarnings("deprecation")
		String body = "redirect_uri="
				+ URLEncoder.encode(B.getOcbUrl(req));
		body += "&code=" + authcode;
		body += "&client_id=" + Constants.WEB_CLIENT_ID;
		body += "&client_secret=" + Constants.CLIENT_SECRET;
		body += "&grant_type=authorization_code";
		BaseLog.info("[CMB626] authcode2refersh is posting "+body);;
		request.setPayload(body.getBytes("UTF-8"));
		System.out.println("==POST== "+body+" to url "+url);

		HTTPResponse gResponse = URLFetchServiceFactory.getURLFetchService()
				.fetch(request);

		if (gResponse.getResponseCode() != 200) {
			BaseLog.severe("[CMB634] bad http response "
					+ gResponse.getResponseCode() + " "
					+ new String(gResponse.getContent()));
			throw new IllegalAccessException(gResponse.getResponseCode() + " "
					+ new String(gResponse.getContent()));
		}

		// here with the result of the URL fetch for access token
		// System.out.println(response.getResponseCode());
		String content = new String(gResponse.getContent());
		// System.out.println(content);

		com.google.gson.Gson gson = new com.google.gson.Gson();
		Tokens tokens = gson.fromJson(content, Tokens.class);

		
		BaseLog.info("[CMB643] a="+tokens.access_token);
//		System.out.println(tokens.refresh_token);
//		System.out.println(tokens.id_token);

		
//		String headerString = new String(Base64.decodeBase64(jwtParts[0]));
		String claimSetString = new String(Base64.decodeBase64(tokens.id_token.split("\\.")[1]));
		JWT.ClaimSet claimSet = gson.fromJson(claimSetString, JWT.ClaimSet.class);
		BaseLog.info("[CMB651] u/e="+claimSet.sub+" "+claimSet.email);;

//		JWT jwt = new JWT(tokens.id_token);

		this.googleUserId = claimSet.sub;
		this.email = claimSet.email;
		credential.setAccessToken(tokens.access_token);
		credential.setRefreshToken(tokens.refresh_token);
		credential.setExpirationTimeMilliseconds((long) (new Date().getTime() +(Long.parseLong(tokens.expires_in)*0.9)));
		
		// if there is a refresh token, persist it 
		// (there won't be a RT if this is a simple identification pass)
		if (credential.getRefreshToken() != null && credential.getRefreshToken().length() > 0) {
			BaseLog.fine("[CMB672] we were given a refresh token, which is being stored");
			storeCredential(googleUserId, credential);
		}
		
		// check we now have a stored refresh token
		// if not, need to repeat the process with forced auth
		Credential cCheck = getStoredCredential(this.googleUserId);
		if (cCheck == null || cCheck.getRefreshToken() == null) {
			String rUrl = "/dd/b?force";
			BaseLog.warning("[CMB678] Somehow we have an user who has authorised us, but no stored refresh token. this may be because in testing, I deleted the storedcredential. Reauthing by redirecting to /ad/b?force");
			resp.sendRedirect(rUrl);  // TODO should really construct this
			resp.flushBuffer();
			resp.getWriter().close();
			throw new IllegalStateException("[CMB678] redirect to re-auth sent url="+rUrl);
		}
	}
	
	

	/**
	 * 
	 * This is the cnw6 2013 method to convert an refresh token to an access token
	 * the access token is stored  in the credential which is also then persisted
	 * 
	 * @see https://developers.google.com/accounts/docs/OAuth2WebServer#offline
	 * 
	 * @param req
	 * @param refreshToken
	 * @param credential
	 * @throws IOException
	 * @throws IllegalAccessException
	 */
	public void refreshtoken2accesstoken(Emp cnUser, String refreshToken,
			Credential credential) throws IOException, IllegalAccessException {

		/* get access token, refersh token and user id */

		URL url = new URL("https://accounts.google.com/o/oauth2/token");
		HTTPRequest request = new HTTPRequest(url, HTTPMethod.POST);
		request.setHeader(new HTTPHeader("Content-Type",
				"application/x-www-form-urlencoded"));
		@SuppressWarnings("deprecation")
//		String body = "refresh_token=" + URLEncoder.encode(refreshtoken);
		String body = "refresh_token=" + refreshToken;
		body += "&client_id=" + Constants.WEB_CLIENT_ID;
		body += "&client_secret=" + Constants.CLIENT_SECRET;
		body += "&grant_type=refresh_token";
		BaseLog.info("[CMB714] refreshtoken2accesstoken is posting "+body);;
		BaseLog.info(body);;
		request.setPayload(body.getBytes("UTF-8"));
		System.out.println("==POST== "+body+" to url "+url);

		HTTPResponse gResponse = URLFetchServiceFactory.getURLFetchService()
				.fetch(request);

		if (gResponse.getResponseCode() != 200) {
			String response = new String(gResponse.getContent());
			if (response.toLowerCase().contains("invalid") && response.toLowerCase().contains("grant")) {
				BaseLog.severe("got an invalid grant. need to invalidate refresh token and force re-auth");
				this.deleteStoredCredential(cnUser.getGoogleId());
			}
			BaseLog.severe("[CMB728] bad http response "
					+ gResponse.getResponseCode() + " "
					+ new String(gResponse.getContent()));
			throw new IllegalAccessException(gResponse.getResponseCode() + " "
					+ new String(gResponse.getContent()));
		}

		// here with the result of the URL fetch for access token
		// System.out.println(response.getResponseCode());
		String content = new String(gResponse.getContent());
		// System.out.println(content);

		com.google.gson.Gson gson = new com.google.gson.Gson();
		Tokens tokens = gson.fromJson(content, Tokens.class);

		
//		System.out.println(tokens.access_token);
//		System.out.println(tokens.refresh_token);
//		System.out.println(tokens.id_token);

		
//		String headerString = new String(Base64.decodeBase64(jwtParts[0]));
		String claimSetString = new String(Base64.decodeBase64(tokens.id_token.split("\\.")[1]));
		JWT.ClaimSet claimSet = gson.fromJson(claimSetString, JWT.ClaimSet.class);
		BaseLog.info("u/e="+claimSet.sub+" "+claimSet.email);;

//		JWT jwt = new JWT(tokens.id_token);

		this.googleUserId = claimSet.sub;
		this.email = claimSet.email;
		credential.setAccessToken(tokens.access_token);
		credential.setExpirationTimeMilliseconds((long) (System.currentTimeMillis() +(Long.parseLong(tokens.expires_in)*900)));
		credential.setRefreshToken(refreshToken);
		
		// if there is a refresh token, persist it 
		// (there won't be a RT if this is a simple identification pass)
		if (credential.getRefreshToken() != null && credential.getRefreshToken().length() > 0) {
			storeCredential(googleUserId, credential);
		}
	}
}