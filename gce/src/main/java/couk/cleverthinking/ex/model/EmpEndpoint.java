package couk.cleverthinking.ex.model;

import static com.googlecode.objectify.ObjectifyService.ofy; //OFY1

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;

import couk.cleverthinking.ex.tools.Constants;
import couk.cleverthinking.ex.tools.Sharder;

@Api(name = "empendpoint",
// change 1 - add authorised clients and minimum scopes
scopes = { Constants.EMAIL_SCOPE }, clientIds = { Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID }, namespace = @ApiNamespace(ownerDomain = "cleverthinking.couk", ownerName = "cleverthinking.couk", packagePath = "ex.model"))
public class EmpEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listEmp")
	//	/ change 2 (for each method) add User user,
	public CollectionResponse<Emp> listEmp(User user, @Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		Cursor cursor = null;
		
		// change 5, add orgn id to emp query
		List<Emp> execute = ofy().load().type(Emp.class).filter("orgnId ==", Sharder.shardFromUser(null, user)).list();
		for (Emp ex : execute) {
			System.out.println("--" + ex.getEmail());
		}
		;

		return CollectionResponse.<Emp> builder().setItems(execute).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getEmp")
	public Emp getEmp(@Named("id") String id) {
		Emp emp = (Emp) ofy().load().key(Key.create(Emp.class, id)).now(); // works
		return emp;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param emp the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertEmp")
	public Emp insertEmp(User user, Emp emp) {
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param emp the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateEmp")
	public Emp updateEmp(User user, Emp emp) {
		// change 7, stuff orgn Id
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeEmp")
	public void removeEmp(User user, @Named("id") String id) {
		ofy().delete().key(Key.create(Emp.class, id));
	}

	private boolean containsEmp(User user, Emp emp) {
		boolean contains = true;
		Emp item = (Emp) ofy().load().key(Key.create(Emp.class, emp.getGoogleId())).now();
		if (item == null) {
			contains = false;
		}
		return contains;
	}
}
