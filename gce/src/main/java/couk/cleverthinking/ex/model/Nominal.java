/**
 * @copyright Roy Smith 2013
 */
package couk.cleverthinking.ex.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;


/**
 * Nominal account codes
 * 
 * @author r
 *
 */
@Entity
public class Nominal implements ShardedByOrgnId{
	@Id
	String mnemonicOrgnId;
	@Index
	String orgnId;
	String mnemonic;
	String desc;
	
	
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getMnemonicOrgnId() {
		return mnemonicOrgnId;
	}
	public void setMnemonicOrgnId(String mnemonicOrgnId) {
		this.mnemonicOrgnId = mnemonicOrgnId;
	}
	@Override
	public String getOrgnId() {
		return orgnId;
	}
	@Override
	public void setOrgnId(String orgnId) {
		this.orgnId = orgnId;
	}
	
	
}
