/**
 * @copyright Roy Smith 2013
 */
package couk.cleverthinking.ex.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;



/**
 * General settings
 * 
 * @author r
 *
 */
@Entity
public class Emp implements ShardedByOrgnId, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	String email;
	String googleId;
	@Index
	String orgnId;  // actually the googleId of the initial manager
	@Index
	String name;
	String bank;
	String lang; // 2 character lower case, eg 'en'
	boolean manager;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public boolean isManager() {
		return manager;
	}
	public void setManager(boolean manager) {
		this.manager = manager;
	}
	public String getGoogleId() {
		return googleId;
	}
	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}
	public String getOrgnId() {
		return orgnId;
	}
	public void setOrgnId(String orgnId) {
		this.orgnId = orgnId;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}


}
