package couk.cleverthinking.ex.model;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;


import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.cmd.Query;

import couk.cleverthinking.ex.tools.BaseLog;

public class CnStorageGAEImplObjectifyVersion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Objectify ofyService;

	public CnStorageGAEImplObjectifyVersion() {
		//		ofyService= ObjectifyService.begin();  v3
	}

	public Emp fetchEmpByEmail(String email) {
		// The Query itself is Iterable
		Query<Emp> q = ofy().load().type(Emp.class).filter("email =", email).limit(10); //v4
		//		Query<Emp> q = ofyService.query(Emp.class).filter("email =", email); // v3
		for (Emp cnUser : q) {
			System.out.println(cnUser.toString());
			BaseLog.fine("user " + cnUser);
			return cnUser;
		}
		return null;
	}

	public Emp fetchEmpById(String googleId) {
		BaseLog.severe("gid = "+googleId);
		// The Query itself is Iterable
		Emp cnUser;
		try {
//			Key<Emp> key = Key.create(Base64.encodeString(googleId));
			Key<Emp> key = Key.create(Emp.class, googleId); // v4
			cnUser = (Emp) ofy().load().key(key).now(); //v4
			//			cnUser = ofyService.get(Emp.class, googleId); //v3
		} catch (NotFoundException e) {
			return null;
		}
		return cnUser;
	}

	public void insertEmp(Emp cnUser) {
		ofy().save().entity(cnUser).now(); // v4
		//		 ofyService.put(cnUser);  // v3
	}

	public void updateEmp(Emp cnUser) {
		ofy().save().entity(cnUser).now(); // v4
		//		 ofyService.put(cnUser); // v3		
	}
}