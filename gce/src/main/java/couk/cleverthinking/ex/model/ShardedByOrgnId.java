/**
 * @copyright 2013 Roy Smith
 */
package couk.cleverthinking.ex.model;

/**
 * defines that an entity has an organistion ID, which is used to create namespaces
 * 
 * @author r
 *
 */
public interface ShardedByOrgnId {
	public void setOrgnId(String orgnId);
	public String getOrgnId();
}
