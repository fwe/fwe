/*
 * Copyright 2012 CleverThinking Ltd
 */
package couk.cleverthinking.ex.model; 

import java.io.Serializable;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

//@Cached(expirationSeconds = 600)
@Entity(name = "CnUser")
@Index
// or @Unindexed sets default for fields in class; if neither specified, assume
// @Indexed 

/**
 * 
 * Data object representing a Clevernote User
 * 
 * Has the concept of all v. exposed fields (exposed = visible to the user)
 * 
 * Stored/retrieved from datastore using  objectify
 * 
 * @author r
 *
 */
public class CnUser implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6515442880748023207L;
	@Id
	@Index
	/**
	 * The Google ID, different from Appengine user id
	 */
	String googleId;
	
	/*
	 * ACCOUNT/STATUS SETTINGS
	 */
	
	/**
	 * gmail address
	 */
	@Expose
	@Index
	String email;
	/**
	 * First name
	 */
	String firstName;
	/**
	 * Last name
	 */
	String lastName;
	/**
	 * Date first logged in, by definition null means new user
	 */
	Date firstLoginDate;
	/**
	 * Version of database that this user has been upgraded to
	 */
	@Expose
	int dbVersion;

	
	/*
	 * INTERNAL SETTINGS
	 */
	/**
	 * The ID of the CleverNote folder
	 */
	@Expose
	String cnFolderId;

	/**
	 * The ID of the Bookmark folder, children of this are considered bookmarks
	 */
	@Expose
	String cnBmFolderId;
	
	
	/**
	 * The ID of the Unfiled folder. New notes from email and CNX go here
	 */
	@Expose
	String cnUnfiledFolderId;
	
	/*
	 * USER CONFIG SETTINGS
	 */
	/**
	 * The ID of the CleverNote task list
	 */
	@Expose
	String cnTasklistId;
	/**
	 * true will set convert=true on uploads 
	 */
	@Expose
	boolean convertToGdocs = false;
	
	
	public CnUser(String googleId, String email, String firstName,
			String lastName, Date firstLoginDate, String cnFolderId,
			String cnBmFolderId, String cnUnfiledFolderId, String cnTasklistId) {

		
		this();
		
		
		this.googleId = googleId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.firstLoginDate = firstLoginDate;
		this.cnFolderId = cnFolderId;
		this.cnTasklistId = cnTasklistId;
		this.cnBmFolderId = cnBmFolderId;
		this.cnUnfiledFolderId = cnUnfiledFolderId;
	}

	public CnUser() {	
//		this.dbVersion=DbVersion.CURRENT_DB_VERSION;
		this.dbVersion=1;
	}

	public String toString() {
		return googleId + ":" + email;
	}

	public String dump() {
		return googleId + ":" + email + ":" + firstName + ":" + lastName
				+ " CnFId=" + cnFolderId + " CnBmFId=" + cnBmFolderId
				+ " CnUnfiledId=" + cnUnfiledFolderId;
	}

	/**
	 * Returns a JSON string of explicitly @Exposed fields
	 * 
	 * @return
	 */
	public String getExposedJson() {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//		String delme = gson.toJson(this).toString();
		return gson.toJson(this).toString();
	}

	/**
	 * Returns a JSON string of all fields. Useful for debugging
	 * 
	 * @return
	 */
	public String getAllJson() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this).toString();
	}
	
	public String getFullname() {
		return firstName + " " + lastName;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getFirstLoginDate() {
		return firstLoginDate;
	}

	public void setFirstLoginDate(Date firstLoginDate) {
		this.firstLoginDate = firstLoginDate;
	}

	public String getCnFolderId() {
		return cnFolderId;
	}

	public void setCnFolderId(String cnFolderId) {
		this.cnFolderId = cnFolderId;
	}

	public String getCnTasklistId() {
		return cnTasklistId;
	}

	public void setCnTasklistId(String cnTasklistId) {
		this.cnTasklistId = cnTasklistId;
	}

	public String getCnBmFolderId() {
		return cnBmFolderId;
	}

	public void setCnBmFolderId(String cnBmFolderId) {
		this.cnBmFolderId = cnBmFolderId;
	}

	public String getCnUnfiledFolderId() {
		return cnUnfiledFolderId;
	}

	public void setCnUnfiledFolderId(String cnUnfiledFolderId) {
		this.cnUnfiledFolderId = cnUnfiledFolderId;
	}

	public boolean isConvertToGdocs() {
		return convertToGdocs;
	}

	public void setConvertToGdocs(boolean convertToGdocs) {
		this.convertToGdocs = convertToGdocs;
	}


	public int getDbVersion() {
		return dbVersion;
	}

	public void setDbVersion(int dbVersion) {
		this.dbVersion = dbVersion;
	}

}