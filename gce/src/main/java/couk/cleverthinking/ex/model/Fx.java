/**
 * @copyright Roy Smith 2013
 */
package couk.cleverthinking.ex.model;

import java.math.BigDecimal;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;


/**
 * General settings
 * 
 * @author r  
 *
 */
@Entity
public class Fx implements ShardedByOrgnId{
	@Id
	String isoOrgnId; // composite key
	String iso;
	String rate;
	@Index
	String orgnId;  // actually the googleId of the initial manager
	public String getIso() {
		return iso;
	}
	public void setIso(String iso) {
		this.iso = iso;
	}
	public BigDecimal getRate() {
		return new BigDecimal(rate);
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate.toPlainString();
	}
	public String getIsoOrgnId() {
		return isoOrgnId;
	}
	
	public void setIsoOrgnId(String isoOrgnId) {
		this.isoOrgnId = isoOrgnId;
	}
	@Override
	public String getOrgnId() {
		return orgnId;
	}
	@Override
	public void setOrgnId(String orgnId) {
		this.orgnId = orgnId;
	}
	
	
}
