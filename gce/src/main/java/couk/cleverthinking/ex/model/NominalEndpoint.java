package couk.cleverthinking.ex.model;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy; //OFY1

import javax.annotation.Nullable;
import javax.inject.Named;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;

import couk.cleverthinking.ex.tools.Constants;
import couk.cleverthinking.ex.tools.Sharder;

@Api(name = "nominalendpoint", 
// change 1 - add authorised clients and minimum scopes
scopes = { Constants.EMAIL_SCOPE }, clientIds = { Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID }, namespace = @ApiNamespace(ownerDomain = "cleverthinking.couk", ownerName = "cleverthinking.couk", packagePath = "ex.model"))
public class NominalEndpoint { 

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" }) 
	@ApiMethod(name = "listNominal")
	//	/ change 2 (for each method) add User user,
	public CollectionResponse<Nominal> listNominal(User user, @Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		Cursor cursor = null;

		// change 5, add orgn id to emp query
		List<Nominal> execute = ofy().load().type(Nominal.class).filter("orgnId ==", Sharder.shardFromUser(null, user)).list();

		return CollectionResponse.<Nominal> builder().setItems(execute).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getNominal")
	public Nominal getNominal(@Named("id") String id) {
		Nominal emp = (Nominal) ofy().load().key(Key.create(Nominal.class, id)).now(); // works
		return emp;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param emp the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertNominal")
	public Nominal insertNominal(User user, Nominal emp) {
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param emp the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateNominal")
	public Nominal updateNominal(User user, Nominal emp) {
		// change 7, stuff orgn Id
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeNominal")
	public void removeNominal(User user, @Named("id") String id) {
		ofy().delete().key(Key.create(Nominal.class, id));
	}

	private boolean containsNominal(User user, Nominal emp) {
		boolean contains = true;
		Nominal item = (Nominal) ofy().load().key(Key.create(Nominal.class, emp.getMnemonicOrgnId())).now();
		if (item == null) {
			contains = false;
		}
		return contains;
	}
}
