/**
 * @copyright Roy Smith 2013
 */
package couk.cleverthinking.ex.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * The Expenses item
 * 
 * @author r
 *
 */
@Entity
public class Exitem implements Serializable, ShardedByOrgnId{
	private static final long serialVersionUID = 7390103290165670089L;
	@Id
	Long id;
	@Index
	String orgnId;
	/**
	* email. used as a unique identifier and foreign key to emps
	*/
	@Index
	String email;
	/**
	* Foreign,Local,Km  , influences what fields are displayed
	*/
	String type;
	/**
	* description of the expense item
	*/
	String desc;
	/**
	* total amount in local currency
	*/
	String amount="0";
	/**
	* net amount in local currency
	*/
	String net="0";
	/**
	* vat amount in local currency
	*/
	String vat="0";
	/**
	* status: N, A, Q, R, P
	*/
	String stat;
	/**
	* nominal account mnemonic and lookup to nominal table
	*/
	String nom;
	/**
	* comma separated tags eg. tag1,tag2
	*/
	String tags;
	/**
	* date incurred
	*/
	String dateI;
	/**
	* date approved for payment. all items with same date form a payment batch
	*/
	@Index
	String dateP;
	/**
	* mileage
	*/
	int km;
	/**
	* mileage rate this item
	*/
	String kmrate;
	/**
	* currency expense was incurred in
	*/
	String currency;
	/**
	* exch rate for this item
	*/
	String fx;
	/**
	* amount in incurred currency
	*/
	String fxamount="0";
	/**
	* net in incurred currency
	*/
	String fxnet="0";
	/**
	* vat in incurred currency
	*/
	String fxvat="0";
	/**
	* managers comment (usually a question)
	*/
	String mcomment;
	/**
	* emp's comment (usually an answer)
	*/
	String ecomment;
	/**
	* receipt url, may be a Driev id
	*/
	String rurl;
	/**
	* action to be applied to this item
	* A, R, Q, P
	*/
	String action;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public BigDecimal getAmount() {
		if (amount == null) {
			amount = "0";
		}
		return new BigDecimal(amount);
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount.toPlainString();
	}
	public BigDecimal getNet() {
		if (net == null) {
			net = "0";
		}
		return new BigDecimal(net);
	}
	public void setNet(BigDecimal net) {
		this.net = net.toPlainString();
	}
	public BigDecimal getVat() {
		if (vat == null) {
			vat = "0";
		}
		return new BigDecimal(vat);
	}
	public void setVat(BigDecimal vat) {
		this.vat = vat.toPlainString();
	}
	public String getStat() {
		return stat;
	}
	public void setStat(String stat) {
		this.stat = stat;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getDateI() {
		return dateI;
	}
	public void setDateI(String dateI) {
		this.dateI = dateI;
	}
	public String getDateP() {
		return dateP;
	}
	public void setDateP(String dateP) {
		this.dateP = dateP;
	}
	public int getKm() {
		return km;
	}
	public void setKm(int km) {
		this.km = km;
	}
	public BigDecimal getKmrate() {
		if (kmrate == null) kmrate="0";
		return new BigDecimal(kmrate);
	}
	public void setKmrate(BigDecimal kmrate) {
		this.kmrate = kmrate.toPlainString();
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getFx() {
		if (fx == null) fx="0";
		return new BigDecimal(fx);
	}
	public void setFx(BigDecimal fx) {
		this.fx = fx.toPlainString();
	}
	public BigDecimal getFxamount() {
		if (fxamount == null) fxamount="0";
		return new BigDecimal(fxamount);
	}
	public void setFxamount(BigDecimal fxamount) {
		this.fxamount = fxamount.toPlainString();
	}
	public BigDecimal getFxnet() {
		if (fxnet == null) fxnet="0";
		return new BigDecimal(fxnet);
	}
	public void setFxnet(BigDecimal fxnet) {
		this.fxnet = fxnet.toPlainString();
	}
	public BigDecimal getFxvat() {
		if (fxvat == null) fxvat="0";
		return new BigDecimal(fxvat);
	}
	public void setFxvat(BigDecimal fxvat) {
		this.fxvat = fxvat.toPlainString();
	}
	public String getMcomment() {
		return mcomment;
	}
	public void setMcomment(String mcomment) {
		this.mcomment = mcomment;
	}
	public String getEcomment() {
		return ecomment;
	}
	public void setEcomment(String ecomment) {
		this.ecomment = ecomment;
	}
	public String getRurl() {
		return rurl;
	}
	public void setRurl(String rurl) {
		this.rurl = rurl;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Override
	public String getOrgnId() {
		return orgnId;
	}
	@Override
	public void setOrgnId(String orgnId) {
		this.orgnId = orgnId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
}

