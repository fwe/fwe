package couk.cleverthinking.ex.model;

import static com.googlecode.objectify.ObjectifyService.ofy; //OFY1

import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;

import couk.cleverthinking.ex.tools.Config;
import couk.cleverthinking.ex.tools.Constants;
import couk.cleverthinking.ex.tools.Sharder;

@Api(name = "exitemendpoint",  
// change 1 - add authorised clients and minimum scopes
scopes = { Constants.EMAIL_SCOPE }, clientIds = { Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID }, namespace = @ApiNamespace(ownerDomain = "cleverthinking.couk", ownerName = "cleverthinking.couk", packagePath = "ex.model"))
public class ExitemEndpoint { 

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" }) 
	@ApiMethod(name = "listExitem")
	//	/ change 2 (for each method) add User user,
	public CollectionResponse<Exitem> listExitemAllLive(User user, @Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		Cursor cursor = null;

		// change 5, add orgn id to emp query
		List<Exitem> execute = ofy().load().type(Exitem.class).filter("orgnId ==", Sharder.shardFromUser(null, user)).list();

		return CollectionResponse.<Exitem> builder().setItems(execute).build();
	}

	@ApiMethod(name = "listEmpExitem", path = "listEmpExitem")
	public CollectionResponse<Exitem> listExiteme(User user,  @Nullable @Named("cursor") String cursorString, @Nullable @Named("limit") Integer limit) throws IllegalAccessException {

		if (user == null) {
			throw new IllegalAccessException("[Eep53] Attempt to retrieve exitems for an employee, but no user has been set (ie. not logged in and authed)");
		}
		Cursor cursor = null;
		// change 5, add orgn id to emp query

		/*
		 * deal with example@example.com
		 */
		User realUser = Sharder.mapUser(user);
		
		List<Exitem> execute = ofy().load().type(Exitem.class).filter("email ==", realUser.getEmail()).list();
		return CollectionResponse.<Exitem> builder().setItems(execute).build();
	}
	
	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getExitem")
	public Exitem getExitem(@Named("id") String id) {
		Exitem emp = (Exitem) ofy().load().key(Key.create(Exitem.class, id)).now(); // works
		return emp;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param emp the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertExitem")
	public Exitem insertExitem(User user, Exitem emp) {
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		if (user == null) {
			user = new User("empdemo@example.com", "domain");
		}
		// stuff the email into the exitem
		emp.setEmail(user.getEmail());
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param emp the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateExitem")
	public Exitem updateExitem(User user, Exitem emp) {
		// change 7, stuff orgn Id
		Sharder.shardFromUser((ShardedByOrgnId) emp, user);
		if (user == null) {
			user = new User("empdemo@example.com", "domain");
		}
		/*
		 * stuff an email into the item is it's blank
		 */
		if (emp.getEmail() == null) {
			emp.setEmail(user.getEmail());  			
		}
		ofy().save().entity(emp).now();
		return emp;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeExitem")
	public void removeExitem(User user, @Named("id") String id) {
		ofy().delete().key(Key.create(Exitem.class, id));
	}

	private boolean containsExitem(User user, Exitem emp) {
		boolean contains = true;
		Exitem item = (Exitem) ofy().load().key(Key.create(Exitem.class, emp.getId())).now();
		if (item == null) {
			contains = false;
		}
		return contains;
	}
}
