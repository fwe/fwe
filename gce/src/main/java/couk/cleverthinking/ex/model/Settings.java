/**
 * @copyright Roy Smith 2013
 */
package couk.cleverthinking.ex.model;

import java.math.BigDecimal;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Serialize;

/**
 * General settings
 * 
 * @author r
 *
 */
@Entity 
public class Settings implements ShardedByOrgnId{
	@Id
	String orgnId;
	String kmrate;
	String tags;
	String coname;
	String baseiso;
	String defaultVatRate;
	String defaultLang;
	String firstLogindate;


	
	public BigDecimal getKmrate() {
		if (kmrate == null) {
			kmrate = "0";
		}
		return new BigDecimal(kmrate);
	}
	public void setKmrate(BigDecimal kmrate) {
		this.kmrate = kmrate.toPlainString();
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public String getBaseiso() {
		return baseiso;
	}
	public void setBaseiso(String baseiso) {
		this.baseiso = baseiso;
	}
	@Override
	public String getOrgnId() {
		return orgnId;
	}
	@Override
	public void setOrgnId(String orgnId) {
		this.orgnId = orgnId;
	}
	public BigDecimal getDefaultVatRate() {
		if (defaultVatRate == null) {
			defaultVatRate = "0";
		}
		return new BigDecimal(defaultVatRate);
	}
	public void setDefaultVatRate(BigDecimal defaultVatRate) {
		this.defaultVatRate = defaultVatRate.toPlainString();
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public String getFirstLogindate() {
		return firstLogindate;
	}
	public void setFirstLogindate(String firstLogindate) {
		this.firstLogindate = firstLogindate;
	}

}
