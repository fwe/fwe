/**
 * @copyright 2013 Roy Smith
 */
package couk.cleverthinking.ex.tools;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.google.api.client.auth.oauth2.Credential;
import com.google.appengine.api.users.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.Exitem;
import couk.cleverthinking.ex.model.Fx;
import couk.cleverthinking.ex.model.Nominal;
import couk.cleverthinking.ex.model.Settings;
import couk.cleverthinking.ex.model.SettingsEndpoint;
import couk.cleverthinking.ex.oauth.CredentialMediatorB;
import couk.cleverthinking.ex.oauth.CredentialMediatorB.InvalidClientSecretsException;

/**
 * pull the config info into json strings
 * 
 * @author r
 *
 */
public class Config {

	

	public static String getSettingsAsJson(HttpServletRequest request) {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C21] no Emp stored in session");
			return null;
		}

		Settings settings = new SettingsEndpoint().getSettings(cnUser.getOrgnId());

		Gson gson = new GsonBuilder().create();
		//		String delme = gson.toJson(this).toString();
		String res = gson.toJson(settings).toString();
		BaseLog.fine("settings json = " + res);
		return res;
	}

	@SuppressWarnings("unchecked")
	public static String getFxAsJson(HttpServletRequest request) {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C21] no Emp stored in session");
			return null;
		}

		List<Fx> execute = ofy().load().type(Fx.class).filter("orgnId ==", cnUser.getOrgnId()).list();

		Gson gson = new GsonBuilder().create();
		//		String delme = gson.toJson(this).toString();
		String res = gson.toJson(execute).toString();
		BaseLog.fine("fxrates json = " + res);
		return res;
	}

	@SuppressWarnings("unchecked")
	public static String getEmpAsJson(HttpServletRequest request) {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C21] no Emp stored in session");
			return null;
		}

		List<Emp> execute = ofy().load().type(Emp.class).filter("orgnId ==", cnUser.getOrgnId()).list();

		Gson gson = new GsonBuilder().create();
		//		String delme = gson.toJson(this).toString();
		String res = gson.toJson(execute).toString();
		BaseLog.fine("Emp json = " + res);
		return res;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public static String getNominalAsJson(HttpServletRequest request) {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C21] no Emp stored in session");
			return null;
		}

		List<Nominal> execute = ofy().load().type(Nominal.class).filter("orgnId ==", cnUser.getOrgnId()).list();

		Gson gson = new GsonBuilder().create();
		//		String delme = gson.toJson(this).toString();
		String res = gson.toJson(execute).toString();
		BaseLog.fine("nominal json = " + res);
		return res;
	}
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	public static String getExitemsAllLiveAsJson(HttpServletRequest request) {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C21] no Emp stored in session");
			return null;
		}

		List<Exitem> execute = ofy().load().type(Exitem.class).filter("orgnId ==", cnUser.getOrgnId()).list();

		Gson gson = new GsonBuilder().create();
		//		String delme = gson.toJson(this).toString();
		String res = gson.toJson(execute).toString();
		BaseLog.fine("json = " + res);
		return res;
	}
	/**
	 * returns {a:{at
	 * @param request
	 * @return
	 * @throws InvalidClientSecretsException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 */
	public static String getCredsAsJson(HttpServletRequest request) throws InvalidClientSecretsException, IOException, IllegalAccessException {
		Emp cnUser = (Emp) request.getSession().getAttribute(Constants.SESSION_CNUSER);
		if (cnUser == null) {
			BaseLog.severe("[C180] no Emp stored in session");
			return null;
		}
		
		if (cnUser.getEmail().contains("@example.com")) {
			String res = "{\"c\":{\"a\":\"demoat\",\"em\":\"99999999999999\"},\"cm\":{\"a\":\"demoat\",\"em\":\"9999999999999999\"}}";
			return res;
		}
		/**
		 * make a user for the manager for credentialmediator to use
		 */
		Emp cnUserM = new Emp();
		cnUserM.setGoogleId(cnUser.getOrgnId());
		BaseLog.info("[C188] fetching creds for "+cnUser.getEmail()+":"+cnUser.getGoogleId()+" and mgr:"+cnUserM.getGoogleId());
		
		Credential c, cm;
		CredentialMediatorB cmb = new CredentialMediatorB(request);
		c = cmb.getStoredCredential(cnUser.getGoogleId());
		cm = cmb.getStoredCredential(cnUserM.getGoogleId());
		BaseLog.fine("[C194] found rt "+c.getRefreshToken()+"/"+cm.getRefreshToken());
		cmb.refreshtoken2accesstoken(cnUser, c.getRefreshToken(), c);
		cmb.refreshtoken2accesstoken(cnUser, cm.getRefreshToken(), cm);
		BaseLog.fine("[C197] got at "+c.getAccessToken()+"/"+cm.getAccessToken() + c.getExpirationTimeMilliseconds());
		BaseLog.fine("[C197] expiry in seconds "+(c.getExpirationTimeMilliseconds()-System.currentTimeMillis())/1000);
		String result = "{e:\""+cnUser.getEmail()+"\" ,\"c\":{\"a\":\""+c.getAccessToken()+"\",\"em\":\""+c.getExpirationTimeMilliseconds()+"\"},\"cm\":{\"a\":\""+cm.getAccessToken()+"\",\"em\":\""+cm.getExpirationTimeMilliseconds()+"\"}}";
		BaseLog.fine("[C200] "+result);
		return result;
	}

	public static void clearDownRse() {
		String orgnId = "116714976234014278794";
		ofy().delete().entities(ofy().load().type(Exitem.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Emp.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Settings.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Fx.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Nominal.class).filter("orgnId ==", orgnId).list());

		orgnId = "123";
		ofy().delete().entities(ofy().load().type(Exitem.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Emp.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Settings.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Fx.class).filter("orgnId ==", orgnId).list());
		ofy().delete().entities(ofy().load().type(Nominal.class).filter("orgnId ==", orgnId).list());
	}
}
