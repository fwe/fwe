package couk.cleverthinking.ex.tools;

import java.util.logging.*;

import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.api.xmpp.JID;
import com.google.appengine.api.xmpp.Message;
import com.google.appengine.api.xmpp.MessageBuilder;
import com.google.appengine.api.xmpp.MessageType;
import com.google.appengine.api.xmpp.XMPPService;
import com.google.appengine.api.xmpp.XMPPServiceFactory;
import com.google.appengine.api.datastore.Entity;

public class BaseLog {
	private static final String LOGNAME = "couk.cleverthinking.cnw";
	private static boolean invited = false;
	private static boolean logToXmpp = true;
	private static final XMPPService xmppService = XMPPServiceFactory
			.getXMPPService();

	public static void severe(String s) {
		Logger.getLogger(LOGNAME).log(Level.SEVERE, s);
		sendXmpp("SEV***: " + s);
	}

	public static void severe(String s, Exception ex) {
		displayStackTraceInformation(ex);
		Logger.getLogger(LOGNAME).log(Level.SEVERE, s);
		sendXmpp("SEV***: " + s);
	}

	public static void warning(String s) {
		Logger.getLogger(LOGNAME).log(Level.WARNING, s);
		sendXmpp("WARN: " + s);
	}

	public static void warning(String s, Exception ex) {
		displayStackTraceInformation(ex);
		Logger.getLogger(LOGNAME).log(Level.WARNING, s);
		sendXmpp("WARN: " + s);
	}

	public static void info(String s) {
		Logger.getLogger(LOGNAME).log(Level.INFO, s);
		sendXmpp("INFO: " + s);
	}

	public static void sendXmpp(String s) {
		if (!logToXmpp) {
			return;
		}
		if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Development) {
			logToXmpp = false;
			return;
		}
		try {
			Message message = new MessageBuilder()
					.withRecipientJids(new JID("roy.smith.esq@gmail.com"))
					.withMessageType(MessageType.NORMAL).withBody(s).build();
			;
			if (!invited) {
				xmppService.sendInvitation(new JID("roy.smith.esq@gmail.com"));
				invited = true;
			}
			XMPPServiceFactory.getXMPPService().sendMessage(message);
			// }
		} catch (Exception e) {
			e.printStackTrace();
			Logger.getLogger(LOGNAME).log(Level.WARNING,
					"[L65] jabber exception " + e);
		}
	}

	public static void fine(String s) {
		// System.out.println("1 in baselog fine with "+s);
		sendXmpp("fine: " + s);
		// System.out.println("2 in baselog fine with "+s);
		Logger.getLogger(LOGNAME).log(Level.FINE, s);
		// System.out.println("3 in baselog fine with "+s);
	}

	//
	// public static void info(String s) {
	// // System.out.println("1 in baselog fine with "+s);
	// sendXmpp("Info: " + s);
	// // System.out.println("2 in baselog fine with "+s);
	// Logger.getLogger(LOGNAME).log(Level.INFO, s);
	// // System.out.println("3 in baselog fine with "+s);
	// }
	//
	// public static void warn(String s) {
	// // System.out.println("1 in baselog fine with "+s);
	// sendXmpp("Warning: " + s);
	// // System.out.println("2 in baselog fine with "+s);
	// Logger.getLogger(LOGNAME).log(Level.WARNING, s);
	// // System.out.println("3 in baselog fine with "+s);
	// }
	public static String dumpEntity(Entity e) {
		return e.getProperties().toString();
	}

	public static void printStackTrace(String title, Exception e) {
		try {
			BaseLog.severe(title + e.toString());
			StackTraceElement[] se = e.getStackTrace();
			for (StackTraceElement ste : se) {
				BaseLog.severe(ste.toString());
			}
		} catch (Exception e1) {
		}
	}

	public static void disableXmpp() {
		logToXmpp = false;
	}

	@SuppressWarnings("null")
	public static void stackTrace(String title) {
		System.out.println("\n\n" + title);
		try {
			String rrrr = null;
			rrrr.length();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean displayStackTraceInformation(Throwable ex) {
		if (null == ex) {
			return false;
		}
		System.out.println("The stack according to printStackTrace():\n");
		ex.printStackTrace();
		System.out.println("");
		StackTraceElement[] stackElements = ex.getStackTrace();

		for (int lcv = 0; lcv < stackElements.length; lcv++) {
			System.out.println("Filename: " + stackElements[lcv].getFileName());
			System.out.println("Line number: "
					+ stackElements[lcv].getLineNumber());
			String className = stackElements[lcv].getClassName();
			String packageName = extractPackageName(className);
			String simpleClassName = extractSimpleClassName(className);
			if (packageName.indexOf("cleverthinking") > 0) {
				BaseLog.warning(stackElements[lcv].toString());
			}
			System.out.println("Package name: "
					+ ("".equals(packageName) ? "[default package]"
							: packageName));
			System.out.println("Full class name: " + className);
			System.out.println("Simple class name: " + simpleClassName);
			System.out.println("Direct class name: "
					+ extractDirectClassName(simpleClassName));
			System.out.println("Method name: "
					+ stackElements[lcv].getMethodName());
			System.out.println("Native method?: "
					+ stackElements[lcv].isNativeMethod());
			System.out.println("toString(): " + stackElements[lcv].toString());
			System.out.println("");
		}
		System.out.println("");
		return true;
	} // End of displayStackTraceInformation().

	public static String extractPackageName(String fullClassName) {
		if ((null == fullClassName) || ("".equals(fullClassName)))
			return "";
		int lastDot = fullClassName.lastIndexOf('.');
		if (0 >= lastDot)
			return "";

		return fullClassName.substring(0, lastDot);
	}

	public static String extractSimpleClassName(String fullClassName) {
		if ((null == fullClassName) || ("".equals(fullClassName)))
			return "";
		int lastDot = fullClassName.lastIndexOf('.');
		if (0 > lastDot)
			return fullClassName;

		return fullClassName.substring(++lastDot);
	}

	public static String extractDirectClassName(String simpleClassName) {
		if ((null == simpleClassName) || ("".equals(simpleClassName)))
			return "";
		int lastSign = simpleClassName.lastIndexOf('$');
		if (0 > lastSign)
			return simpleClassName;
		return simpleClassName.substring(++lastSign);
	}
}
