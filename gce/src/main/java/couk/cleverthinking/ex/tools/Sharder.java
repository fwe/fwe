/**
 * @copyright 2013 Roy Smith
 */
package couk.cleverthinking.ex.tools;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.google.appengine.api.users.User;

import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.model.ShardedByOrgnId;


/**
 * provides static methods to shard entities by organisation id
 * 
 * @author r
 *
 */
public class Sharder {
 
	/**
	 * use the email address from the User object to lookup the stored emp
	 * 
	 * @param entity
	 * @param user
	 * @param mgr
	 * @return the orgn id
	 * 
	 */
	public static String shardFromUser(ShardedByOrgnId entity, User user) {
		if (user == null || "example@example.com".equalsIgnoreCase(user.getEmail())) {
			BaseLog.warning("[S36] user is null so defaulting to rse");
			return shardFromEmail( entity,  "roy.smith.esq@gmail.com");
		}
		return shardFromEmail( entity,  user.getEmail());	
	}
	/**
	 * use the email to lookup the user, from there can extract the orgn id
	 * 
	 * @param entity
	 * @param email
 	 * @return the orgn id
	 */
	public static String shardFromEmail(ShardedByOrgnId entity, String email) {
//		String sql = "select from Emp as Emp where email = '"+email+"'";
//		System.out.println(sql);
		
		List<Emp> emps = ofy().load().type(Emp.class).filter("email ==", email).list();
		if (emps != null && emps.size() > 0) {
			return shardFromOrgnId(entity, emps.get(0).getOrgnId());			
		} else {
			return null;
		}
	}
	/**
	 * Simplest case, simply set orgn id into entity
	 * @param entity can be null
	 * @param orgnId
	 * 
	 * @return the orgn id
	 */
	
	public static String shardFromOrgnId(ShardedByOrgnId entity, String orgnId) {
		if (entity != null) {  // sometimes we only want the orgn Id, so entity may be null
			entity.setOrgnId(orgnId);
		}
		return orgnId;
	}
	

	/**
	 * Takes an appengine user and if it's example@example.com, converts it to r.s.e@gmail.com
	 * 
	 * @param user
	 * @return
	 */
	public static User mapUser(User user) {
		if ("example@example.com".equalsIgnoreCase(user.getEmail())) {
			return new User("roy.smith.esq@gmail.com", null);
		}
		return user;
	}
	
}