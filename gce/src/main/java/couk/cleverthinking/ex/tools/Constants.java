package couk.cleverthinking.ex.tools;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;


public class Constants {
	// set here https://cloud.google.com/console#/project/apps~cleverexpense/apiui/app/WEB/656116473673.apps.googleusercontent.com
//  public static final String WEB_CLIENT_ID = "141627708642.apps.googleusercontent.com";
	public static final String WEB_CLIENT_ID = "822097196702-jmapkqnikm7ql5mp4619748f33j7ev4l.apps.googleusercontent.com";
//	public static final String CLIENT_SECRET = "UF8_vPrxi4MG2Ce-MjuAgzdh";
	public static final String CLIENT_SECRET = "q_Iv3CVS1a-wWkvs0vaCZRs8";
//  public static final String ANDROID_CLIENT_ID = "2-android-apps.googleusercontent.com";
//  public static final String IOS_CLIENT_ID = "3-ios-apps.googleusercontent.com";
  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
  
  
  
  
  /*
   * from clevernote
   * 
   */
  
  /**
	 * Key of session variable to store user IDs.
	 */
	public static final String SESSION_USER_ID_KEY = "userId";

	/**
	 * Key of session variable to store user email addresses.
	 */
	public static final String SESSION_EMAIL_KEY = "emailAddress";

	// public static final String SESSION_CNSTORE="cnStore"; cannot be
	// serialized
	public static final String SESSION_CNUSER = "cnUser";
//	public static final String SESSION_URL = "url";
	public static final String SESSION_CREDENTIAL = "cred";
	public static final String SESSION_ACCESSTOKEN = "at";
	public static final String SESSION_ATEXPIRATION = "ate";
	public static final String SESSION_CLIENTID = "clientId";
	public static final String SESSION_SCOPES = "scopes";
	
	public static final String TASKLIST_NAME = "CleverNote";
	// stuff that we save in the request object - which used to be instance variables which
	// weren't thread safe
	protected static final String REQUEST_CNUSER = "CNUSER";
	protected static final String REQUEST_CREDENTIAL = "CREDENTIAL";
	
	
	/**
	 * Default MIME type of files created or handled by DrEdit.
	 * 
	 * This is also set in the Google APIs Console under the Drive SDK tab.
	 */
	public static final String DEFAULT_MIMETYPE = "text/plain";

	/**
	 * MIME type to use when sending responses back to DrEdit JavaScript client.
	 */
	public static final String JSON_MIMETYPE = "application/json";

	/**
	 * Path component under war/ to locate client_secrets.json file.
	 */

	public static final List<String> SCOPES = Arrays.asList(
			// Required to access and manipulate files.
//			"https://www.googleapis.com/auth/drive.file",
//			"https://www.googleapis.com/auth/drive.metadata.readonly",
//			"https://www.googleapis.com/auth/drive.readonly",
//			"https://www.googleapis.com/auth/drive",
//			"https://www.googleapis.com/auth/drive", ALL DRIVE FILES
			// Required to identify the user in our data store.
//			"https://www.googleapis.com/auth/userinfo.email",
//			"https://www.googleapis.com/auth/userinfo.profile",
//			"https://docs.googleusercontent.com/",
//			"https://docs.google.com/feeds/",
//			"https://www.googleapis.com/auth/drive.install",
//			"openid",
			"email",
			"https://www.googleapis.com/auth/calendar"
//			"email",
//			"https://www.googleapis.com/auth/tasks"
			);
	

	public static final HttpTransport TRANSPORT = new NetHttpTransport();
	public static final JsonFactory JSON_FACTORY = new JacksonFactory();

	public static final String SECRETS = "{  \"web\": {    \"client_id\": \""+WEB_CLIENT_ID+"\",    \"client_secret\": \""+CLIENT_SECRET+"\",    \"Zcomment\": \"The URLS below must be registered at https://code.google.com/apis/console/?pli=1#project:698624257995:access\",    \"Zorigredirect_uris\": [\"https://clever-note.appspot.com/sp/oauth2callback\",\"http://dev.clevernote.co:8888/sp/oauth2callback\"],    \"redirect_uris\": [\"https://clever-note.appspot.com/oauthcallback\",\"http://dev.clevernote.co:8888/oauthcallback\"],    \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",    \"token_uri\": \"https://accounts.google.com/o/oauth2/token\"  }}}";

	public static final String APP_HTML = "/manager.jsp";
	public static final String EMP_APP_HTML = "/emp.jsp";
	public static final String EMP_APP_M_HTML = "/emp-m.jsp";

	public static final int PEEK_LENGTH = 10000;  // upper length limit on a peek note
	public static final String DESCRIPTION_SEPARATOR = "\n-=CleverNoteObject=-\n";

	public static final String APPLICATION_NAME = "Clever Expense";
	public static final String aSIGNUP_HTML = "/app/signup.html";
	
	private static HashMap<String, String> urlMap = null;
	

		
/**
 * use the last component of the path to lookup the intended page (html or jsp) to redirect to
 * 
 * this can be called in two ways. 
 * Firstly it can be called from the main servlet (eg. B) which is what will happen if the user was already logged onto a session
 * Secondly, it can be called from, Oauthcallback, in which case we need to decode state to determine where to go next
 * 
 * @param req The full http request
 * @return
 */
	public static String getUrlFromPath(HttpServletRequest req) {
		System.out.println(req.getServletPath());
		System.out.println(req.getLocalAddr());
		System.out.println(req.getQueryString());

		/*
		 * set up the lookup map
		 */
		if (urlMap == null) {
			urlMap = new HashMap<String, String>();
			urlMap.put("b", "manager.jsp");
			urlMap.put("be", "emp.jsp");
			urlMap.put("bem", "emp-m.jsp");
			urlMap.put("manager", "manager.jsp");
			urlMap.put("emp", "emp.jsp");
			urlMap.put("empm", "empm.jsp");
		}
		
		
		String state = req.getParameter("state");
		if (state == null || state.length() == 0) {
			/*
			 * do main servlet logic
			 */
			String[] parts = req.getServletPath().split("/");
			String get = parts[parts.length-1];
			String got = urlMap.get(get);
			System.out.println("B looking for "+get+" got "+parts[1]+"/"+got);
			String result = "/"+parts[1]+"/"+got; 
			/* see if result was emp.jsp, and if so check for a mobile device */
			if (result.toLowerCase().contains("emp")) {
				result = check4mobile(req, result);
			}
			return result;
		} else {
			/*
			 * do oauthcallback logic. get the original URL from the session
			 */
			BaseLog.severe("DDD satet  = "+state);
			String url = URLDecoder.decode(state.replaceFirst(".*;", ""));
			BaseLog.severe("DDD decoded url = "+url);
			String[] parts = url.split("/");
			String get = url.replaceAll(".*/", "").replaceAll("\\?.*", "");
			String got = urlMap.get(get);
//			String path= state.replaceAll("^uu", "");
			String result = "/"+parts[parts.length-2]+"/"+got ;
			System.out.println("OCB looking for "+get+" got "+result);
			/* see if result was emp.jsp, and if so check for a mobile device */
			if (result.toLowerCase().contains("emp")) {
				result = check4mobile(req, result);
			}
			return result;
		}
	}
	
	/**
	 * checks the user agent header for a mobile device, and replaces . with m. 
	 * (eg emp.jsp -> empm.jsp)
	 * 
	 * If not mobile, just return result
	 * 
	 * @param result
	 * @return
	 */
	private static String check4mobile(HttpServletRequest req, String result) {
		String ua = req.getHeader("User-Agent");
		BaseLog.warning("ua = "+ua);
		if (ua.toLowerCase().contains("mobi") || ua.toLowerCase().contains("android") ) {
			result=result.replace(".", "m.");
		}
		return result;
	}


	/**
	 * encodes all of the state/url information required to let oauthcallback know what to do when it is called.
	 * 
	 * @param req
	 * @return state : the full called URL
	 */
	public static String encodeState(HttpServletRequest req, String state) {
//		String[] parts = req.getRequestURI().split("/"); 
		String query = req.getQueryString()==null ? "" : "?"+req.getQueryString();
		String result = state+";"+URLEncoder.encode(req.getRequestURL()+query); 
		BaseLog.severe("DDD "+result);
		return result;
	}
  
}