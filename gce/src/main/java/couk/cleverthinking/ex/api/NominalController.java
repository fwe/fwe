package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import org.restlet.data.Status;
import org.restlet.resource.ServerResource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import couk.cleverthinking.ex.api.FweAuth.Exception401;
import couk.cleverthinking.ex.model.Nominal;

public class NominalController extends ServerResource implements NominalControllerInterface {

	private static String allShardPin = "" + (int) (System.currentTimeMillis() % 10000);

	public NominalController() {
		System.out.println("------------- allshardpin = " + allShardPin);

		// i wanted to do auth here, but the queryvalue isn;t set so need to do in each method
		//		try {
		//			role = FweAuth.getRole(getQueryValue("access_token"));
		//		} catch (Exception401 e) {
		//			super.doError(new Status(401),e.toString());
		//		}
	}

	@Override
	public NominalContainer create(Nominal obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println("in create with at=" + getQueryValue("access_token"));
		System.out.println("saving " + obj.getMnemonicOrgnId());

		// force email and orgn from authed user
		obj.setMnemonicOrgnId(obj.getMnemonic()+role.getOrgnId());
		obj.setOrgnId(role.getOrgnId());

		ofy().save().entity(obj).now();

		System.out.println("inserted with id := " + obj.getMnemonicOrgnId());
		Nominal fetched2 = ofy().load().type(Nominal.class).id(obj.getMnemonicOrgnId()).now();
		System.out.println("refetched id " + fetched2.getMnemonicOrgnId());
		return wrapObject(fetched2);
	}

	@Override
	public NominalContainer getAllNominals() {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println(" in getall " + getRequestAttributes().get("id"));
		if (getRequestAttributes().get("id") != null) {
			return getOneNominal(Long.parseLong((String) getRequestAttributes().get("id")));
		}
		String shardLevel = getQueryValue("s"); // one of aLl, o, or e
		//		if (shardLevel == null || shardLevel.length() == 0) {
		//			shardLevel = "o"; // default is org to match other resources
		//		}
		NominalContainer content = new NominalContainer();
		;
		List<Nominal> objs = null;

		//		List<Nominal> execute = ofy().load().type(Nominal.class).filter("email ==", realobj.getEmail()).list();
		if (allShardPin.equals(shardLevel)) {
			System.out.println("q=all");
			objs = ofy().load().type(Nominal.class).list(); // all
		} else {
			if ("e".equals(shardLevel)) {
				System.out.println("q=emp");
				objs = ofy().load().type(Nominal.class).filter("email ==", role.getEmail()).list(); // emp
			} else {
				System.out.println("q=org");
				objs = ofy().load().type(Nominal.class).filter("orgnId ==", role.getOrgnId()).list(); // org (default)
			}
		}

		//		for (Nominal u : execute)
		//			objs.add(u);

		content.setitems(objs);

		return content;
	}

	public NominalContainer getOneNominal(Long id) {
		Nominal newobj = ofy().load().type(Nominal.class).id(id).now();
		if (newobj == null) {
			super.doError(new Status(401), "Requested id not found");
			return null;
		}
		return wrapObject(newobj);
	}

	/**
	 * Wraps an item in a Container object
	 * 
	 * @param newobj
	 * @return
	 */
	private NominalContainer wrapObject(Nominal obj) {
		NominalContainer content = new NominalContainer();
		;
		content.setitems(new ArrayList<Nominal>());
		content.getitems().add(obj);
		content.setHref(getRequest().getHostRef() + RestletDispatch.API_PREFIX + RestletDispatch.EXITEMS_URL + "/" + obj.getMnemonicOrgnId());
		return content;
	}

	@Override
	public NominalContainer update(Nominal obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//---------------------------------------
		String sid = getRequestAttributes().get("id").toString();
		System.out.println(" in update " + sid);

		Nominal savedObj = null;
		try {
			savedObj = ofy().load().type(Nominal.class).id(sid).safe();
		} catch (NotFoundException e) {
			return this.create(obj); // if not found, it becomes a new
			//			System.out.println("ex"+e);
			//			super.doError(new Status(404), "Requested id not found");
			//			return null;
		}
		//		Nominal newobj = (Nominal) ofy().load(). key(Key.create(Nominal.class, getRequestAttributes().get("id").toString())).now(); // works
		//		System.out.println(getRequest().getAttributes().keySet());
		//		try {
		//			Request r = getRequest();
		//			System.out.println();
		//		} catch (Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		if (newobj == null) {
		//			super.doError(new Status(404), "Requested id not found");
		//			return;
		//		}
		// when transcribing fields, do NOT transcribe email or orgn id
		if (obj.getDesc() != null) {
			savedObj.setDesc(obj.getDesc());
		}

		ofy().save().entity(savedObj).now();
		return wrapObject(savedObj);
	}

	@Override
	/**
	 * always returns 203, even if id wasn't found
	 */
	public void delete(Nominal obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(404), e.toString());
		}
		//---------------------------------------
		System.out.println(" in delete " + getRequestAttributes().get("id"));
		//		Nominal newobj = (Nominal) ofy().load().key(Key.create(Nominal.class, getRequestAttributes().get("id").toString())).now(); // works
		//		if (newobj == null) {
		//			System.out.println("not found");
		//			super.doError(new Status(404), "Requested id not found");
		//			return;
		//		}

		ofy().delete().key(Key.create(Nominal.class, getRequestAttributes().get("id").toString())).now();
		//		ofy().delete().key(Key.create(Nominal.class, Long.parseLong((String) getRequestAttributes().get("id")))).now();
		//		newobj = (Nominal) ofy().load().key(Key.create(Nominal.class, getRequestAttributes().get("id").toString())).now(); // works
		//		if (newobj != null) {
		//			System.out.println("still there!");
		//		}
	}

	public class NominalContainer {
		public String kind = "fwe#nom";
		public String href;
		public List<Nominal> items;

		public NominalContainer() {
			items = new ArrayList<Nominal>();
		}

		public NominalContainer(List<Nominal> items) {
			this.items = items;
		}

		public List<Nominal> getitems() {
			return items;
		}

		public void setitems(List<Nominal> items) {
			this.items = items;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}
	}
}