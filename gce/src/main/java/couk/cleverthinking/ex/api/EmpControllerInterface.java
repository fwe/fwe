package couk.cleverthinking.ex.api;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

import couk.cleverthinking.ex.api.EmpController.EmpContainer;
import couk.cleverthinking.ex.model.Emp;

public interface EmpControllerInterface {
 @Post
 EmpContainer create(Emp exitem);

 @Put
 EmpContainer update(Emp exitem);

 @Delete
 void delete(Emp exitem);

 @Get
 EmpContainer getAllEmps();
}
