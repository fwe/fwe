package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.ServerResource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import couk.cleverthinking.ex.api.FweAuth.Exception401;
import couk.cleverthinking.ex.model.Fx;

public class FxController extends ServerResource implements FxControllerInterface {

	private static String allShardPin = "" + (int) (System.currentTimeMillis() % 10000);

	public FxController() {
		System.out.println("------------- allshardpin = " + allShardPin);

		// i wanted to do auth here, but the queryvalue isn;t set so need to do in each method
		//		try {
		//			role = FweAuth.getRole(getQueryValue("access_token"));
		//		} catch (Exception401 e) {
		//			super.doError(new Status(401),e.toString());
		//		}
	}

	@Override
	public FxContainer create(Fx obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println("in create with at=" + getQueryValue("access_token"));
		System.out.println("saving " + obj.getIsoOrgnId());

		// force email and orgn from authed user
		obj.setIsoOrgnId(obj.getIso()+role.getOrgnId());
		obj.setOrgnId(role.getOrgnId());

		ofy().save().entity(obj).now();

		System.out.println("inserted with id := " + obj.getIsoOrgnId());
		Fx fetched2 = ofy().load().type(Fx.class).id(obj.getIsoOrgnId()).now();
		System.out.println("refetched id " + fetched2.getIsoOrgnId());
		return wrapObject(fetched2);
	}

	@Override
	public FxContainer getAllFxs() {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println(" in getall " + getRequestAttributes().get("id"));
		if (getRequestAttributes().get("id") != null) {
			return getOneFx(Long.parseLong((String) getRequestAttributes().get("id")));
		}
		String shardLevel = getQueryValue("s"); // one of aLl, o, or e
		//		if (shardLevel == null || shardLevel.length() == 0) {
		//			shardLevel = "o"; // default is org to match other resources
		//		}
		FxContainer content = new FxContainer();
		;
		List<Fx> objs = null;

		//		List<Fx> execute = ofy().load().type(Fx.class).filter("email ==", realobj.getEmail()).list();
		if (allShardPin.equals(shardLevel)) {
			System.out.println("q=all");
			objs = ofy().load().type(Fx.class).list(); // all
		} else {
			if ("e".equals(shardLevel)) {
				System.out.println("q=emp");
				objs = ofy().load().type(Fx.class).filter("email ==", role.getEmail()).list(); // emp
			} else {
				System.out.println("q=org");
				objs = ofy().load().type(Fx.class).filter("orgnId ==", role.getOrgnId()).list(); // org (default)
			}
		}

		//		for (Fx u : execute)
		//			objs.add(u);

		content.setitems(objs);

		return content;
	}

	public FxContainer getOneFx(Long id) {
		Fx newobj = ofy().load().type(Fx.class).id(id).now();
		if (newobj == null) {
			super.doError(new Status(401), "Requested id not found");
			return null;
		}
		return wrapObject(newobj);
	}

	/**
	 * Wraps an item in a Container object
	 * 
	 * @param newobj
	 * @return
	 */
	private FxContainer wrapObject(Fx obj) {
		FxContainer content = new FxContainer();
		;
		content.setitems(new ArrayList<Fx>());
		content.getitems().add(obj);
		content.setHref(getRequest().getHostRef() + RestletDispatch.API_PREFIX + RestletDispatch.EXITEMS_URL + "/" + obj.getIsoOrgnId());
		return content;
	}

	@Override
	public FxContainer update(Fx obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401), e.toString());
			return null;
		}
		//--------------------------------------
		
//		try {
//			System.out.println("json="+getRequestEntity().getText());
//			byte[] b = new byte[100];
//			System.out.println("json="+getRequestEntity().getStream().read(b));
//			System.out.println("json="+b.toString());
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		String sid = getRequestAttributes().get("id").toString();
		System.out.println(" in update " + sid);

		Fx savedObj = null;
		try {
			savedObj = ofy().load().type(Fx.class).id(sid).safe();
		} catch (NotFoundException e) {
			return this.create(obj); // if not found, it becomes a new
			//			System.out.println("ex"+e);
			//			super.doError(new Status(404), "Requested id not found");
			//			return null;
		}
		//		Fx newobj = (Fx) ofy().load(). key(Key.create(Fx.class, getRequestAttributes().get("id").toString())).now(); // works
		//		System.out.println(getRequest().getAttributes().keySet());
		//		try {
		//			Request r = getRequest();
		//			System.out.println();
		//		} catch (Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		if (newobj == null) {
		//			super.doError(new Status(404), "Requested id not found");
		//			return;
		//		}
		// when transcribing fields, do NOT transcribe email or orgn id
		if (obj.getRate() != null) {
			savedObj.setRate(obj.getRate());
		}

		ofy().save().entity(savedObj).now();
		return wrapObject(savedObj);
	}

	@Override
	/**
	 * always returns 203, even if id wasn't found
	 */
	public void delete(Fx obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(404), e.toString());
		}
		//---------------------------------------
		System.out.println(" in delete " + getRequestAttributes().get("id"));
		//		Fx newobj = (Fx) ofy().load().key(Key.create(Fx.class, getRequestAttributes().get("id").toString())).now(); // works
		//		if (newobj == null) {
		//			System.out.println("not found");
		//			super.doError(new Status(404), "Requested id not found");
		//			return;
		//		}

		ofy().delete().key(Key.create(Fx.class, getRequestAttributes().get("id").toString())).now();
		//		ofy().delete().key(Key.create(Fx.class, Long.parseLong((String) getRequestAttributes().get("id")))).now();
		//		newobj = (Fx) ofy().load().key(Key.create(Fx.class, getRequestAttributes().get("id").toString())).now(); // works
		//		if (newobj != null) {
		//			System.out.println("still there!");
		//		}
	}

	public class FxContainer {
		public String kind = "fwe#fx";
		public String href;
		public List<Fx> items;

		public FxContainer() {
			items = new ArrayList<Fx>();
		}

		public FxContainer(List<Fx> items) {
			this.items = items;
		}

		public List<Fx> getitems() {
			return items;
		}

		public void setitems(List<Fx> items) {
			this.items = items;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}
	}
}