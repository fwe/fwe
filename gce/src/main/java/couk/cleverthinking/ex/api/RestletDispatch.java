package couk.cleverthinking.ex.api;

//import java.security.Guard;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

//import org.restlet.security.Guard;

@SuppressWarnings("deprecation")
public class RestletDispatch extends Application {

	public static final String VERSION = "v1";
	public static final String EXITEMS_URL = "/"+VERSION+"/exitems";
	public static final String EMPS_URL = "/"+VERSION+"/emps";
	public static final String NOMS_URL = "/"+VERSION+"/noms";
	public static final String FXRATES_URL = "/"+VERSION+"/fxrates";
	public static final String SETTINGS_URL = "/"+VERSION+"/settings";
	public static final String API_PREFIX = "/api";
	@Override
	public synchronized Restlet createInboundRoot() {
		final Router router = new Router(getContext());
		router.attach("/user", CnUserController.class);
		router.attach(EXITEMS_URL, ExitemController.class);
		router.attach(EXITEMS_URL+"/{id}", ExitemController.class);
		router.attach(NOMS_URL, NominalController.class);
		router.attach(NOMS_URL+"/{id}", NominalController.class);
		router.attach(FXRATES_URL, FxController.class);
		router.attach(FXRATES_URL+"/{id}", FxController.class);
		router.attach(EMPS_URL, EmpController.class);
		router.attach(EMPS_URL+"/{id}", EmpController.class);
		router.attach(SETTINGS_URL, SettingsController.class);
		router.attach(SETTINGS_URL+"/{id}", SettingsController.class);
		return router;
//		ChallengeAuthenticator guard = new ChallengeAuthenticator(getContext(), ChallengeScheme.HTTP_OAUTH_BEARER, "Sample App"); 
//		guard.
////		[... complete the secrets, note guards can be more flexible ...] 
//
//		// Securize the whole hierarchy 
//
//		guard.setNext(router); 
//		// Return the guard as root of the hierachy of calls handlers. 
//		return guard; 
	}
	
}
//
//
//**** Add Guard filter **** 
//// Define your resources hierarchy as usual 
//Router router = new Router(this.getContext()); 
//router.attach("/items", ItemsResource.class); 
//router.attach("/item/{item_id}", ItemResource.class); 
//router.attach("/item/{item_id}/name/{name_id}", ItemResource.class); 

// Define your guard 
//Guard guard = new Guard(getContext(), ChallengeScheme.HTTP_BASIC, "Sample App"); 
//[... complete the secrets, note guards can be more flexible ...] 
//
//// Securize the whole hierarchy 
//
//guard.setNext(router); 
//// Return the guard as root of the hierachy of calls handlers. 
//return guard; 