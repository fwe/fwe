package couk.cleverthinking.ex.api;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

import couk.cleverthinking.ex.api.FxController.FxContainer;
import couk.cleverthinking.ex.model.Fx;

public interface FxControllerInterface {
 @Post
 FxContainer create(Fx exitem);

 @Put
 FxContainer update(Fx exitem);

 @Delete
 void delete(Fx exitem);

 @Get
 FxContainer getAllFxs();
}
