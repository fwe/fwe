package couk.cleverthinking.ex.api;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

import couk.cleverthinking.ex.api.ExitemController.ExitemContainer;
import couk.cleverthinking.ex.model.Exitem;

public interface ExitemControllerInterface {
 @Post
 ExitemContainer create(Exitem exitem);

 @Put
 ExitemContainer update(Exitem exitem);

 @Delete
 void delete(Exitem exitem);

 @Get
 ExitemContainer getAllExitems();
}
