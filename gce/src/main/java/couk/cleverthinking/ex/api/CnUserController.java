package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import org.restlet.resource.ServerResource;

import com.googlecode.objectify.ObjectifyService;

import couk.cleverthinking.ex.model.CnUser;

public class CnUserController extends ServerResource implements CnUserControllerInterface {

	public CnUserController() {
	}

	@Override
	public void create(CnUser user) {
		ObjectifyService.register(CnUser.class);

		CnUser tp = new CnUser();
		tp.setEmail(user.getEmail());
		tp.setFirstName(user.getFirstName());
		tp.setLastName(user.getLastName());
		ofy().save().entity(tp).now();
	}

	@Override
	public CnUserContainer getAllUsers() {
		CnUserContainer content = null;
		List users = new ArrayList();
//		List<CnUser> execute = ofy().load().type(Exitem.class).filter("email ==", realUser.getEmail()).list();
		List<CnUser> execute = ofy().load().type(CnUser.class).list();
		

		for (CnUser u : execute)
			users.add(u);

		content = new CnUserContainer();
		content.setUser_list(users);

		return content;
	}
}