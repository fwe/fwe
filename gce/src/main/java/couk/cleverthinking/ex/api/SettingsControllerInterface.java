package couk.cleverthinking.ex.api;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

import couk.cleverthinking.ex.api.SettingsController.SettingsContainer;
import couk.cleverthinking.ex.model.Settings;

public interface SettingsControllerInterface {
 @Post
 SettingsContainer create(Settings exitem);

 @Put
 SettingsContainer update(Settings exitem);

 @Delete
 void delete(Settings exitem);

 @Get
 SettingsContainer getAllSettingss();
}
