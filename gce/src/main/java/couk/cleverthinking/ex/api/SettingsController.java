package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import org.restlet.data.Status;
import org.restlet.resource.ServerResource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import couk.cleverthinking.ex.api.FweAuth.Exception401;
import couk.cleverthinking.ex.model.Settings;

public class SettingsController extends ServerResource implements SettingsControllerInterface {
	
	private static String allShardPin = "" +(int) (System.currentTimeMillis()%10000);
	public SettingsController() {
		System.out.println("------------- allshardpin = "+allShardPin);
	
		// i wanted to do auth here, but the queryvalue isn;t set so need to do in each method
//		try {
//			role = FweAuth.getRole(getQueryValue("access_token"));
//		} catch (Exception401 e) {
//			super.doError(new Status(401),e.toString());
//		}
	}

	@Override
	public SettingsContainer create(Settings obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println("in create with at="+getQueryValue("access_token"));
		System.out.println("saving "+obj.getOrgnId());
		
		// force email and orgn from authed user
		obj.setOrgnId(role.getOrgnId());
		
		ofy().save().entity(obj).now();
		

		System.out.println("inserted with id := "+obj.getOrgnId());
		Settings fetched2 = ofy().load().type(Settings.class).id(obj.getOrgnId()).now();
		System.out.println("refetched id "+fetched2.getOrgnId());
		return wrapObject(fetched2);
	}

	@Override
	public SettingsContainer getAllSettingss() {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println(" in getall "+getRequestAttributes().get("id"));
		if (getRequestAttributes().get("id") != null) {
			return getOneSettings(Long.parseLong((String) getRequestAttributes().get("id")));
		}
		String shardLevel = getQueryValue("s"); // one of aLl, o, or e
//		if (shardLevel == null || shardLevel.length() == 0) {
//			shardLevel = "o"; // default is org to match other resources
//		}
		SettingsContainer content = new SettingsContainer();;
		List<Settings> objs = null;
		
//		List<Settings> execute = ofy().load().type(Settings.class).filter("email ==", realobj.getEmail()).list();
		if (allShardPin.equals(shardLevel)) {
			System.out.println("q=all");
			objs = ofy().load().type(Settings.class).list();  // all
		} else {
			if ("e".equals(shardLevel)) {
				System.out.println("q=emp");
				objs = ofy().load().type(Settings.class).filter("email ==", role.getEmail()).list(); // emp
			} else {
				System.out.println("q=org");
				objs = ofy().load().type(Settings.class).filter("orgnId ==", role.getOrgnId()).list(); // org (default)
			}
		}
		

//		for (Settings u : execute)
//			objs.add(u);
		
		content.setitems(objs);

		return content;
	}

	public SettingsContainer getOneSettings(Long id) {
		Settings newobj = ofy().load().type(Settings.class).id(id).now();
		if (newobj == null) {
			super.doError(new Status(401), "Requested id not found");
			return null;			
		}
		return wrapObject(newobj);
	}

	/**
	 * Wraps an item in a Container object
	 * 
	 * @param newobj
	 * @return
	 */
	private SettingsContainer wrapObject(Settings obj) {
		SettingsContainer content = new SettingsContainer();;
		content.setitems(new ArrayList<Settings>());
		content.getitems().add(obj);
		content.setHref(getRequest().getHostRef()+RestletDispatch.API_PREFIX+RestletDispatch.EXITEMS_URL+"/"+obj.getOrgnId());
		return content;
	}

	@Override
	public SettingsContainer update(Settings obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------
		String sid = getRequestAttributes().get("id").toString();
		System.out.println(" in update "+sid);
		
		Settings savedObj =null;
		try {
			savedObj = ofy().load().type(Settings.class).id(sid).safe();
		} catch (NotFoundException e) {
			return this.create(obj);  // if not found, it becomes a new
//			System.out.println("ex"+e);
//			super.doError(new Status(404), "Requested id not found");
//			return null;
		}
//		Settings newobj = (Settings) ofy().load(). key(Key.create(Settings.class, getRequestAttributes().get("id").toString())).now(); // works
//		System.out.println(getRequest().getAttributes().keySet());
//		try {
//			Request r = getRequest();
//			System.out.println();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (newobj == null) {
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}
		// when transcribing fields, do NOT transcribe email or orgn id
		if (obj.getKmrate() != null) {
			savedObj.setKmrate(obj.getKmrate());
		}
		if (obj.getTags() != null) {
			savedObj.setTags(obj.getTags());
		}
		if (obj.getConame() != null) {
			savedObj.setConame(obj.getConame());
		}
		if (obj.getBaseiso() != null) {
			savedObj.setBaseiso(obj.getBaseiso());
		}
		if (obj.getDefaultVatRate() != null) {
			savedObj.setDefaultVatRate(obj.getDefaultVatRate());
		}
		if (obj.getDefaultLang() != null) {
			savedObj.setDefaultLang(obj.getDefaultLang());
		}
		
		
		ofy().save().entity(savedObj).now();
		return wrapObject(savedObj);
	}

	@Override
	/**
	 * always returns 203, even if id wasn't found
	 */
	public void delete(Settings obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(404),e.toString());
		}
		//---------------------------------------
		System.out.println(" in delete "+Long.parseLong((String) getRequestAttributes().get("id")));
//		Settings newobj = (Settings) ofy().load().key(Key.create(Settings.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj == null) {
//			System.out.println("not found");
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}

		ofy().delete().key(Key.create(Settings.class, Long.parseLong(getRequestAttributes().get("id").toString()))).now();
//		ofy().delete().key(Key.create(Settings.class, Long.parseLong((String) getRequestAttributes().get("id")))).now();
//		newobj = (Settings) ofy().load().key(Key.create(Settings.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj != null) {
//			System.out.println("still there!");
//		}
	}

	public class SettingsContainer {
		public String kind = "fwe#settings";
		public String href;
		public List<Settings> items;

		public SettingsContainer() {
			items = new ArrayList<Settings>();
		}

		public SettingsContainer(List<Settings> items) {
			this.items = items;
		}

		public List<Settings> getitems() {
			return items;
		}

		public void setitems(List<Settings> items) {
			this.items = items;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}
	}
}