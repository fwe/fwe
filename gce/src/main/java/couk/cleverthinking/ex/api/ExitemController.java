package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import org.restlet.data.Status;
import org.restlet.resource.ServerResource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import couk.cleverthinking.ex.api.FweAuth.Exception401;
import couk.cleverthinking.ex.model.Exitem;

public class ExitemController extends ServerResource implements ExitemControllerInterface {
	
	private static String allShardPin = "" +(int) (System.currentTimeMillis()%10000);
	public ExitemController() {
		System.out.println("------------- allshardpin = "+allShardPin);
	
		// i wanted to do auth here, but the queryvalue isn;t set so need to do in each method
//		try {
//			role = FweAuth.getRole(getQueryValue("access_token"));
//		} catch (Exception401 e) {
//			super.doError(new Status(401),e.toString());
//		}
	}

	@Override
	public ExitemContainer create(Exitem obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println("in create with at="+getQueryValue("access_token"));
		System.out.println("saving "+obj.getId());
		
		// force email and orgn from authed user
		obj.setEmail(role.getEmail());
		obj.setOrgnId(role.getOrgnId());
		
		ofy().save().entity(obj).now();
		

		System.out.println("inserted with id := "+obj.getId());
		Exitem fetched2 = ofy().load().type(Exitem.class).id(obj.getId()).now();
		System.out.println("refetched id "+fetched2.getId());
		return wrapObject(fetched2);
	}

	@Override
	public ExitemContainer getAllExitems() {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println(" in getall "+getRequestAttributes().get("id"));
		if (getRequestAttributes().get("id") != null) {
			return getOneExitem(Long.parseLong((String) getRequestAttributes().get("id")));
		}
		String shardLevel = getQueryValue("s"); // one of aLl, o, or e
//		if (shardLevel == null || shardLevel.length() == 0) {
//			shardLevel = "o"; // default is org to match other resources
//		}
		ExitemContainer content = new ExitemContainer();;
		List<Exitem> objs = null;
		
//		List<Exitem> execute = ofy().load().type(Exitem.class).filter("email ==", realobj.getEmail()).list();
		if (allShardPin.equals(shardLevel)) {
			System.out.println("q=all");
			if (!role.isManager()) {
				super.doError(new Status(403),"[EC85] request requires manager privilege");
				return null;
			}
			objs = ofy().load().type(Exitem.class).list();  // all
		} else {
			if ("e".equals(shardLevel)) {
				System.out.println("q=emp");
				objs = ofy().load().type(Exitem.class).filter("email ==", role.getEmail()).filter("orgnId ==", role.getOrgnId()).list(); // emp
			} else {
				System.out.println("q=org for "+role.getOrgnId());
				if (!role.isManager()) {
					super.doError(new Status(403),"[EC96] request requires manager privilege");
					return null;
				}
				objs = ofy().load().type(Exitem.class).filter("orgnId ==", role.getOrgnId()).list(); // org (default)
			}
		}
		

//		for (Exitem u : execute)
//			objs.add(u);
		
		content.setitems(objs);

		return content;
	}

	public ExitemContainer getOneExitem(Long id) {
		Exitem newobj = ofy().load().type(Exitem.class).id(id).now();
		if (newobj == null) {
			super.doError(new Status(401), "Requested id not found");
			return null;			
		}
		return wrapObject(newobj);
	}

	/**
	 * Wraps an item in a Container object
	 * 
	 * @param newobj
	 * @return
	 */
	private ExitemContainer wrapObject(Exitem obj) {
		ExitemContainer content = new ExitemContainer();;
		content.setitems(new ArrayList<Exitem>());
		content.getitems().add(obj);
		content.setHref(getRequest().getHostRef()+RestletDispatch.API_PREFIX+RestletDispatch.EXITEMS_URL+"/"+obj.getId());
		return content;
	}

	@Override
	public ExitemContainer update(Exitem obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------
		String sid = getRequestAttributes().get("id").toString();
		System.out.println(" in update "+Long.parseLong(sid));
		
		Exitem savedObj =null;
		try {
			savedObj = ofy().load().type(Exitem.class).id(Long.parseLong(sid)).safe();
		} catch (NotFoundException e) {
			return this.create(obj);  // if not found, it becomes a new
//			System.out.println("ex"+e);
//			super.doError(new Status(404), "Requested id not found");
//			return null;
		}
//		Exitem newobj = (Exitem) ofy().load(). key(Key.create(Exitem.class, getRequestAttributes().get("id").toString())).now(); // works
//		System.out.println(getRequest().getAttributes().keySet());
//		try {
//			Request r = getRequest();
//			System.out.println();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (newobj == null) {
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}
		// when transcribing fields, do NOT transcribe email or orgn id
		if (obj.getDesc() != null) {
			savedObj.setDesc(obj.getDesc());
		}
		if (obj.getStat() != null) {
			savedObj.setStat(obj.getStat());
		}
		if (obj.getAmount() != null) {
			savedObj.setAmount(obj.getAmount());
		}
		if (obj.getNet() != null) {
			savedObj.setNet(obj.getNet());
		}
		if (obj.getVat() != null) {
			savedObj.setVat(obj.getVat());
		}
		if (obj.getNom() != null) {
			savedObj.setNom(obj.getNom());
		}
		if (obj.getTags() != null) {
			savedObj.setTags(obj.getTags());
		}
		if (obj.getDateI() != null) {
			savedObj.setDateI(obj.getDateI());
		}
		if (obj.getDateP() != null) {
			savedObj.setDateP(obj.getDateP());
		}
		if (obj.getKm() >0) {
			savedObj.setKm(obj.getKm());
		}
		if (obj.getKmrate() != null) {
			savedObj.setKmrate(obj.getKmrate());
		}
		if (obj.getCurrency() != null) {
			savedObj.setCurrency(obj.getCurrency());
		}
		if (obj.getFx() != null) {
			savedObj.setFx(obj.getFx());
		}
		if (obj.getFxamount() != null) {
			savedObj.setFxamount(obj.getFxamount());
		}
		if (obj.getFxnet() != null) {
			savedObj.setFxnet(obj.getFxnet());
		}
		if (obj.getFxvat() != null) {
			savedObj.setFxvat(obj.getFxvat());
		}
		if (obj.getMcomment() != null) {
			savedObj.setMcomment(obj.getMcomment());
		}
		if (obj.getEcomment() != null) {
			savedObj.setEcomment(obj.getEcomment());
		}
		if (obj.getRurl() != null) {
			savedObj.setRurl(obj.getRurl());
		}
		if (obj.getAction() != null) {
			savedObj.setAction(obj.getAction());
		}
		if (obj.getType() != null) {
			savedObj.setType(obj.getType());
		}
		
		
		ofy().save().entity(savedObj).now();
		return wrapObject(savedObj);
	}

	@Override
	/**
	 * always returns 203, even if id wasn't found
	 */
	public void delete(Exitem obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(404),e.toString());
		}
		//---------------------------------------
		System.out.println(" in delete "+Long.parseLong((String) getRequestAttributes().get("id")));
//		Exitem newobj = (Exitem) ofy().load().key(Key.create(Exitem.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj == null) {
//			System.out.println("not found");
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}

		ofy().delete().key(Key.create(Exitem.class, Long.parseLong(getRequestAttributes().get("id").toString()))).now();
//		ofy().delete().key(Key.create(Exitem.class, Long.parseLong((String) getRequestAttributes().get("id")))).now();
//		newobj = (Exitem) ofy().load().key(Key.create(Exitem.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj != null) {
//			System.out.println("still there!");
//		}
	}

	public class ExitemContainer {
		public String kind = "fwe#exitem";
		public String href;
		public List<Exitem> items;

		public ExitemContainer() {
			items = new ArrayList<Exitem>();
		}

		public ExitemContainer(List<Exitem> items) {
			this.items = items;
		}

		public List<Exitem> getitems() {
			return items;
		}

		public void setitems(List<Exitem> items) {
			this.items = items;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}
	}
}