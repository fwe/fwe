package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import org.restlet.data.Status;
import org.restlet.resource.ServerResource;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;

import couk.cleverthinking.ex.api.FweAuth.Exception401;
import couk.cleverthinking.ex.model.Emp;

public class EmpController extends ServerResource implements EmpControllerInterface {
	
	private static String allShardPin = "" +(int) (System.currentTimeMillis()%10000);
	public EmpController() {
		System.out.println("------------- allshardpin = "+allShardPin);
	
		// i wanted to do auth here, but the queryvalue isn;t set so need to do in each method
//		try {
//			role = FweAuth.getRole(getQueryValue("access_token"));
//		} catch (Exception401 e) {
//			super.doError(new Status(401),e.toString());
//		}
	}

	@Override
	public EmpContainer create(Emp obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println("in create with at="+getQueryValue("access_token"));
		System.out.println("saving "+obj.getEmail());
		
		// force orgn from authed user
		obj.setOrgnId(role.getOrgnId());
		
		ofy().save().entity(obj).now();
		

		System.out.println("inserted with id := "+obj.getEmail());
		Emp fetched2 = ofy().load().type(Emp.class).id(obj.getEmail()).now();
		System.out.println("refetched id "+fetched2.getEmail());
		return wrapObject(fetched2);
	}

	@Override
	public EmpContainer getAllEmps() {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),e.toString());
			return null;
		}
		//---------------------------------------

		System.out.println(" in getall "+getRequestAttributes().get("id"));
		if (getRequestAttributes().get("id") != null) {
			return getOneEmp(Long.parseLong((String) getRequestAttributes().get("id")));
		}
		String shardLevel = getQueryValue("s"); // one of aLl, o, or e
//		if (shardLevel == null || shardLevel.length() == 0) {
//			shardLevel = "o"; // default is org to match other resources
//		}
		EmpContainer content = new EmpContainer();;
		List<Emp> objs = null;
		
//		List<Emp> execute = ofy().load().type(Emp.class).filter("email ==", realobj.getEmail()).list();
		if (allShardPin.equals(shardLevel)) {
			System.out.println("q=all");
			objs = ofy().load().type(Emp.class).list();  // all
		} else {
			if ("e".equals(shardLevel)) {
				System.out.println("q=emp");
				objs = ofy().load().type(Emp.class).filter("email ==", role.getEmail()).list(); // emp
			} else {
				System.out.println("q=org");
				objs = ofy().load().type(Emp.class).filter("orgnId ==", role.getOrgnId()).list(); // org (default)
			}
		}
		

//		for (Emp u : execute)
//			objs.add(u);
		
		content.setitems(objs);

		return content;
	}

	public EmpContainer getOneEmp(Long id) {
		Emp newobj = ofy().load().type(Emp.class).id(id).now();
		if (newobj == null) {
			super.doError(new Status(401), "Requested id not found");
			return null;			
		}
		return wrapObject(newobj);
	}

	/**
	 * Wraps an item in a Container object
	 * 
	 * @param newobj
	 * @return
	 */
	private EmpContainer wrapObject(Emp obj) {
		EmpContainer content = new EmpContainer();;
		content.setitems(new ArrayList<Emp>());
		content.getitems().add(obj);
		content.setHref(getRequest().getHostRef()+RestletDispatch.API_PREFIX+RestletDispatch.EXITEMS_URL+"/"+obj.getEmail());
		return content;
	}

	@Override
	public EmpContainer update(Emp obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(401),"[E133 ]"+ e.toString());
			return null;
		}
		//---------------------------------------
		
		Emp savedObj =null;
		try {
			String sid = getRequestAttributes().get("id").toString();
			System.out.println(" in update "+sid);
			savedObj = ofy().load().type(Emp.class).id(sid).safe();
		} catch (Exception e) {
			return this.create(obj);  // if not found, it becomes a new
//			System.out.println("ex"+e);
//			super.doError(new Status(404), "Requested id not found");
//			return null;
		}
//		Emp newobj = (Emp) ofy().load(). key(Key.create(Emp.class, getRequestAttributes().get("id").toString())).now(); // works
//		System.out.println(getRequest().getAttributes().keySet());
//		try {
//			Request r = getRequest();
//			System.out.println();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (newobj == null) {
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}
		// when transcribing fields, do NOT transcribe email or orgn id
		if (obj.getEmail() != null) {
			savedObj.setEmail(obj.getEmail());
		}
		if (obj.getName() != null) {
			savedObj.setName(obj.getName());
		}
		if (obj.getBank() != null) {
			savedObj.setBank(obj.getBank());
		}
		savedObj.setManager(obj.isManager());
		if (obj.getLang() != null) {
			savedObj.setLang(obj.getLang());
		}
		
		ofy().save().entity(savedObj).now();
		return wrapObject(savedObj);
	}

	@Override
	/**
	 * always returns 203, even if id wasn't found
	 */
	public void delete(Emp obj) {
		// ---- auth at top of each method -----
		FweRole role;
		try {
			role = FweAuth.getRole(getQueryValue("access_token"));
		} catch (Exception401 e) {
			super.doError(new Status(404),e.toString());
		}
		//---------------------------------------
		System.out.println(" in delete "+getRequestAttributes().get("id"));
//		Emp newobj = (Emp) ofy().load().key(Key.create(Emp.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj == null) {
//			System.out.println("not found");
//			super.doError(new Status(404), "Requested id not found");
//			return;
//		}

		ofy().delete().key(Key.create(Emp.class, Long.parseLong(getRequestAttributes().get("id").toString()))).now();
//		ofy().delete().key(Key.create(Emp.class, Long.parseLong((String) getRequestAttributes().get("id")))).now();
//		newobj = (Emp) ofy().load().key(Key.create(Emp.class, getRequestAttributes().get("id").toString())).now(); // works
//		if (newobj != null) {
//			System.out.println("still there!");
//		}
	}

	public class EmpContainer {
		public String kind = "fwe#emp";
		public String href;
		public List<Emp> items;

		public EmpContainer() {
			items = new ArrayList<Emp>();
		}

		public EmpContainer(List<Emp> items) {
			this.items = items;
		}

		public List<Emp> getitems() {
			return items;
		}

		public void setitems(List<Emp> items) {
			this.items = items;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getHref() {
			return href;
		}

		public void setHref(String href) {
			this.href = href;
		}
	}
}