/**
 * @author r
 * @copyright cleverthinking 2014
 *
 * 
 */
package couk.cleverthinking.ex.api;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.HashMap;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;

import couk.cleverthinking.ex.model.CnUser;
import couk.cleverthinking.ex.model.Emp;
import couk.cleverthinking.ex.oauth.CredentialMediatorB;

/**
 * 
 * handles all of the auth, sharding related issues
 * 
 *  Maintains a cached map of access tokens -> auth objects
 *
 */
public class FweAuth {

	// map of roles, keyed by access token
	private static HashMap<String, FweRole> rolesByToken = new HashMap<String, FweRole>();
	// map of access tokens keyed by email. This is used to detect a changed access token for a user so we can clear down the old tokens
	private static HashMap<String, String> tokensByEmail = new HashMap<String, String>();
	
	/**
	 * gets the role associated with the current access token
	 * 
	 * @param accessToken
	 * @return
	 * @throws Exception401 
	 */
	public static FweRole getRole(String accessToken) throws Exception401 {
//		makeCnUser("1@g.com","1");  //for testing only
		
		if (accessToken == null || accessToken.length() < 2) {
			throw new FweAuth().new Exception401("[FA41] missing access token");
		}
		
		FweRole role = rolesByToken.get(accessToken);
		if (role != null) {
			return role;
		}
		
		// here is there is no stored role for this token. 
		// This could be due to a new session, or a new access token for an existing session (in which case, clear down the old one)
		
		// get email from token by calling google
		Credential cred = new Credential(BearerToken.authorizationHeaderAccessMethod());
		cred.setAccessToken(accessToken);
		CredentialMediatorB cmb = new CredentialMediatorB();
		String email = null;
		try {
			email = cmb.getUserInfo(cred).getEmail();
		} catch (Exception e) {
			throw new FweAuth().new Exception401("[FA65] Google failed to return an email for the given access token");
		}
		if (email == null) {
			System.out.println("[FA68] Google failed to return an email for the given access token");
			throw new FweAuth().new Exception401("[FA69] Google failed to return an email for the given access token");
		}
		System.out.println("google found email "+email);
		
		// if there is an existing role for this email, store it, delete it from the role map, delete it from the email map
		role = rolesByToken.get(tokensByEmail.get(email));
		if (role != null) { // ie. existing role
			rolesByToken.remove(tokensByEmail.get(email));
			tokensByEmail.remove(email);
		} else { // no existing role (ie) new session
			// if there is no existing role, make one by retrieving CnUser and emp
			CnUser user = null;
			Emp emp = null;
			/* for now, let's go without cnuser
			try {
				List<CnUser> ulist = ofy().load().type(CnUser.class).filter("email ==", email).list();
				if (ulist.size()>0) {
					user = ulist.get(0);
				} else {
					throw new FweAuth().new Exception401("[FA86] Missing cnUser for email. ");
				}
			} catch (Exception ex) {
				throw new FweAuth().new Exception401("[FA90] Missing cnUser for email. Underlying exception was "+ex);				
			}
			*/
			try {
				emp = ofy().load().type(Emp.class).id(email).safe();
//				emp = ofy().load().type(Emp.class).filter("email ==", email).list().get(0);
			} catch (Exception ex) {
				throw  new FweAuth().new Exception401("[FA95] Missing emp for email. Underlying exception was "+ex);				
			}
			// if here, we have found the CnUser and emp for this email address
			role = new FweRole();
			role.setEmp(emp);
			role.setCnUser(user);
		}
		
		// here we have a role
		
		// store the role/email/token in the two maps
		rolesByToken.put(accessToken, role);
		tokensByEmail.put(email, accessToken);
		
		// return the role
		return role;
	}
	
	/**
	 * just for testingm make a user
	 * @param string
	 */
	private static void makeCnUser(String string, String id) {
		CnUser user = new CnUser();
		user.setEmail(string);
		user.setGoogleId(id);
		ofy().save().entity(user).now();
		// TODO Auto-generated method stub
		
	}

	
	public class Exception401 extends Throwable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String reason;

		public Exception401(String reason) {
			super();
			this.reason = reason;
		}
		
		public String toString() {
			return reason;
		}		
	}
}
