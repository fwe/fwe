package couk.cleverthinking.ex.api;

import org.restlet.resource.Get;
import org.restlet.resource.Put;


import couk.cleverthinking.ex.model.CnUser;

public interface CnUserControllerInterface {
 @Put
 void create(CnUser user);

 @Get
 CnUserContainer getAllUsers();

}
