package couk.cleverthinking.ex.api;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

import couk.cleverthinking.ex.api.NominalController.NominalContainer;
import couk.cleverthinking.ex.model.Nominal;

public interface NominalControllerInterface {
 @Post
 NominalContainer create(Nominal exitem);

 @Put
 NominalContainer update(Nominal exitem);

 @Delete
 void delete(Nominal exitem);

 @Get
 NominalContainer getAllNominals();
}
