package couk.cleverthinking.ex.api;

import couk.cleverthinking.ex.model.CnUser;
import couk.cleverthinking.ex.model.Emp;


/**
 * container class for the role related info required by each rest api call to auth and shard
 * 
 * @author r
 *
 */
public class FweRole {
	private CnUser cnUser;
	private Emp emp;
	
	/**
	 * gets isManager from emp
	 * @return
	 */
	public boolean isManager() {
		return emp.isManager();
	}
	/**
	 * gets email from emp object
	 * 
	 * @return
	 */
	public String getEmail() {
		return emp.getEmail();
	}
	/**
	 * gets google id from emp object
	 * 
	 * @return
	 */
	public String getGoogleId() {
		return emp.getGoogleId();
	}
	/**
	 * gets orgn id from emp object
	 * 
	 * @return
	 */
	public String getOrgnId() {
		return emp.getOrgnId();
	}

	public CnUser getCnUser() {
		return cnUser;
	}

	public void setCnUser(CnUser cnUser) {
		this.cnUser = cnUser;
	}

	public Emp getEmp() {
		return emp;
	}

	public void setEmp(Emp emp) {
		this.emp = emp;
	}
}