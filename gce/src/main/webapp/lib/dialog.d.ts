/**
 * 
 * definition file for angular dialog from https://github.com/m-e-conroy/angular-dialog-service
 * 
 */


/*
from http://angular-ui.github.io/bootstrap/

backdrop - controls presence of a backdrop. Allowed values: true (default), false (no backdrop), 'static' - backdrop is present but modal window is not closed when clicking outside of the modal window.
keyboard - indicates whether the dialog should be closable by hitting the ESC key, defaults to true
windowClass - additional CSS class(es) to be added to a modal window template
*/
interface AngularDialogOptions {
	keyboard: Boolean; // values: true,false
	backdrop: any; // values: 'static',true,false
	windowClass?: string
}
interface AngularDialogService {
	confirm: (title: string, body: string) => { result: ng.IPromise<any> };
	create: (viewFile: string, controller: string, data: {}, options: AngularDialogOptions) => { result: ng.IPromise<any> };
	notify: (title: string, body: string) => { result: ng.IPromise<any> };
}
