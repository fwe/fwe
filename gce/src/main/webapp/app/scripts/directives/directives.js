try  {
    window['exApp'].directive('uppercase', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                var capitalize = function (inputValue) {
                    if (!inputValue) {
                        inputValue = "";
                    }
                    var capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                };
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]); // capitalize initial value
            }
        };
    });

    window['exApp'].directive('fileChange', [
        function () {
            return {
                link: function (scope, element, attrs) {
                    element[0].onchange = function () {
                        console.log(scope[attrs]);
                        scope[attrs['fileChange']](element[0]);
                    };
                }
            };
        }
    ]);
} catch (ex) {
    console.log('[d38]' + ex);
}
//# sourceMappingURL=directives.js.map
