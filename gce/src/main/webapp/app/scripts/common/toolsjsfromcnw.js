var tools = {

	/**
	 * misc constants descriptionSeparator also in Factory
	 */
	Constants : { 
		peekMime: "text/plain",
		fnMime: "text/html",
		newEtag: "new-etag",
		quickNote : "qn",
		formattedNote : "f",
		logLevel : "4",
		lskeyTree : "tree",
		lskeyConfig : "localconfig",
		lskeyDriveQ : "driveq",
		lskeyDirty: "dirty",
		lskeyVirtual: "virtual",
		recent_fid: "-1",
		peek_fid: "-2",
		virtualNames : {recent_fid: "Recent", peek_fid:"Quick notes"},
		folderMime : "application/vnd.google-apps.folder",
		readFolderName : "Recently Read",
		writeFolderName : "Recently Updated",
		cnFolderName : "CleverNote",
		bundleFolderName : "Bundles",
		apiKey : "AIzaSyCt1bxTnrxo_IGvSUCBBAN_-29HJnzX_MU",
		descriptionSeparator : "\n-=CleverNoteObject=-\n",
		thumbnailTag : "cNw_tHuMbNaIl:",
		// tagSeparater : "==Tags==",
		tagPrefix : "Tag",
		evernotePrefix : "Imported-",
		editPopup : "editpopupAmin.html",
//		editPopup : "editpopupA.html",
		newNoteTitle : "New note",
		clientId : '698624257995',
		clientIdLong : '698624257995-1c2b2d4u54b57ff00mqu596iqn78lrnk.apps.googleusercontent.com',
//		apiKey : '698624257995',
		scopes : ['https://www.googleapis.com/auth/drive.file', 
		   				'email','openid',
		   				"https://www.googleapis.com/auth/drive.install", "https://www.googleapis.com/auth/tasks" ]
//		scopes : ['https://www.googleapis.com/auth/drive.file', 'openid',
//		   				'email', "https://docs.googleusercontent.com/", "https://docs.google.com/feeds/",
//		   				"https://www.googleapis.com/auth/drive.install", "https://www.googleapis.com/auth/tasks" ]
//		scopes : [ 'https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/drive.file', 'openid',
//				'email', "https://docs.googleusercontent.com/", "https://docs.google.com/feeds/",
//				"https://www.googleapis.com/auth/drive.install", "https://www.googleapis.com/auth/tasks" ]
	},

	/**
	 * Metadata prototype
	 * 
	 * now moved to Metadata class in CnModel
	 * 
	 * desc
	 * 
	 * -=CleverNoteObject=-
	 * {"urls":["https://groups.google.com/forum/?fromgroups=#!forum/google-apps-developer-challenge-2012"],"tags":["Tag:tag1"],"peek":"text"}
	 * 
	 */
	Metadata : function(note) {
		if (note) {
			var temp = (note.description ? note.description : "");
			var parts = temp.split(tools.Constants.descriptionSeparator);
			this.isPeek = false;
			this.description = parts[0];
			if (parts.length == 1) { // if no delimiter, return description and
				this.json = {};
			} else {
				this.json = angular.fromJson(parts[1]);
			}
			if (this.json.urls) {
				this.urls = this.json.urls;
			} else {
				this.urls = [];
			}
			if (this.description && this.description.length > 0) {
				this.isPeek = true;
			}
//			if (this.json.peek) {
////				console.log("TODO for testing, no encode on peek x2");
////				this.peek = (this.json.peek);
//				this.peek = tools.base64Decode(this.json.peek);
//			} else {
//				this.peek = undefined;
//			}
			this.tagwords = [];
			if (this.json.tags) {
				for ( var i = 0; i < this.json.tags.length; i++) {
					this.tagwords.push(this.json.tags[i].substring(tools.Constants.tagPrefix.length + 1));
				}
			}
		}
		/**
		 * 
		 * @returns the text String version of the metadata, suitable for saving to Drive 
		 */
		this.toString = function() {
			if (!this.urls) {
				console.error("[t95] trying to pack an empty metadata. Check that when new metadata(note) was called, that note was valid");
				return "";
			}
			var str = this.description;
			var tags = [];
			if (this.tagwords) {
				for ( var i = 0; i < this.tagwords.length; i++) {
					tags.push(tools.Constants.tagPrefix + ":" + this.tagwords[i]);
				}
			}
//			var peek64 = undefined;
//			if (this.peek) {
////				peek64=(this.peek); console.log("TODO for testing, no encode on peek x2");
//				peek64=tools.base64Encode(this.peek);
//			}
			temp = {
				urls : (this.urls.length > 0 ? this.urls : undefined),
				shortcut : this.shortcut,
				tags : (tags.length > 0 ? tags : undefined)
			};
			var j = angular.toJson(temp);
			if (j == "{}") {
				return str;
			}
			return this.description + tools.Constants.descriptionSeparator + j;
		};
	},

	/**
	 * Qobject prototype
	 * 
	 * @param type
	 *          (aka t) eg 'nf'
	 * @param uitxt
	 *          eg new folder foo
	 * 
	 * Only properties of the core object are persisted
	 */
	Qobject : function(type, uitxt) {
		this.core = {};
		this.core.t = type;
		this.core.uitxt = uitxt;
	},
	base64Encode : function(str) {
		var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		var out = "", i = 0, len = str.length, c1, c2, c3;
		while (i < len) {
			c1 = str.charCodeAt(i++) & 0xff;
			if (i == len) {
				out += CHARS.charAt(c1 >> 2);
				out += CHARS.charAt((c1 & 0x3) << 4);
				out += "==";
				break;
			}
			c2 = str.charCodeAt(i++);
			if (i == len) {
				out += CHARS.charAt(c1 >> 2);
				out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
				out += CHARS.charAt((c2 & 0xF) << 2);
				out += "=";
				break;
			}
			c3 = str.charCodeAt(i++);
			out += CHARS.charAt(c1 >> 2);
			out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
			out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
			out += CHARS.charAt(c3 & 0x3F);
		}
		return out;
	},

	// public method for decoding
	base64Decode : function(input) {
		var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = _keyStr.indexOf(input.charAt(i++));
			enc2 = _keyStr.indexOf(input.charAt(i++));
			enc3 = _keyStr.indexOf(input.charAt(i++));
			enc4 = _keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = tools._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for ( var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while (i < utftext.length) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}
		return string;
	},
	/**
	 * clones an object structure.
	 * 
	 * DANGER. DO NOT USE THIS TO CLONE A NOTE OBJECT AS THE CONSTRUCTOR WILL BE EXECUTED!!!
	 * 
	 * @param obj
	 *          the object to clone
	 * @param exclusions
	 *          an array of property names to exclude from the cloning
	 * @returns the cloned object
	 */
	clone : function(obj, exclusions) {
//		debugger; 
		if (obj == null || typeof (obj) != 'object')
			return obj;
		var temp = {};  // dfault is a blank object 
		if (obj instanceof Array) {
			temp=[];
		}

//		console.log(exclusions);

		for ( var key in obj) {
			if (exclusions) {
//				console.log("ex="+exclusions);
			}
			if (exclusions && (exclusions.indexOf(key) > -1)) {
				continue;
			}
//			console.log(temp);
//			console.log(key); 
			if (temp == undefined) {
//				debugger;
			}
			temp[key] = tools.clone(obj[key]);
		}
		return temp;
	}, 
	/**
	 * clones an object structure into a pre-existing object.
	 * 
	 * @param obj
	 *          the object to clone
	 * @param temp
	 *          the target object to clone into
	 * @param exclusions
	 *          an array of property names to exclude from the cloning
	 * @returns the cloned object
	 */
	cloneInto : function(obj, temp, exclusions) {
//		debugger; 
		if (obj == null || typeof (obj) != 'object')
			return obj;

//		console.log(exclusions);

		for ( var key in obj) {
			if (exclusions) {
//				console.log("ex="+exclusions);
			}
			if (exclusions && (exclusions.indexOf(key) > -1)) {
				continue;
			}
//			console.log(temp);
//			console.log(key); 
			if (temp == undefined) {
//				debugger;
			}
			temp[key] = tools.clone(obj[key]);
		}
		return temp;
	}, 

	/**
	 * finds the appropriate insert point for a (eg) note in an array (eg of
	 * notes)
	 * 
	 * @param newChild
	 *          the note
	 * @param parent
	 *          the array of notes
	 * @param comparator
	 *          the field to compare on eg. title
	 * @param order
	 *          a = ascending (eg title), d=descending (eg time)
	 * @param limit
	 *          the maximum number of entries in the array
	 * @returns undefined=insert at end (eg push), -1=don't insert coz full, else
	 *          insert point
	 */
	findSibling : function(newChild, parent, comparator, order, limit) {
		if (parent == null || parent == []) {

			return undefined;
		}
		for ( var i = 0; i < parent.length; i++) {
			if (i >= limit) { // quit if the array is full
				return -1;
			}
			// console.log(newChild[comparator]+"<"+parent[i][comparator]);
			if (order == 'a') {
				if (newChild[comparator] < parent[i][comparator]) {
					return i;
				}
			} else {
				if (newChild[comparator] > parent[i][comparator]) {
					return i;
				}
			}
		}
		if (i >= limit) { // quit if the array is full
			return -1;
		}
		return undefined;
	},

	/**
	 * finds the "eldest" sibling, defined as the node BEFORE which this new child
	 * should be inserted. This is done ALPHAbetically by name, READdate or
	 * WRITEdate
	 * 
	 * @param {Object}
	 *          parent
	 * @param {Object}
	 *          newChild
	 * @param {string}
	 *          mode ALPHA, READ or WRITE for sort purposes
	 * 
	 * @returns sibling node, add
	 */
	findSiblingZ : function(parent, newChild, mode) {
		if (!mode) {
			mode = "APLHA";
		}
		var firstNote = undefined;
		var siblings = parent.notes;
		if (!siblings) {
			return undefined;
		}
		for ( var i = 0; i < siblings.length; i++) {
			// cl(siblings[i].data.title + "<" + newChild.title);
			if (!firstNote && !siblings[i].data.isFolder) {
				firstNote = siblings[i]; // store the first child in case we
				// need
				// to append a folder
			}
			switch (mode) {
			case "ALPHA":
				if (siblings[i].data.title > newChild.title && siblings[i].data.isFolder == newChild.isFolder && siblings[i].data.title != Constants.readFolderName
						&& siblings[i].data.title != Constants.writeFolderName) {
					return siblings[i];
				}
				break;
			case "WRITE":
				var date = itemsMapById[siblings[i].data.key].modifiedDate;
				if (date < newChild.rDate) {
					return siblings[i];
				}
				break;
			case "READ":
				var date = itemsMapById[siblings[i].data.key].lastViewedByMeDate;
				if (date < newChild.rDate) {
					return siblings[i];
				}
				break;
			}
		}
		if (!newChild.isFolder) { // if no match set undefined to add to end
			// (ie
			// append)
			return undefined;
		}
		// folders are a slightly odd case
		return firstNote;
	},
	cl : function(object) {
		if (document.location.hash.indexOf("cLoG") == -1) {
			return;
		}
		try {
			console.debug(object);
//			var a;
//			a.foo();
		} catch (ex) {
			console.log(new Date());
			console.log(ex);
		}
	},
	/**
	 * translates an id (typically a temp Cn id) to its drive equivalent if
	 * there is no drive equivaent, returns the original id
	 * 
	 * works the other way round too. 
	 * 
	 * @param id
	 * @returns {*}
	 */
	fixedId : function(id) {
//		debugger;
		if (id.substring(0, 2) != "Cn") {  // if it's not a cn, then prob it's already a good id, so just return it
			return id;
		}
		if (window.cNote.idMap && window.cNote.idMap[id]) {
			return window.cNote.idMap[id];
		} else {
			return tools.oldCnId(id);
		}
	},
	/**
	 * the opposite of fixedId if there is no drive equivaent, returns the
	 * original id
	 * 
	 * @param id
	 * @returns {*}
	 */
	oldCnId : function(id) {
		if (window.cNote.idMap) {
			for ( var cnId in window.cNote.idMap) {
				if (window.cNote.idMap[cnId] == id) {
					return cnId;
				}
			}
		}
		return id;
	}

};


// ---------------------------- polyfills -----------------------------------------

if (typeof console == 'undefined'){
  console={};
  console.log=function(){return;};
  console.debug=function(){return;};
  console.error=function(){return;};
  console.warn=function(){return;};
  console.info=function(){return;};
}


/*
 * for IE 8
 */
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(searchElement /* , fromIndex */) {
		"use strict";
		if (this == null) {
			throw new TypeError();
		}
		var t = Object(this);
		var len = t.length >>> 0;

		if (len === 0) {
			return -1;
		}
		var n = 0;
		if (arguments.length > 1) {
			n = Number(arguments[1]);
			if (n != n) { // shortcut for verifying if it's NaN
				n = 0;
			} else if (n != 0 && n != Infinity && n != -Infinity) {
				n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}
		}
		if (n >= len) {
			return -1;
		}
		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) {
				return k;
			}
		}
		return -1;
	};
}
