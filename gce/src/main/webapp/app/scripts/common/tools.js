var Tools;
(function (Tools) {
    /**
    * clones an object structure.
    *
    * @param obj
    *          the object to clone
    * @param exclusions
    *          an array of property names to exclude from the cloning
    * @returns the cloned object
    */
    function clone(obj, exclusions) {
        if (typeof exclusions === "undefined") { exclusions = undefined; }
        //    debugger;
        if (obj == null || typeof (obj) != 'object')
            return obj;
        var temp = obj.constructor();
        if (!temp) {
            temp = {};
        }

        for (var key in obj) {
            if (exclusions) {
                //        console.log("ex="+exclusions);
            }
            if (exclusions && (exclusions.indexOf(key) > -1)) {
                continue;
            }

            //      console.log(temp);
            //      console.log(key);
            if (temp == undefined) {
                //        debugger;
            }
            temp[key] = this.clone(obj[key], undefined);
        }
        return temp;
    }
    Tools.clone = clone;

    /**
    * clones an object structure.
    *
    * @param obj
    *          the object to clone
    * @param exclusions
    *          an array of property names to exclude from the cloning
    * @returns the cloned object
    */
    function transcribe(obj, temp, exclusions) {
        if (typeof exclusions === "undefined") { exclusions = undefined; }
        //    debugger;
        if (obj == null || typeof (obj) != 'object')
            return obj;
        if (!temp) {
            temp = {};
        }

        for (var key in obj) {
            if (exclusions) {
                //        console.log("ex="+exclusions);
            }
            if (exclusions && (exclusions.indexOf(key) > -1)) {
                continue;
            }

            //      console.log(temp);
            //      console.log(key);
            if (temp == undefined) {
                //        debugger;
            }
            temp[key] = this.clone(obj[key], undefined);
        }
        return temp;
    }
    Tools.transcribe = transcribe;
})(Tools || (Tools = {}));
//# sourceMappingURL=tools.js.map
