
module Tools {

    /**
     * clones an object structure.
     * 
     * @param obj
     *          the object to clone
     * @param exclusions
     *          an array of property names to exclude from the cloning
     * @returns the cloned object
     */
     export function   clone(obj:any, exclusions:Array<any> = undefined) {
        //    debugger; 
        if (obj == null || typeof (obj) != 'object')
          return obj;
        var temp = obj.constructor(); // changed
        if (!temp) {
          temp={};
        }

        //    console.log(exclusions);

        for ( var key in obj) {
          if (exclusions) {
          //        console.log("ex="+exclusions);
          }
          if (exclusions && (exclusions.indexOf(key) > -1)) {
            continue;
          }
          //      console.log(temp);
          //      console.log(key); 
          if (temp == undefined) {
          //        debugger;
          }
          temp[key] = this.clone(obj[key], undefined);
        }
        return temp;
  }


     /**
     * clones an object structure.
     * 
     * @param obj
     *          the object to clone
     * @param exclusions
     *          an array of property names to exclude from the cloning
     * @returns the cloned object
     */
     export function   transcribe(obj:any, temp:any, exclusions:Array<any> = undefined) {
        //    debugger; 
        if (obj == null || typeof (obj) != 'object')
          return obj;
        if (!temp) {
          temp={};
        }

        //    console.log(exclusions);

        for ( var key in obj) {
          if (exclusions) {
          //        console.log("ex="+exclusions);
          }
          if (exclusions && (exclusions.indexOf(key) > -1)) {
            continue;
          }
          //      console.log(temp);
          //      console.log(key); 
          if (temp == undefined) {
          //        debugger;
          }
          temp[key] = this.clone(obj[key], undefined);
        }
        return temp;
  }
}
