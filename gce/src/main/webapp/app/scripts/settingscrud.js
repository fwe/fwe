/**
* (c) Roy Smith 2014
*
* Drive CRUD for AngularJS
*
*
*/
/// <reference path="services/driveResourceService.ts"/>
/// <reference path="services/fileReaderService.ts"/>
/// <reference path="../lib/angular.d.ts"/>
/// <reference path="../lib/gapi.d.ts"/>
/// <reference path="../lib/angular-resource.d.ts" />

var SettingscrudCtrl = (function () {
    function SettingscrudCtrl($scope, FileReaderService, ExResourceService) {
        this.dorigin = document.location['origin'];
        this.drivescope = "email https://www.googleapis.com/auth/drive.file";
        //	clientId = '700995682262-2kk81vdcu8j83j0ahjmk84u1drcbg5di.apps.googleusercontent.com'; // drivecrud
        this.clientId = '822097196702-jmapkqnikm7ql5mp4619748f33j7ev4l.apps.googleusercontent.com';
        this.listResults = [];
        //	refs:{scope:ng.IScope; driveResourceService: DriveResourceServiceModule.DriveResourceService};
        this.lang = "lang";
        this.q = "title contains 'qz' and trashed=false";
        this.maxResults = "100";
        //	static $inject = ['$scope', 'DriveResourceService'];
        //	constructor($scope, DriveResourceService) {
        //	static $inject = ['$scope', 'FileReaderService'];
        //	constructor($scope, FileReaderService) {
        console.log(ExResourceService);
        this.refs = { scope: $scope, exResourceService: ExResourceService, fileReaderService: FileReaderService };

        //		this.refs = {scope:$scope, driveResourceService: DriveResourceService};
        $scope.vm = this;

        $scope.online = true;
    }
    //	/**
    //	 * takes a list of parents and creates a JSON object in focussedItem.parents
    //	 * called onchange of the form field
    //	 */
    //	parentsListToJson(list?: string) {
    //		if (!list) {
    //			list = this.parentsList;
    //		}
    //		var listArray = list.replace(/ /g, "").split(",");
    //		this.focussedItem.parents = [];
    //		for (var i = 0; i < listArray.length; i++) {
    //			this.focussedItem.parents.push({ id: listArray[i] });
    //		}
    //	}
    //	/**
    //	 * takes a the JSON object in focussedItem.parents and strips it to a comma separated list in parentsList
    //	 */
    //	parentsJsonToList() {
    //		this.parentsList = "";
    //		for (var i = 0; i < this.focussedItem.parents.length; i++) {  		// foreach parent
    //			this.parentsList += ", " + this.focussedItem.parents[i].id;  	// append to string
    //		}
    //		this.parentsList = this.parentsList.substr(1);  // lose the first comma
    //	}
    /**
    * uses gapi to authorise the app using the scope provided by the input field
    * upon receipt of a token, passes it into driveResource to stuff into a header
    */
    SettingscrudCtrl.prototype.doAuth = function () {
        var _this = this;
        var scope = "https://www.googleapis.com/auth/drive";
        scope = document.getElementById('scope').getAttribute('value');
        console.log("authing " + this.clientId + " " + this.drivescope);
        gapi.auth.authorize({
            client_id: this.clientId,
            scope: [this.drivescope],
            immediate: false
        }, function () {
            console.log("authed " + gapi.auth.getToken().access_token);
            _this.refs.exResourceService.fixOauth();
            _this.haveToken = true;
            _this.refs.scope.$apply(function () {
                _this.haveToken = true;
            });
        });
    };

    /**
    * use the q input to generate and run a files.list
    */
    SettingscrudCtrl.prototype.doList = function () {
        var _this = this;
        this.driveActive = true;
        this.driveError = false;
        console.log("doList with q=" + this.q);
        this.refs.exResourceService.getAllSettings(this.listResults, this.q, this.maxResults, undefined, undefined).then(function () {
            _this.driveActive = false;
            console.log('fetched ' + _this.listResults.length + ' items');
        }, function () {
            _this.driveError = true;
        });
    };

    /**
    * called when a file is selected from the list
    */
    SettingscrudCtrl.prototype.doSelect = function (f) {
        this.focussedItem = f; // store in focussed Item (form)
        //		this.parentsJsonToList();			// unpack parents
    };

    /**
    * clears down the array of list results
    */
    SettingscrudCtrl.prototype.doClearResults = function () {
        this.listResults = [];
    };

    /**
    * called to update the current file
    */
    SettingscrudCtrl.prototype.doUpdate = function () {
        var _this = this;
        this.driveError = false;
        this.driveActive = true;
        this.refs.exResourceService.getSettingsResource().update({ 'id': this.focussedItem.orgnId }, this.focussedItem).$promise.then(function () {
            _this.driveActive = false;
        }, function () {
            _this.driveError = true;
        });
    };

    /**
    * called to inserta new file (NB no media content)
    */
    SettingscrudCtrl.prototype.doInsert = function () {
        var _this = this;
        this.driveError = false;
        this.driveActive = true;
        this.focussedItem.orgnId = undefined;
        this.refs.exResourceService.getSettingsResource().insert({}, this.focussedItem).$promise.then(function () {
            _this.driveActive = false;
        }, function () {
            _this.driveError = true;
        });
    };

    /**
    * called to delete a file (NB, not trash)
    */
    SettingscrudCtrl.prototype.doDelete = function () {
        var _this = this;
        this.driveError = false;
        this.driveActive = true;
        this.refs.exResourceService.getSettingsResource().remove({ 'id': this.focussedItem.orgnId }, {}).$promise.then(function () {
            _this.driveActive = false;
        }, function () {
            _this.driveError = true;
        });
    };
    SettingscrudCtrl.$inject = ['$scope', 'FileReaderService', 'ExResourceService'];
    return SettingscrudCtrl;
})();
//# sourceMappingURL=settingscrud.js.map
