/**
 * @copyright CleverThinking Ltd 2014
 */
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/angular-resource.d.ts" />
/**
 * serves drive resource objects
 * 
 * 
 */
module DriveResourceServiceModule {

	/**
	 * my window object to store auth related stuff
	 * 
	 * currently defined in driveq.js
	 */
	export interface IWinAuth {
		immediate: boolean; // sets the mode of any auth request, immediaetmeans background
		validToken: boolean; // true if the current token is valid
		tokenExpiryMilliseconds: number;
		gapiLoaded: boolean;  // true once gapi has fully loaded, set by onload=
		requestingToken: boolean; // used to stop retry loops around a login
		fetchToken:()=>void; // this is the way we get a new access token 
	}
	
	export interface IDriveChange {
		kind?: string;
		id?: number;
		fileId?: string;
		selfLink?: string;
		deleted?: boolean;
		file?: IDriveFile;
		modificationDate?: string
	}


	export interface IDriveFile {
		//		id?: string;
		//		title?: string;
		//		description?: string;
		//		$promise?: any;
		//		parents?: Array<any>;
		//		md5Checksum?: string;
		//		etag?: string;
		//		labels?: { trashed: boolean };
		//		mimeType?: string;
		$promise?: ng.IPromise<{}>;
		kind?: string;
		id?: string;
		etag?: string;
		selfLink?: string;
		webContentLink?: string;
		webViewLink?: string;
		alternateLink?: string;
		embedLink?: string;
		openWithLinks?: {};
		defaultOpenWithLink?: string;
		iconLink?: string;
		thumbnailLink?: string;
		thumbnail?: {
			image?: string;
			mimeType?: string
		};
		title?: string;
		mimeType?: string;
		description?: string;
		labels?: {
			starred?: boolean;
			hidden?: boolean;
			trashed?: boolean;
			restricted?: boolean;
			viewed?: boolean
		};
		createdDate?: string;
		modifiedDate?: string;
		modifiedByMeDate?: string;
		lastViewedByMeDate?: string;
		sharedWithMeDate?: string;
		parents?: Array<{ id: string }>;
		downloadUrl?: string;
		exportLinks?: {}
  		indexableText?: {
			text?: string
		};
		userPermission?: {
			kind?: string;
			etag?: string;
			id?: string;
			selfLink?: string;
			name?: string;
			emailAddress?: string;
			domain?: string;
			role?: string;
			additionalRoles?: Array<string>;
			type?: string;
			value?: string;
			authKey?: string;
			withLink?: boolean;
			photoLink?: string;
		}
  		originalFilename?: string;
		fileExtension?: string;
		md5Checksum?: string;
		fileSize?: number;
		quotaBytesUsed?: number;
		ownerNames?: Array<string>;
		owners?: Array<
		{
			kind?: string;
			displayName?: string;
			picture?: {
				url?: string
			};
			isAuthenticatedUser?: boolean;
			permissionId?: string
		}
		>;
		lastModifyingUserName?: string;
		lastModifyingUser?: {
			kind?: string;
			displayName?: string;
			picture?: {
				url?: string
			};
			isAuthenticatedUser?: boolean;
			permissionId?: string
		};
		editable?: boolean;
		copyable?: boolean;
		writersCanShare?: boolean;
		shared?: boolean;
		explicitlyTrashed?: boolean;
		appDataContents?: boolean;
		headRevisionId?: string;
		properties?: Array<{
			kind: string;
			etag: string;
			selfLink: string;
			key: string;
			visibility: string;
			value: string
		}>;
		imageMediaMetadata?: {
			width?: number;
			height?: number;
			rotation?: number;
			location?: {
				latitude?: number;
				numberitude?: number;
				altitude?: number
			};
			date?: string;
			cameraMake?: string;
			cameraModel?: string;
			exposureTime?: number;
			aperture?: number;
			flashUsed?: boolean;
			focalLength?: number;
			isoSpeed?: number;
			meteringMode?: string;
			sensor?: string;
			exposureMode?: string;
			colorSpace?: string;
			whiteBalance?: string;
			exposureBias?: number;
			maxApertureValue?: number;
			subjectDistance?: number;
			lens?: string
		}
	}


	/*
		Changes resource https://developers.google.com/drive/v2/reference/changes
		*/
	export interface IChangesResource extends ng.resource.IResource<IChangesResource> {
		largestChangeId: number; // NB. this is the same as About, ie NOT the largestin this set
		items: Array<IDriveChange>;
		$promise?: ng.IPromise<any>;
	}

	export interface IChangesResourceClass extends ng.resource.IResourceClass<IChangesResource> {
	}





	export interface IAboutResource extends ng.resource.IResource<IAboutResource> {
		largestChangeId: number;
		//    text: string;
		//    date: Date;
		//    author: number;

		// Although all actions defined on IAboutResourceClass are avaiable with
		// the '$' prefix, we have the choice to expose only what we will use
		//    $publish(): IAboutResource;
		//    $unpublish(): IAboutResource;
	}

	// Let's define a custom resource
	export interface IAboutResourceClass extends ng.resource.IResourceClass<IAboutResource> {
		// Overload get to accept our custom parameters
	}


	export interface IFilesResource extends ng.resource.IResource<IFilesResource> {
		//    title: string;
		//    text: string;
		//    date: Date;
		//    author: number;

		// Although all actions defined on IAboutResourceClass are avaiable with
		// the '$' prefix, we have the choice to expose only what we will use
		//    $publish(): IAboutResource;
		//    $unpublish(): IAboutResource;
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface IFilesResourceClass extends ng.resource.IResourceClass<IFilesResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}

	export interface IPromisedResult {
		promise: {}
	}

	export interface IDriveResourceService {
		oauthAccessTokenGetterFunction: () => string
		DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION: () => string
		refs: {}
		getFilesResource(): ng.resource.IResourceClass<ng.resource.IResource<any>>;
		getAboutResource(): IAboutResourceClass;
		insertFile(meta:IDriveFile, content:string);
		uploadMedia(id:string, mimeType:string, content:string);
		//		getAboutResource(): ng.resource.IResource<ng.resource.IResourceClass<any>>;
	}

	export class DriveResourceService implements IDriveResourceService {
		oauthAccessTokenGetterFunction: () => string;
		
		// define the default means of getting an access token
		DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION = ():string => {
			if (gapi.auth && gapi.auth.getToken() && gapi.auth.getToken().access_token) { // if it looks like we have a token in gapi
				return gapi.auth.getToken().access_token // return it
			} 
			// else (no token poss because we've emerged from a sleep)
			try { //cn use only
				if(window['auth']) {
					window['auth'].validToken = false;
					window['auth'].fetchToken();  // fetch a new one. this will take time, so return null for now
				}
			} catch (ex) {};
			
			return null;
		};
		DRIVE_URL = 'https://www.googleapis.com/drive/v2/'
		FILES_URL = this.DRIVE_URL + 'files';
		ABOUT_URL = this.DRIVE_URL + 'about';
		CHANGE_URL = this.DRIVE_URL + 'changes';
		refs: { q: ng.IQService; resourceService: ng.resource.IResourceService; http: ng.IHttpService };

		// constructor
		static $inject = ['$q', '$http', '$resource', '$log'];
		constructor($q, $http, $resource: ng.resource.IResourceService, $log) {		   
			  $http.defaults.useXDomain = true;

//			var oauthurl = "https://accounts.google.com/o/oauth2/auth?foo=bar&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.file+email+openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.install+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Ftasks&state=%2Fprofile&redirect_uri=http%3A%2F%2Fwww.clevernote.co%2Focb&response_type=token&client_id=698624257995.apps.googleusercontent.com&include_granted_scopes=true&login_hint=roy%2Esmith%2Eesq%40gmail%2Ecom";
//			 $http({method: 'GET', url: oauthurl}).
//    success(function(data, status, headers, config) {
//		console.log("3 success");
//    }).
//    error(function(data, status, headers, config) {
//		console.log("3 fail"+status+data);
//    });
			this.refs = { q: $q, resourceService: $resource, http: $http, log: $log };
		}

		/**
		 * sets the function to be called when we need an access token
		 * if called with no params, set default. This has the effect of suppressing teh warning message.
		 */
		setOauthAccessTokenGetterFunction(_oauthAccessTokenGetterFunction?: () => string) {
			var self = this;
			if (_oauthAccessTokenGetterFunction) {
				this.oauthAccessTokenGetterFunction = _oauthAccessTokenGetterFunction;
			} else {
				this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
			}
		}

		/**
		 * get a Files resource
		 * 
		 */
		getFilesResource(): IFilesResourceClass {
			this.fixOauth;
			var File = <IFilesResourceClass> this.refs.resourceService(this.FILES_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST' }, // custom update method
					update: { method: 'PUT' }, // custom update method
					list: { method: 'GET' }, // custom list method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}


		/**
		 * get the about resource
		 * 
		 */
		getAboutResource(): IAboutResourceClass {
			this.fixOauth();
			var About = <IAboutResourceClass> this.refs.resourceService(this.ABOUT_URL);
			return About;
		}


		/**
		 * get the changes resource
		 * 
		 */
		getChangesResource(): IChangesResourceClass {
			this.fixOauth();
			var Change = <IChangesResourceClass> this.refs.resourceService(this.CHANGE_URL);
			return Change;
		}


		/**
		  * get all files for user (subject to scope)
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllFiles(destArray: Array<any>, q: string, maxResults: string, self: any, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
				self = this;
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var files: ng.resource.IResourceClass<ng.resource.IResource<any>>;
			if (nextLink) {
				self.fixOauth();
				files = <ng.resource.IResourceClass<ng.resource.IResource<any>>> this['refs'].resourceService(nextLink);
			} else {
				files = this.getFilesResource();
			}
			files.get({ q: q , maxResults: maxResults}, function(data) { // do the

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					self['getAllFiles'](destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
		}







		/**
		 * Insert new file.
		 *https://developers.google.com/drive/v2/reference/files/insert
		
			stll gapi, but callback -> promise
		
		returns a IDriveFile object
		
		
		 *//*
		insertFile2(fileName, fileContent): IDriveFile {
			var driveFile: IDriveFile = {};  // make an empty file object
			this.fixOauth();

			var deferred = this.refs.q.defer();

			var boundary = '-------3141592ff65358979323846';
			var delimiter = "\r\n--" + boundary + "\r\n";
			var close_delim = "\r\n--" + boundary + "--";


			var contentType = 'text/plain';
			var metadata = {
				'title': fileName,
				'mimeType': contentType
			};

			var base64Data = btoa(fileContent);
			console.log("base54Data = " + base64Data);
			var multipartRequestBody =
				delimiter +
				'Content-Type: application/json\r\n\r\n' +
				JSON.stringify(metadata) +
				delimiter +
				'Content-Type: ' + contentType + '\r\n' +
				'Content-Transfer-Encoding: base64\r\n' +
				'\r\n' +
				base64Data +
				close_delim;

			var request = gapi.client.request({
				'path': '/upload/drive/v2/files',
				'method': 'POST',
				'params': { 'uploadType': 'multipart' },
				'headers': {
					'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
				},
				'body': multipartRequestBody
			});

			request.execute(function(file) {
				deferred['resolve'](file);
				console.log(11);
				console.log(file);
				//					driveFile = file; // dunno if this will work since it overwrites the original object ref
				if (window['tools']) {
					window['tools'].cloneInto(file, driveFile); // check if this is the correct approach. i suspect provided no in $scope, but rather vm., that it's OK
				} else {
					driveFile = file;
				}
				driveFile['foo'] = 'bar';
				driveFile.$promise = deferred.promise;
			});
			driveFile.$promise = deferred['promise'];
			return driveFile;
		}
*/

		/**
				 * Insert new file.
				 *https://developers.google.com/drive/v2/reference/files/insert
		
				this is the pure JS (no libraries) version from https://www.youtube.com/watch?v=l82yxb8AZ1Q
				and https://developers.google.com/drive/web/quickstart/quickstart-js
		
				returns a IDriveFile object
		
		
				 */
		/*
		insertFile3(fileName, fileContent): IDriveFile {
			var driveFile: IDriveFile = {};  // make an empty file object
			this.fixOauth();

			var deferred = this.refs.q.defer();

			var boundary = '-------3141592ff65358979323846';
			var delimiter = "\r\n--" + boundary + "\r\n";
			var close_delim = "\r\n--" + boundary + "--";


			var contentType = 'text/plain';
			var metadata = {
				'title': fileName,
				'mimeType': contentType
			};

			var base64Data = btoa(fileContent);
			console.log("base54Data = " + base64Data);
			var multipartRequestBody =
				delimiter +
				'Content-Type: application/json\r\n\r\n' +
				JSON.stringify(metadata) +
				delimiter +
				'Content-Type: ' + contentType + '\r\n' +
				'Content-Transfer-Encoding: base64\r\n' +
				'\r\n' +
				base64Data +
				close_delim;

			var xhr = new XMLHttpRequest();
			xhr.open('POST', 'https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart&alt=json', true);
			xhr.setRequestHeader('Authorization', 'Bearer ' + this.oauthAccessTokenGetterFunction());
			xhr.setRequestHeader('Content-type', 'multipart/mixed; boundary="' + boundary + "'");
			xhr.setRequestHeader('X-Upload-Content-Length', fileContent.length);
			xhr.setRequestHeader('X-Upload-Content-Type', contentType);


			xhr.onload = function(e) {
				var location = e.target.getResponseHeader('Location');
				this.url = location;
				this.sendFile_();
			}.bind(this);

			xhr.onload = function(file) {
				deferred['resolve'](file);
				console.log(file);
				//					driveFile = file; // dunno if this will work since it overwrites the original object ref
				if (window['tools']) {
					window['tools'].cloneInto(file, driveFile); // check if this is the correct approach. i suspect provided no in $scope, but rather vm., that it's OK
				} else {
					driveFile = file;
				}
				driveFile['foo'] = 'bar';
				driveFile.$promise = deferred.promise;
			};
			//xhr.onError=onError;
			xhr.send(multipartRequestBody);

			//		
			// add a promise to the returned drive file
			driveFile.$promise = deferred['promise'];
			return driveFile;
		}
*/

	
		/**
		 * Insert new file.	 *https://developers.google.com/drive/v2/reference/files/insert
		 * Does a multipart POST, so will create a new file
		 * 
		 * @param file meta data (eg. {title:"foo", mimeType:"text/html"})
		 * NB MIME MUST BE INCLUDED
		 * @param content. must be base64 encoded
		 */
		insertFile(fileMeta:IDriveFile, fileContent): IDriveFile {
			var self = this;
			var request:{data?:IDriveFile; $promise?:ng.IPromise<any>} = {}; 
//			var driveFile: IDriveFile = {};  // make an empty file object
			this.fixOauth();

			var deferred = this.refs.q.defer();

			var boundary = '-------3141592ff65358979323846';
			var delimiter = "\r\n--" + boundary + "\r\n";
			var close_delim = "\r\n--" + boundary + "--";

			if (!fileMeta.mimeType) {
				console.error("[drs560] file metadata is missing mandatory mime type");
				return;
			}

//			var base64Data = window['tools'].base64Encode(fileContent);
//			console.log("base54Data = " + base64Data);
			var multipartRequestBody =
				delimiter +
				'Content-Type: application/json\r\n\r\n' +
				JSON.stringify(fileMeta) +
				delimiter +
				'Content-Type: ' + fileMeta.mimeType + '\r\n' +
				'Content-Transfer-Encoding: base64\r\n' +
				'\r\n' +
				fileContent +
				close_delim;


			request.$promise = this.refs.http({
				method: fileMeta.id?'PUT':'POST',
				withCredentials: false,
				url: 'https://www.googleapis.com/upload/drive/v2/files'+(fileMeta.id?'/'+fileMeta.id:'')+'?uploadType=multipart&alt=json',
				//IMPORTANT!!! You might think this should be set to 'multipart/form-data' 
				// but this is not true because when we are sending up files the request 
				// needs to include a 'boundary' parameter which identifies the boundary 
				// name between parts in this multi-part request and setting the Content-type 
				// manually will not set this boundary parameter. For whatever reason, 
				// setting the Content-type to 'false' will force the request to automatically
				// populate the headers properly including the boundary parameter.
				headers: {
					'Authorization': 'Bearer ' + this.oauthAccessTokenGetterFunction(),
					'X-Upload-Content-Length': fileContent.length,
					'X-Upload-Content-Type': fileMeta.mimeType,
					'Content-Type': 'multipart/mixed; boundary="' + boundary + '"',
				},
				//This method will allow us to change how the data is sent up to the server
				// for which we'll need to encapsulate the model data in 'FormData'
				//Create an object that contains the model and files which will be transformed
				// in the above transformRequest method
				data: multipartRequestBody
			});
//			request.$promise['success'](function(response, status) {
//				self.cloneInto(response.data, driveFile, undefined);
//			});
//			driveFile.$promise = request.$promise;
			return request;
		}


		/**
		 * Insert new file.	 https://developers.google.com/drive/web/manage-uploads
		 * Does a multipart POST, so will create a new file
		 * 
		 * @param id of existing file
		 * @param content. must be base64 encoded
		 */
		uploadMedia(id:string, mimeType: string, fileContent): IDriveFile {
			var self = this;
			var driveFile: IDriveFile = {};  // make an empty file object
			this.fixOauth();

			var deferred = this.refs.q.defer();

			var boundary = '-------3141592ff65358979323846';
			var delimiter = "\r\n--" + boundary + "\r\n";
			var close_delim = "\r\n--" + boundary + "--";

			driveFile.$promise = this.refs.http({
				method: 'PUT',
				withCredentials: false,
				url: 'https://www.googleapis.com/upload/drive/v2/files/'+id+'?uploadType=media',
				//IMPORTANT!!! You might think this should be set to 'multipart/form-data' 
				// but this is not true because when we are sending up files the request 
				// needs to include a 'boundary' parameter which identifies the boundary 
				// name between parts in this multi-part request and setting the Content-type 
				// manually will not set this boundary parameter. For whatever reason, 
				// setting the Content-type to 'false' will force the request to automatically
				// populate the headers properly including the boundary parameter.
				headers: {
					'Authorization': 'Bearer ' + this.oauthAccessTokenGetterFunction(),
					'Upload-Content-Length': fileContent.length,
					'Upload-Content-Type': mimeType
				},
				//This method will allow us to change how the data is sent up to the server
				// for which we'll need to encapsulate the model data in 'FormData'
				//Create an object that contains the model and files which will be transformed
				// in the above transformRequest method
				data: fileContent
			});
			driveFile.$promise['success'](function(data, status) {
				self.cloneInto(data, driveFile, undefined);
			});
			return driveFile;
		}

		
		/**
		 * Insert new file.
		 *https://developers.google.com/drive/v2/reference/files/insert
		
			this is more or les the original, using callback and gapi
		
		
		 */
		/*
		insertFile1(fileName, fileContent, callback) {
			var boundary = '-------3141592ff65358979323846';
			var delimiter = "\r\n--" + boundary + "\r\n";
			var close_delim = "\r\n--" + boundary + "--";


			var contentType = 'text/plain';
			var metadata = {
				'title': fileName,
				'mimeType': contentType
			};

			var base64Data = btoa(fileContent);
			console.log("base54Data = " + base64Data);
			var multipartRequestBody =
				delimiter +
				'Content-Type: application/json\r\n\r\n' +
				JSON.stringify(metadata) +
				delimiter +
				'Content-Type: ' + contentType + '\r\n' +
				'Content-Transfer-Encoding: base64\r\n' +
				'\r\n' +
				base64Data +
				close_delim;

			var request = gapi.client.request({
				'path': '/upload/drive/v2/files',
				'method': 'POST',
				'params': { 'uploadType': 'multipart' },
				'headers': {
					'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
				},
				'body': multipartRequestBody
			});
			if (!callback) {
				callback = function(file) {
					console.log(file)
      		};
			}
			request.execute(callback);
		}
*/



		/**
		 * do whatever is needed to inject ouath
		 * this will prb need to become an injector which deals with 401
		 */
		fixOauth() {
//			console.log("adding oauth header");
			if (!this.oauthAccessTokenGetterFunction) {
				console.warn("[dRS336] You should call setOauthAccessTokenGetterFunction with a getter function. Using default " + this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION);
				this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
			}
			var at = this.oauthAccessTokenGetterFunction();
			this.refs.http.defaults.headers.common['Authorization'] = 'Bearer ' + at;
		}

		/**
	 * clones an object structure into a pre-existing object.
	 * 
	 * @param obj
	 *          the object to clone
	 * @param temp
	 *          the target object to clone into
	 * @param exclusions
	 *          an array of property names to exclude from the cloning
	 * @returns the cloned object
	 */
		cloneInto(obj, temp, exclusions) {
			//		debugger; 
			if (obj == null || typeof (obj) != 'object')
				return obj;

			//		console.log(exclusions);

			for (var key in obj) {
				if (exclusions) {
					//				console.log("ex="+exclusions);
				}
				if (exclusions && (exclusions.indexOf(key) > -1)) {
					continue;
				}
				//			console.log(temp);
				//			console.log(key); 
				if (temp == undefined) {
					//				debugger;
				}
				temp[key] = this.clone(obj[key], undefined);
			}
			return temp;
		}
		/**
 * clones an object structure.
 * 
 * @param obj
 *          the object to clone
 * @param exclusions
 *          an array of property names to exclude from the cloning
 * @returns the cloned object
 */
		clone(obj, exclusions) {
			//		debugger; 
			if (obj == null || typeof (obj) != 'object')
				return obj;
			var temp = obj.constructor(); // changed
			if (!temp) {
				temp = {};
			}

			//		console.log(exclusions);

			for (var key in obj) {
				if (exclusions) {
					//				console.log("ex="+exclusions);
				}
				if (exclusions && (exclusions.indexOf(key) > -1)) {
					continue;
				}
				//			console.log(temp);
				//			console.log(key); 
				if (temp == undefined) {
					//				debugger;
				}
				temp[key] = this.clone(obj[key], undefined);
			}
			return temp;
		}
	}
}
// declare var angular;
angular.module('exApp').service('DriveResourceService', DriveResourceServiceModule.DriveResourceService);
