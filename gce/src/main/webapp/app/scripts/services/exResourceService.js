var ExResourceServiceModule;
(function (ExResourceServiceModule) {
    

    

    

    

    

    

    var ExResourceService = (function () {
        function ExResourceService($q, $http, $resource, $log) {
            this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION = function () {
                console.log("in default fixouath");
                if (gapi.auth && gapi.auth.getToken() && gapi.auth.getToken().access_token) {
                    console.log("returning " + gapi.auth.getToken().access_token);
                    return gapi.auth.getToken().access_token;
                }

                try  {
                    if (window['auth']) {
                        window['auth'].validToken = false;
                        window['auth'].fetchToken();
                    }
                } catch (ex) {
                }
                ;
                console.log('returning null');
                return null;
            };
            this.FWE_URL = '/api/v1/';
            this.EXITEMS_URL = this.FWE_URL + 'exitems';
            this.EMPS_URL = this.FWE_URL + 'emps';
            this.NOMS_URL = this.FWE_URL + 'noms';
            this.FXRATES_URL = this.FWE_URL + 'fxrates';
            this.SETTINGS_URL = this.FWE_URL + 'settings';
            this.ABOUT_URL = this.FWE_URL + 'about';
            this.CHANGE_URL = this.FWE_URL + 'changes';
            $http.defaults.useXDomain = true;

            this.refs = { q: $q, resourceService: $resource, http: $http, log: $log };
        }
        ExResourceService.prototype.setOauthAccessTokenGetterFunction = function (_oauthAccessTokenGetterFunction) {
            var self = this;
            if (_oauthAccessTokenGetterFunction) {
                this.oauthAccessTokenGetterFunction = _oauthAccessTokenGetterFunction;
            } else {
                this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
            }
        };

        ExResourceService.prototype.getExitemsResource = function () {
            this.fixOauth();
            var File = this.refs.resourceService(this.EXITEMS_URL + "/:id", {}, {
                insert: { method: 'POST', params: { access_token: this.fixOauth() } },
                update: { method: 'PUT', params: { access_token: this.fixOauth() } },
                list: { method: 'GET', params: { access_token: this.fixOauth() } },
                remove: { method: 'DELETE', params: { access_token: this.fixOauth() } },
                upload: {
                    method: 'POST',
                    params: { uploadType: "multipart" },
                    headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
                }
            });

            return File;
        };

        ExResourceService.prototype.getAllExitems = function (destArray, q, maxResults, nextLink, deferred) {
            this.fixOauth();

            if (!deferred) {
                deferred = this.refs.q.defer();
            }

            var items;
            if (nextLink) {
                this.fixOauth();
                items = this['refs'].resourceService(nextLink);
            } else {
                items = this.getExitemsResource();
            }
            var qq = {};
            if (q) {
                qq = q;
            }
            items.list(qq, function (data) {
                destArray.push.apply(destArray, data['items']);

                if (data['nextLink']) {
                    this.getAllExitems(destArray, undefined, undefined, self, data['nextLink'], deferred);
                } else {
                    deferred['resolve'](self['allFiles']);
                }
            });

            return deferred['promise'];
        };

        ExResourceService.prototype.getNomsResource = function () {
            this.fixOauth();
            var File = this.refs.resourceService(this.NOMS_URL + "/:id", {}, {
                insert: { method: 'POST', params: { access_token: this.fixOauth() } },
                update: { method: 'PUT', params: { access_token: this.fixOauth() } },
                list: { method: 'GET', params: { access_token: this.fixOauth() } },
                remove: { method: 'DELETE', params: { access_token: this.fixOauth() } },
                upload: {
                    method: 'POST',
                    params: { uploadType: "multipart" },
                    headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
                }
            });

            return File;
        };

        ExResourceService.prototype.getAllNoms = function (destArray, q, maxResults, nextLink, deferred) {
            this.fixOauth();

            if (!deferred) {
                deferred = this.refs.q.defer();
            }

            var items;
            if (nextLink) {
                this.fixOauth();
                items = this['refs'].resourceService(nextLink);
            } else {
                items = this.getNomsResource();
            }
            items.list({}, function (data) {
                destArray.push.apply(destArray, data['items']);

                if (data['nextLink']) {
                    this.getAllNoms(destArray, undefined, undefined, self, data['nextLink'], deferred);
                } else {
                    deferred['resolve'](self['allFiles']);
                }
            });

            return deferred['promise'];
        };

        ExResourceService.prototype.getFxratesResource = function () {
            this.fixOauth();
            var File = this.refs.resourceService(this.FXRATES_URL + "/:id", {}, {
                insert: { method: 'POST', params: { access_token: this.fixOauth() } },
                update: { method: 'PUT', params: { access_token: this.fixOauth() } },
                list: { method: 'GET', params: { access_token: this.fixOauth() } },
                remove: { method: 'DELETE', params: { access_token: this.fixOauth() } },
                upload: {
                    method: 'POST',
                    params: { uploadType: "multipart" },
                    headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
                }
            });

            return File;
        };

        ExResourceService.prototype.getAllFxrates = function (destArray, q, maxResults, nextLink, deferred) {
            this.fixOauth();

            if (!deferred) {
                deferred = this.refs.q.defer();
            }

            var items;
            if (nextLink) {
                this.fixOauth();
                items = this['refs'].resourceService(nextLink);
            } else {
                items = this.getFxratesResource();
            }
            items.list({}, function (data) {
                destArray.push.apply(destArray, data['items']);

                if (data['nextLink']) {
                    this.getAllEmps(destArray, undefined, undefined, self, data['nextLink'], deferred);
                } else {
                    deferred['resolve'](self['allFiles']);
                }
            });

            return deferred['promise'];
        };

        ExResourceService.prototype.getEmpsResource = function () {
            this.fixOauth();
            var File = this.refs.resourceService(this.EMPS_URL + "/:id", {}, {
                insert: { method: 'POST', params: { access_token: this.fixOauth() } },
                update: { method: 'PUT', params: { access_token: this.fixOauth() } },
                list: { method: 'GET', params: { access_token: this.fixOauth() } },
                remove: { method: 'DELETE', params: { access_token: this.fixOauth() } },
                upload: {
                    method: 'POST',
                    params: { uploadType: "multipart" },
                    headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
                }
            });

            return File;
        };

        ExResourceService.prototype.getAllEmps = function (destArray, q, maxResults, nextLink, deferred) {
            this.fixOauth();

            if (!deferred) {
                deferred = this.refs.q.defer();
            }

            var items;
            if (nextLink) {
                this.fixOauth();
                items = this['refs'].resourceService(nextLink);
            } else {
                items = this.getEmpsResource();
            }
            items.list({}, function (data) {
                destArray.push.apply(destArray, data['items']);

                if (data['nextLink']) {
                    this.getAllEmps(destArray, undefined, undefined, self, data['nextLink'], deferred);
                } else {
                    deferred['resolve'](self['allFiles']);
                }
            });

            return deferred['promise'];
        };

        ExResourceService.prototype.getSettingsResource = function () {
            this.fixOauth();
            var File = this.refs.resourceService(this.SETTINGS_URL + "/:id", {}, {
                insert: { method: 'POST', params: { access_token: this.fixOauth() } },
                update: { method: 'PUT', params: { access_token: this.fixOauth() } },
                list: { method: 'GET', params: { access_token: this.fixOauth() } },
                remove: { method: 'DELETE', params: { access_token: this.fixOauth() } },
                upload: {
                    method: 'POST',
                    params: { uploadType: "multipart" },
                    headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
                }
            });

            return File;
        };

        ExResourceService.prototype.getAllSettings = function (destArray, q, maxResults, nextLink, deferred) {
            this.fixOauth();

            if (!deferred) {
                deferred = this.refs.q.defer();
            }

            var items;
            if (nextLink) {
                this.fixOauth();
                items = this['refs'].resourceService(nextLink);
            } else {
                items = this.getSettingsResource();
            }
            items.list({}, function (data) {
                destArray.push.apply(destArray, data['items']);

                if (data['nextLink']) {
                    this.getAllSettings(destArray, undefined, undefined, self, data['nextLink'], deferred);
                } else {
                    deferred['resolve'](self['allFiles']);
                }
            });

            return deferred['promise'];
        };

        ExResourceService.prototype.fixOauth = function () {
            console.log("adding oauth header");

            if (!this.oauthAccessTokenGetterFunction) {
                console.warn("[dRS336] You should call setOauthAccessTokenGetterFunction with a getter function. Using default " + this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION);
                this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
            }
            var at = this.oauthAccessTokenGetterFunction();
            this.refs.http.defaults.headers.common['Authorization'] = 'Bearer ' + at;
            return at;
        };

        ExResourceService.prototype.cloneInto = function (obj, temp, exclusions) {
            if (obj == null || typeof (obj) != 'object')
                return obj;

            for (var key in obj) {
                if (exclusions) {
                }
                if (exclusions && (exclusions.indexOf(key) > -1)) {
                    continue;
                }

                if (temp == undefined) {
                }
                temp[key] = this.clone(obj[key], undefined);
            }
            return temp;
        };

        ExResourceService.prototype.clone = function (obj, exclusions) {
            if (obj == null || typeof (obj) != 'object')
                return obj;
            var temp = obj.constructor();
            if (!temp) {
                temp = {};
            }

            for (var key in obj) {
                if (exclusions) {
                }
                if (exclusions && (exclusions.indexOf(key) > -1)) {
                    continue;
                }

                if (temp == undefined) {
                }
                temp[key] = this.clone(obj[key], undefined);
            }
            return temp;
        };
        ExResourceService.$inject = ['$q', '$http', '$resource', '$log'];
        return ExResourceService;
    })();
    ExResourceServiceModule.ExResourceService = ExResourceService;
})(ExResourceServiceModule || (ExResourceServiceModule = {}));

angular.module('exApp').service('ExResourceService', ExResourceServiceModule.ExResourceService);
//# sourceMappingURL=exResourceService.js.map
