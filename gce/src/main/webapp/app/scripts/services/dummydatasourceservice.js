/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="modelService.ts" />
/// <reference path="../common/tools.ts"/>
/*
* (c) RoySmith 2013
*/
var DatasourceModule;
(function (DatasourceModule) {
    //	export interface IDatasourceService {
    //		getLive:(arry:Array<Model.Exitem>, bundle:{})=>void;
    //		getSettings:(obj:any) =>void;
    //		getEmp:() =>{};
    //		getNoms:() =>{};
    //		getFxrates:() =>{};
    //		saveSettingsChanges:(obj:{})=>void;
    //		putUpdatedItem:(item:Model.Exitem)=>void;
    // getNewZ :()=>string[];
    // saveNew :()=>void
    //	}
    var DummyDatasourceService = (function () {
        // newArrayZ = ["q","b"];
        function DummyDatasourceService() {
            this.liveArray = [];
            this.newArray = [];
            console.log();
        }
        DummyDatasourceService.prototype.putUpdatedItem = function (item) {
            console.log("dummy datasource putUpdatedItem ...");
            console.log(item);
        };

        DummyDatasourceService.prototype.getSettings = function () {
            var s = new Model.Settings();
            s.kmrate = 25;
            s.tags = "proj1,proj2";
            s.baseiso = "GBP";
            s.coname = "cleverthinking";
            return s;
        };

        /**
        * deletes an item from the server
        */
        DummyDatasourceService.prototype.deleteItem = function (item) {
            //            this.httpDelete("/_ah/api/exitemendpoint/v1/exitem", item,  false);
        };

        DummyDatasourceService.prototype.getEmp = function () {
            var emparray = new Array();

            var s = new Model.Emp();
            s.email = "fred@foo.com";
            s.name = "Fred Bloggs";
            s.bank = "12345678 12-23-45";
            s.manager = true;
            emparray.push(Tools.clone(s));

            s.email = "joe@foo.com";
            s.name = "Joe Soap";
            s.bank = "12345678 12-23-45";
            s.manager = false;
            emparray.push(Tools.clone(s));

            return emparray;
        };
        DummyDatasourceService.prototype.getNoms = function () {
            var nomarray = new Array();

            var s = new Model.Nominal();
            s.mnemonic = "TEL";
            s.desc = "Telephone";
            nomarray.push(Tools.clone(s));

            var s = new Model.Nominal();
            s.mnemonic = "KM";
            s.desc = "Fuel mileage";
            nomarray.push(Tools.clone(s));

            var s = new Model.Nominal();
            s.mnemonic = "MEA";
            s.desc = "Meals";
            nomarray.push(Tools.clone(s));

            var s = new Model.Nominal();
            s.mnemonic = "HOT";
            s.desc = "Hotel and accommodation";
            nomarray.push(Tools.clone(s));

            return nomarray;
        };

        DummyDatasourceService.prototype.getFxrates = function () {
            var fxarray = new Array();

            var s = new Model.Fx();
            s.iso = "THB";
            s.rate = 1.2;
            fxarray.push(Tools.clone(s));

            var s = new Model.Fx();
            s.iso = "PHP";
            s.rate = 1.3;
            fxarray.push(Tools.clone(s));

            var s = new Model.Fx();
            s.iso = "EUR";
            s.rate = 0.6;
            fxarray.push(Tools.clone(s));

            var s = new Model.Fx();
            s.iso = "USD";
            s.rate = 1.0;
            fxarray.push(Tools.clone(s));

            return fxarray;
        };

        // getNewZ() {
        // 	return this.newArrayZ;
        // }
        DummyDatasourceService.prototype.getLive = function (force, arry, bundle) {
            var live = [];

            var item = new Model.Exitem();
            item.id = "99";
            item.desc = "description of expense";
            item.email = "john@bar.com";
            item.stat = "N";
            item.amount = 120;
            item.net = 100;
            item.action = "A";
            item.vat = 20;
            arry.push(Tools.clone(item));
            item.id = "98";
            arry.push(Tools.clone(item));
            item.id = "97";
            arry.push(Tools.clone(item));
            item.id = "96";
            item.stat = "R";
            arry.push(Tools.clone(item));
            console.log(live);
            if (bundle) {
                console.log("calling callback");
                setTimeout(function () {
                    bundle['callback'](bundle['parent']);
                }, 100);
            }
            return;
        };

        // saveNew() {
        // 	for (var i = 0; i< this.newArray.length; i++) {
        // 		console.log("TODO: update live array, recreate newArray and send updates to server");
        // 	}
        // }
        /**
        * called witha changesContainer object
        */
        DummyDatasourceService.prototype.saveSettingsChanges = function (obj) {
            console.log("dummyDatasource is saving ...");

            console.log(obj);
        };
        return DummyDatasourceService;
    })();
    DatasourceModule.DummyDatasourceService = DummyDatasourceService;
})(DatasourceModule || (DatasourceModule = {}));

// declare var angular;
angular.module('exApp').service('DummyDatasourceService', DatasourceModule.DummyDatasourceService);
//# sourceMappingURL=dummydatasourceservice.js.map
