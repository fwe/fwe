/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="modelService.ts" />
/// <reference path="../common/tools.ts"/>
/*
* (c) RoySmith 2013
*/

module DatasourceModule {

	export interface IDatasourceService {
		getLive:(force:boolean, arry:Array<Model.Exitem>, bundle:{})=>void;
		getSettings:(obj:any) =>void;
		getEmp:(arry:Array<Model.Emp>)=>void;
		getNoms:(arry:Array<Model.Nominal>)=>void;
		getFxrates:(arry:Array<Model.Fx>)=>void;
		saveSettingsChanges:(obj:{})=>void;
		putUpdatedItem:(item:Model.Exitem)=>void;
		deleteItem:(item:Model.Exitem)=>void;
		// getNewZ :()=>string[];
		// saveNew :()=>void
	}

	export class HttpDatasourceService implements IDatasourceService{


		liveArray = [];
		newArray:{}[] = [];
		calendarArray:{}[];
		evArray:{}[];
		recentEventsArray:{}[];

		http;
		timeout;
		window;

        host;
		// newArrayZ = ["q","b"];


		static $inject = ['$http', '$timeout', '$window'];
		constructor ($http, $timeout, $window) {
			console.log("constructor2");
			this.http = $http;
			this.timeout = $timeout;
			this.window = $window;

			// fetch the recent events into an array
			if (!window['clexunittesting']) {
		    	this.fetchRecentCalendarEvents();
			}

            /* set the default host to be the https on appspot. unless testing server (ie dev.clevernote.co:8888) */
            this.host = "https://gcdc2013-freewebexpenses.appspot.com";
            if (window.location.host.indexOf(":") > -1) {
                this.host = "http://"+window.location.host;
            }
		}


		getSettings(obj:any):void {
			if (this.window.config && this.window.config.settings) {
				this.timeout(function () {
				window['Tools'].transcribe(this.window.config.settings, obj);
					},10)

				return;
			}
			var url="/_ah/api/settingsendpoint/v1/settings";
            url = this.host + url;
			this.httpGetObject(url,obj,false);
		}
		
		getEmp(arry:Array<Model.Emp>) {
			if (this.window.config && this.window.config.emp) {
				this.timeout(function () {
					for (var i = 0; i<this.window.config.emp.length; i++) {
						arry.push(this.window.config.emp[i]);
						console.log(this.window.config.emp[i]);
					}
					},10)

				return;
			}
			var url="/_ah/api/empendpoint/v1/emp";
            url = this.host + url;
			this.httpGetArray(url,arry,false);
		}
		getNoms(arry:Array<Model.Nominal>) {
			if (this.window.config && this.window.config.nominal) {
				this.timeout(function () {
					for (var i = 0; i<this.window.config.nominal.length; i++) {
						arry.push(this.window.config.nominal[i]);
					}
				},10)

				return;
			}
			var url="/_ah/api/nominalendpoint/v1/nominal";
            url = this.host + url;
            this.httpGetArray(url,arry,false);
		}
		getFxrates(arry:Array<Model.Fx>) {
			if (this.window.config && this.window.config.fx) {
				this.timeout(function () {
					for (var i = 0; i<this.window.config.fx.length; i++) {
						arry.push(this.window.config.fx[i]);
					}
				},10)

				return;
			}
			var url="/_ah/api/fxendpoint/v1/fx";
            url = this.host + url;

            this.httpGetArray(url,arry,false);
		}



		// getNoms() {
		// 	var nomarray = new Array();

		// 	var s = new Model.Nominal();
		// 	s.mnemonic = "TEL";
		// 	s.desc = "Telephone";
		// 	nomarray.push(Tools.clone(s));

		// 	var s = new Model.Nominal();
		// 	s.mnemonic = "KM";
		// 	s.desc = "Fuel mileage";
		// 	nomarray.push(Tools.clone(s));

		// 	var s = new Model.Nominal();
		// 	s.mnemonic = "MEA";
		// 	s.desc = "Meals";
		// 	nomarray.push(Tools.clone(s));

		// 	var s = new Model.Nominal();
		// 	s.mnemonic = "HOT";
		// 	s.desc = "Hotel and accommodation";
		// 	nomarray.push(Tools.clone(s));

		// 	return nomarray;
		// }

		// getFxrates() {
		// 	var fxarray = new Array();

		// 	var s = new Model.Fx();
		// 	s.iso = "THB";
		// 	s.rate = 1.2;
		// 	fxarray.push(Tools.clone(s));

		// 	var s = new Model.Fx();
		// 	s.iso = "PHP";
		// 	s.rate = 1.3;
		// 	fxarray.push(Tools.clone(s));

		// 	var s = new Model.Fx();
		// 	s.iso = "EUR";
		// 	s.rate = 0.6;
		// 	fxarray.push(Tools.clone(s));

		// 	var s = new Model.Fx();
		// 	s.iso = "USD";
		// 	s.rate = 1.0;
		// 	fxarray.push(Tools.clone(s));

		// 	return fxarray;
		// }


        /**
         * gets the live array of all exitems from embedded window config or REST server
         *
         * @param force if true, forces fetch from REST, if false, will use window config if found
         * @param arry
         * @param bundle
         */
		getLive(force:boolean, arry:Array<Model.Exitem>, bundle:{parent:{};callback:(parent:{})=>void}) {
			console.log("[hDS158] in getLive");
			if (force==false && this.window.config && this.window.config.exitems) {
				console.log("[hDS158] found "+this.window.config.exitems.length+" in config");
				this.timeout(function () {
					for (var i = 0; i<this.window.config.exitems.length; i++) {
						console.log("pushed "+this.window.config.exitems[i]['desc']);
						arry.push(this.window.config.exitems[i]);
					}
					if (bundle) {
						bundle.callback(bundle.parent);
					}
				},10)

				return;
			}

            console.log("refreshing liveArray");
			var url="/_ah/api/exitemendpoint/v1/exitem";
            url = this.host + url;

            this.httpGetArray(url,arry,false, bundle);
		}


		/**
		* this runs off and scans all calendars for events in the last 2 months
		* and stores them in a local cache.
		*
		* A controller will call getCalendarEventsAround(date) to retrieve an array subset
		*/
		fetchRecentCalendarEvents() {
			if (this.recentEventsArray) {
				return this.recentEventsArray;
			}
			var now = new Date();
			var maxDate = new Date(now.getTime()+(1000*60*60*24));
			var minDate = new Date(now.getTime()-(1000*60*60*24*60));
			var maxDateS = encodeURIComponent(maxDate.toISOString());
			var minDateS = encodeURIComponent(minDate.toISOString());
			var calUrl = "https://www.googleapis.com/calendar/v3/users/me/calendarList";
			var evUrla = "https://www.googleapis.com/calendar/v3/calendars/ID/events?timeMax=2012-07-22T11%3A30%3A00-07%3A00&timeMin=2011-07-22T11%3A30%3A00-07%3A00&key={YOUR_API_KEY}";
			var evUrl = "https://www.googleapis.com/calendar/v3/calendars/ID/events?timeMax="+maxDateS+"&timeMin="+minDateS+"&fields=items(start%2Csummary)%2Csummary";
			console.log(evUrl);

// GET https://www.googleapis.com/calendar/v3/calendars/roy.smith.esq%40gmail.com/events?timeMax=2012-07-22T11%3A30%3A00-07%3A00&timeMin=2011-07-22T11%3A30%3A00-07%3A00&key={YOUR_API_KEY}

			var calArray = this.calendarArray = new Array;
			var evArray = this.evArray = new Array;	// events per calendar
			var recentEventsArray = this.recentEventsArray = new Array;	// the accumulation of events for all calendars

			var bundle = {parent: this, callback: {}};
			bundle.callback = function () {
				for (var i = 0; i < calArray.length; i++) {
					console.log(calArray[i].summary);
					var evurla = evUrl.replace("ID",calArray[i].id);
					var bundle2 = {parent: bundle.parent, callback: {}};
					bundle2.callback = function () {
						for (var j = 0; j < evArray.length; j++) {
							// some have date, some have dateTime
							console.log("-- ev--"+evArray[j].start.dateTime+" "+evArray[j].start.date+" "+evArray[j].summary);
							recentEventsArray.push(evArray[j]);
						}

					}
					// console.log(bundle.parent);
					bundle.parent.httpGetArray(evurla,evArray,false,bundle2);
				}

			}
			this.httpGetArray(calUrl,calArray,false,bundle);
			return this.recentEventsArray;
		}


		// getNewZ() {
		// 	return this.newArrayZ;
		// }
		// getLive() {
		// 	var live = 
		// 	[
		// 	// {id: 21,name:"John", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"KM", tags:"ARM", dateI:"2013-09-12", km:"20", action:"A"},
		// 	// {id: 22,name:"John", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"HOT", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 23,name:"John", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"N", nom:"HOT", tags:"", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 24,name:"John", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 25,name:"Paul", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"MEA", tags:"", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 26,name:"Paul", desc:"item 3",amount:"9111.2", net:"900.0", vat:".2", stat:"N", nom:"HOT", tags:"", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 27,name:"Paul", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 28,name:"Paul", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"MEA", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 1,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"KM", tags:"", dateI:"2013-09-12", km:"20", action:"A"},
		// 	// {id: 2,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 3,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 4,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 5,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 6,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 7,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
		// 	// {id: 8,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
		// 	// {id: 9,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
		// 	// {id: 10,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"P", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 11,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"P", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	// {id: 12,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"R", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
		// 	];

		// 	var item = new Model.Exitem();
		// 	item.id = 99;
		// 	item.desc = "description of expense";
		// 	item.email="john@bar.com";
		// 	item.stat = "N";
		// 	item.amount = 120;
		// 	item.net = 100;
		// 	item.action = "A";
		// 	item.vat = 20;
		// 	live.push(Tools.clone(item));
		// 	item.id=98;
		// 	live.push(Tools.clone(item));
		// 	item.id=97;
		// 	live.push(Tools.clone(item));
		// 	console.log(live); 

		// 	return live;
		// }



		/**
		* Saves an updated item to the server using put	
		*/
		putUpdatedItem(item:Model.Exitem) {
			this.httpPut(this.host + "/_ah/api/exitemendpoint/v1/exitem", item, false);
		}



		/**
		* deletes an item from the server
		*/
		deleteItem(item:Model.Exitem) {
			this.httpDelete(this.host + "/_ah/api/exitemendpoint/v1/exitem", item,  false);
		}






		// saveNew() {
		// 	for (var i = 0; i< this.newArray.length; i++) {
		// 		console.log("TODO: update live array, recreate newArray and send updates to server");
		// 	}
		// }
		/**
		* called witha changesContainer object
		* saves each item from each settings category
		* NB, also deduplicates
		*/ 
		saveSettingsChanges(obj: {}) {
			console.log("httpDatasource is saving ...");
			console.log(obj);
			/*
			* settings
			*/
			if (obj['settings']) {
				this.httpPut(this.host+'/_ah/api/settingsendpoint/v1/settings', obj['settings'],false);
			}
			/*
			* emp
			*/
			var hashKeyMap = {}
			for (var i = 0; i< obj['emp'].length ; i++) {
				if (hashKeyMap[obj['emp'][i]['$$hashKey']]) {  // don;t send multiple updates, only the first
					continue;						// which is actually the most recent
				}
				hashKeyMap[obj['emp'][i]['$$hashKey']] = "foo";
				this.httpPut(this.host+'/_ah/api/empendpoint/v1/emp', obj['emp'][i],false);
			}
			/*
			* fx
			*/
			var hashKeyMap = {}
			for (var i = 0; i< obj['fx'].length ; i++) {
				if (hashKeyMap[obj['fx'][i]['$$hashKey']]) {  // don;t send multiple updates, only the first
					continue;						// which is actually the most recent
				}
				hashKeyMap[obj['fx'][i]['$$hashKey']] = "foo";
				this.httpPut(this.host+'/_ah/api/fxendpoint/v1/fx', obj['fx'][i],false);
			}
			/*
			* nom
			*/
			var hashKeyMap = {}
			for (var i = 0; i< obj['nom'].length ; i++) {
				if (hashKeyMap[obj['nom'][i]['$$hashKey']]) {  // don;t send multiple updates, only the first
					continue;						// which is actually the most recent
				}
				hashKeyMap[obj['nom'][i]['$$hashKey']] = "foo";
				this.httpPut(this.host+'/_ah/api/nominalendpoint/v1/nominal', obj['nom'][i],false);
			}
		}

		httpDelete(url, obj, isManager) {
			this.http({
				url: url,
				method: "DELETE",
				data: obj,
				headers: {'Content-Type': 'application/json','Authorization':  'Bearer '+this.getAccessToken(isManager)}

				}).success(function (data, status, headers, config) {
					console.log("delete success");
					}).error(function (data, status, headers, config) {
						console.log("delete fail");
						});

		}

		httpPut(url, obj, isManager) {
			this.http({
				url: url,
				method: "PUT",
				data: obj,
				headers: {'Content-Type': 'application/json','Authorization':  'Bearer '+this.getAccessToken(isManager)}

				}).success(function (data, status, headers, config) {
					console.log("success");
					}).error(function (data, status, headers, config) {
						console.log("fail");
						});

		}


		httpGetArray(url, arry, isManager, bundle?:{}) {
			this.http({
			url: url,
			method: "GET",
			headers: {'Authorization': 'Bearer '+this.getAccessToken(isManager)}

			}).success(function (data, status, headers, config) {
				console.log(data);
				if (data.items) {
					for (var i = 0; i< data.items.length; i++) {
						arry.push(data.items[i]);
					}
				}
				// arry=data.items;
				if (bundle) {
					bundle['callback'](bundle['parent']);
				}
				}).error(function (data, status, headers, config) {
					console.log("fail");
					});
			}
		httpGetObject(url, obj, isManager) {
			this.http({
			url: url,
			method: "GET",
			headers: {'Authorization': 'Bearer '+this.getAccessToken(isManager)}
			}).success(function (data, status, headers, config) {
				console.log("================");
				console.log(data.items[0]);
				Tools.transcribe(data.items[0], obj);
				// for (var i = 0; i< data.items.length; i++) {
				// 	arry.push(data.items[i]);
				// }
				// arry=data.items;
				}).error(function (data, status, headers, config) {
					console.log("fail");
					});
			}
		getAccessToken(isManager:boolean) {
			if (isManager) {
				return this.window.config.a['cm'].a;
			} else {
				return this.window.config.a['c'].a;
			}
		}





	}
}
// declare var angular;
angular.module('exApp')
.service('HttpDatasourceService', DatasourceModule.HttpDatasourceService);