/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="modelService.ts" />
/// <reference path="../common/tools.ts"/>
/*
* (c) RoySmith 2013
*/

module DatasourceModule {

//	export interface IDatasourceService {
//		getLive:(arry:Array<Model.Exitem>, bundle:{})=>void;
//		getSettings:(obj:any) =>void;
//		getEmp:() =>{};
//		getNoms:() =>{};
//		getFxrates:() =>{};
//		saveSettingsChanges:(obj:{})=>void;
//		putUpdatedItem:(item:Model.Exitem)=>void;

		// getNewZ :()=>string[];
		// saveNew :()=>void
//	}


	export class DummyDatasourceService implements IDatasourceService{


		liveArray = [];
		newArray:{}[] = [];
		// newArrayZ = ["q","b"];

		constructor () {
			console.log();
		}

		putUpdatedItem(item:Model.Exitem) {
			console.log("dummy datasource putUpdatedItem ...");
			console.log(item);
		}


		getSettings() {
			var s = new Model.Settings();
			s.kmrate = 25;
			s.tags = "proj1,proj2";
			s.baseiso = "GBP";
			s.coname = "cleverthinking";
			return s;
		}

        /**
         * deletes an item from the server
         */
         deleteItem(item:Model.Exitem) {
//            this.httpDelete("/_ah/api/exitemendpoint/v1/exitem", item,  false);
         }

        getEmp() {
			var emparray = new Array();

			var s = new Model.Emp();
			s.email="fred@foo.com";
			s.name = "Fred Bloggs";
			s.bank = "12345678 12-23-45";
			s.manager = true;
			emparray.push(Tools.clone(s));


			s.email="joe@foo.com";
			s.name = "Joe Soap";
			s.bank = "12345678 12-23-45";
			s.manager = false;
			emparray.push(Tools.clone(s));

			return emparray;
		}
		getNoms() {
			var nomarray = new Array();

			var s = new Model.Nominal();
			s.mnemonic = "TEL";
			s.desc = "Telephone";
			nomarray.push(Tools.clone(s));

			var s = new Model.Nominal();
			s.mnemonic = "KM";
			s.desc = "Fuel mileage";
			nomarray.push(Tools.clone(s));

			var s = new Model.Nominal();
			s.mnemonic = "MEA";
			s.desc = "Meals";
			nomarray.push(Tools.clone(s));

			var s = new Model.Nominal();
			s.mnemonic = "HOT";
			s.desc = "Hotel and accommodation";
			nomarray.push(Tools.clone(s));

			return nomarray;
		}

		getFxrates() {
			var fxarray = new Array();

			var s = new Model.Fx();
			s.iso = "THB";
			s.rate = 1.2;
			fxarray.push(Tools.clone(s));

			var s = new Model.Fx();
			s.iso = "PHP";
			s.rate = 1.3;
			fxarray.push(Tools.clone(s));

			var s = new Model.Fx();
			s.iso = "EUR";
			s.rate = 0.6;
			fxarray.push(Tools.clone(s));

			var s = new Model.Fx();
			s.iso = "USD";
			s.rate = 1.0;
			fxarray.push(Tools.clone(s));

			return fxarray;
		}




		// getNewZ() {
		// 	return this.newArrayZ;
		// }
		getLive(force:boolean, arry:Array<Model.Exitem>, bundle:{}) {
			var live = 
			[
			// {id: 21,name:"John", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"KM", tags:"ARM", dateI:"2013-09-12", km:"20", action:"A"},
			// {id: 22,name:"John", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"HOT", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 23,name:"John", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"N", nom:"HOT", tags:"", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 24,name:"John", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 25,name:"Paul", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"MEA", tags:"", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 26,name:"Paul", desc:"item 3",amount:"9111.2", net:"900.0", vat:".2", stat:"N", nom:"HOT", tags:"", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 27,name:"Paul", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 28,name:"Paul", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"MEA", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 1,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"N", nom:"KM", tags:"", dateI:"2013-09-12", km:"20", action:"A"},
			// {id: 2,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 3,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"N", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 4,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 5,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 6,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"Q", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 7,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
			// {id: 8,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
			// {id: 9,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"A", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"P"},
			// {id: 10,name:"emp1", desc:"item 1",amount:"1.2", net:"1.0", vat:".2", stat:"P", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 11,name:"emp2", desc:"item 2", amount:"11.2", net:"1.0", vat:".2", stat:"P", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			// {id: 12,name:"emp2", desc:"item 3",amount:"111.2", net:"1.0", vat:".2", stat:"R", nom:"TEL", tags:"ARM", dateI:"2013-09-12", km:"", action:"A"},
			];

			var item = new Model.Exitem();
			item.id = "99";
			item.desc = "description of expense";
			item.email="john@bar.com";
			item.stat = "N";
			item.amount = 120;
			item.net = 100;
			item.action = "A";
			item.vat = 20;
			arry.push(Tools.clone(item));
			item.id="98";
			arry.push(Tools.clone(item));
			item.id="97";
			arry.push(Tools.clone(item));
			item.id="96";
			item.stat="R";
			arry.push(Tools.clone(item));
			console.log(live); 
			if (bundle) {
				console.log("calling callback");
					setTimeout( function () {
					bundle['callback'](bundle['parent']);						
						}, 100);
				}
			return;
		}
		// saveNew() {
		// 	for (var i = 0; i< this.newArray.length; i++) {
		// 		console.log("TODO: update live array, recreate newArray and send updates to server");
		// 	}
		// }
		/**
		* called witha changesContainer object
		*/ 
		saveSettingsChanges(obj: {}) {
			console.log("dummyDatasource is saving ...");

			console.log(obj);
		}
	}
}
// declare var angular;
angular.module('exApp')
.service('DummyDatasourceService', DatasourceModule.DummyDatasourceService);