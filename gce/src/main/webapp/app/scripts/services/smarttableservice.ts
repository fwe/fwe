/// <reference path="../../../lib/angular.d.ts" />

module CleverExpense.Service {

	/**
	* data sharing class to provide the smart table column definitions to multiple controllers
	*/
    export class SmarttableService {


    outboxColumnCollection = [
      {label: 'Date Incurred', headerTemplateUrl: 'views/colDateI.html', map: 'dateI'},
      {label: 'Description', headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Send',  headerTemplateUrl: 'views/colSend.html',cellTemplateUrl: 'views/radioS.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoomDelete.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
    newColumnCollection = [
      {label: 'Email', map: 'email'},
      {label: 'Date Incurred', headerTemplateUrl: 'views/colDateI.html', map: 'dateI'},
      {label: 'Description', headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Approve',  headerTemplateUrl: 'views/colApprove.html',cellTemplateUrl: 'views/radioA.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Query',  headerTemplateUrl: 'views/colQuery.html',cellTemplateUrl: 'views/radioQ.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Reject',  headerTemplateUrl: 'views/colReject.html',cellTemplateUrl: 'views/radioR.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoomDelete.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
    queryColumnCollection = [
      {label: 'Email', map: 'email'},
      {label: 'Date Incurred', headerTemplateUrl: 'views/colDateI.html', map: 'dateI'},
      {label: 'Description', headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'My comments',  headerTemplateUrl: 'views/colCommentsM.html', map: 'mcomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Employee comments',  headerTemplateUrl: 'views/colCommentsE.html', map: 'ecomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Approve',  headerTemplateUrl: 'views/colApprove.html',cellTemplateUrl: 'views/radioA.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Reject',  headerTemplateUrl: 'views/colReject.html',cellTemplateUrl: 'views/radioR.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoom.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
   approvedColumnCollection = [
      {label: 'Email', map: 'email'},
      {label: 'Date Incurred', headerTemplateUrl: 'views/colDateI.html', map: 'dateI'},
      {label: 'Description', headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'My comments',  headerTemplateUrl: 'views/colCommentsM.html', map: 'mcomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Employee comments',  headerTemplateUrl: 'views/colCommentsE.html', map: 'ecomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Pay',  headerTemplateUrl: 'views/colPay.html',cellTemplateUrl: 'views/radioP.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Query',  headerTemplateUrl: 'views/colQuery.html',cellTemplateUrl: 'views/radioQ.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoom.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
    paidColumnCollection = [
      {label: 'Email', map: 'email',headerClass: 'wid5',cellClass: 'wid5',},
      {label: 'Date Incurred', headerTemplateUrl: 'views/colDateI.html', map: 'dateI'},
      {label: 'Description', headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'My comments',  headerTemplateUrl: 'views/colCommentsM.html', map: 'mcomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Employee comments',  headerTemplateUrl: 'views/colCommentsE.html', map: 'ecomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Date Paid',  headerTemplateUrl: 'views/colDateP.html', map: 'dateP', headerClass: 'wid2', cellClass: 'wid2', isEditable:false},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoom.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
    rejectedColumnCollection = [
      {label: 'Email', map: 'email'},
      {label: 'Date Incurred', map: 'dateI'},
      {label: 'Description',  headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'My comments',  headerTemplateUrl: 'views/colCommentsM.html', map: 'mcomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Employee comments',  headerTemplateUrl: 'views/colCommentsE.html', map: 'ecomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoom.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];
    deletedColumnCollection = [
      {label: 'Email', map: 'email'},
      {label: 'Date Incurred', map: 'dateI'},
      {label: 'Description',  headerTemplateUrl: 'views/colDescription.html',map: 'desc', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Mileage',  headerTemplateUrl: 'views/colKm.html', map: 'km', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Amount',  headerTemplateUrl: 'views/colAmount.html', map: 'amount', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'Nett', map: 'net', formatFunction: "currency",headerClass: 'wid2 r', cellClass: 'wid2 r'},
      // {label: 'VAT', map: 'vat', formatFunction: "currency", headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Nominal code', headerTemplateUrl: 'views/colNominal.html', map: 'nom', headerClass: 'wid2', cellClass: 'wid2'},
      {label: 'Project',  headerTemplateUrl: 'views/colTags.html',map: 'tags'},
      // {label: 'Mileage rate', map: 'kmrate', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'Currency',  headerTemplateUrl: 'views/colFx.html',map: 'currency', headerClass: 'wid2 r', cellClass: 'wid2 r'},
      {label: 'My comments',  headerTemplateUrl: 'views/colCommentsM.html', map: 'mcomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      {label: 'Employee comments',  headerTemplateUrl: 'views/colCommentsE.html', map: 'ecomment', headerClass: 'wid5', cellClass: 'wid5', isEditable:true},
      // {label: 'Date Paid', nom: 'dateP'},
      // {label: 'Date Paid', map: 'dateP'},
      {label: 'Details/Edit',  headerTemplateUrl: 'views/colZoom.html',cellTemplateUrl: 'views/zoomUndelete.html', headerClass: 'wid2 c', cellClass: 'wid2 c'},
    ];

    hiddenColumnCollection = [
      {label: 'FX rate', map: 'fx'},
      {label: 'Foreign amount', map: 'fxamount', headerClass: 'wid2', cellClass: 'wid2'},
    ];

    }
}
// declare var angular;
angular.module('exApp')
.service('SmarttableService', CleverExpense.Service.SmarttableService);