/// <reference path="../../../lib/angular.d.ts" />


module DriveServiceModule {
	export interface IDriveService {

	}

	export class DriveService implements IDriveService{


		storageService;

		static $inject = ['StorageService'];
		constructor (storageService) {
			this.storageService = storageService;
		}

		/**
		* r
		*/
		uploadFile(title, file,callback) {
			var parent = this;
 			window['gapi'].client.load('drive', 'v2', function() {
 				console.log("drive api loaded. should do this only once!!");
          		// var file = evt.target.files[0];
          		parent.insertFile(title, file, callback);
        	});		
 		}

  /**
       * Insert new file.
       *
       * @param {File} fileData File object to read data from.
       * @param {Function} callback Function to call when the request is complete.
       */
      	insertFile(title, fileData, callback) {
      		var parent = this;
      		console.log("in drive.insertFile with title/filedata = "+title);
      		console.log("");
      		console.log(fileData);
      		// NB, uses the MANAGER ACCESS TOKEN!!!!!!!!!
	        window['gapi'].auth.setToken({access_token:window['config'].a.cm.a});

	        var boundary = '-------314159265358979323846';
	        var delimiter = "\r\n--" + boundary + "\r\n";
	        var close_delim = "\r\n--" + boundary + "--";

	        var reader = new FileReader();
	        reader.onerror = function(ev) {
	        	console.log("filereader error...");
	        	console.log(ev);
	        }
	        reader.onload = function(e) {
	          console.log("in reader onload");
	          var contentType = fileData.type || 'application/octet-stream';
	          var metadata = {
	            'title': title,
	            'mimeType': contentType
	          };

	          var base64Data:any;
	          if (reader['result64']) {
	          	base64Data = reader['result64'];  // stored and fetched
	          } else {
	          	base64Data = btoa(reader.result); // fresh
	          }
	          var multipartRequestBody =
	              delimiter +
	              'Content-Type: application/json\r\n\r\n' +
	              JSON.stringify(metadata) +
	              delimiter +
	              'Content-Type: ' + contentType + '\r\n' +
	              'Content-Transfer-Encoding: base64\r\n' +
	              '\r\n' +
	              base64Data +
	              close_delim;

	          var request = window['gapi'].client.request({
	              'path': '/upload/drive/v2/files',
	              'method': 'POST',
	              'params': {'uploadType': 'multipart'},
	              'headers': {
	                'Content-Type': 'multipart/mixed; boundary="' + boundary + '"'
	              },
	              'body': multipartRequestBody});
	          if (!callback) {
	            callback = function(file) {
	            	console.log("Drive responded with ...");
	            	console.log(file)
	            };
	          }
	          request.execute(callback);
	          setTimeout(
	          	function () {
		          	console.log("deleting from local storage");
	            	parent.storageService.deleteImageFile();

	          		}, 10000);
	        }

	        /* if the file object has a base64Data property, it means it was 
	        * stored and retrieved. So no need to use FileReader to fetch the content
	        * I can just stuff the base64 content and call the onload callback directly
	        */

      		if (fileData['base64Data']) {
      			console.log("file has embedded media !!!!!!");
      			reader['result64'] = fileData['base64Data'];
      			reader.onload(null);
      		} else {
   	        	reader['readAsBinaryString'](fileData);
   	        }
      }

	}
}
// declare var angular;
angular.module('exApp')
.service('DriveService', DriveServiceModule.DriveService);