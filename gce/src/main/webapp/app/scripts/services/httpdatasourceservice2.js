var DatasourceModule2;
(function (DatasourceModule2) {
    var HttpDatasourceService = (function () {
        function HttpDatasourceService($http, $timeout, $window, exResourceService) {
            this.liveArray = [];
            this.newArray = [];
            console.log("constructor2");
            this.http = $http;
            this.timeout = $timeout;
            this.window = $window;
            this.refs = { exResourceService: exResourceService };

            if (!window['clexunittesting']) {
                this.fetchRecentCalendarEvents();
            }

            this.host = "https://freewebexpenses.appspot.com";
            if (window.location.host.indexOf(":") > -1) {
                this.host = "http://" + window.location.host;
            }
        }
        HttpDatasourceService.prototype.getSettings = function (obj) {
            if (this.window.config && this.window.config.settings) {
                this.timeout(function () {
                    window['Tools'].transcribe(this.window.config.settings, obj);
                }, 10);

                return;
            }

            var arry = new Array();
            this.refs.exResourceService.getAllSettings(arry, undefined, undefined, undefined, undefined).then(function () {
                console.log("settings coname = " + arry[0].coname);
                angular.copy(arry[0], obj);
            });
        };

        HttpDatasourceService.prototype.getEmp = function (arry) {
            if (this.window.config && this.window.config.emp) {
                this.timeout(function () {
                    for (var i = 0; i < this.window.config.emp.length; i++) {
                        arry.push(this.window.config.emp[i]);
                        console.log(this.window.config.emp[i]);
                    }
                }, 10);

                return;
            }
            this.refs.exResourceService.getAllEmps(arry, undefined, undefined, undefined, undefined).then(function () {
            });
        };
        HttpDatasourceService.prototype.getNoms = function (arry) {
            if (this.window.config && this.window.config.nominal) {
                this.timeout(function () {
                    for (var i = 0; i < this.window.config.nominal.length; i++) {
                        arry.push(this.window.config.nominal[i]);
                    }
                }, 10);

                return;
            }
            this.refs.exResourceService.getAllNoms(arry, undefined, undefined, undefined, undefined).then(function () {
            });
        };
        HttpDatasourceService.prototype.getFxrates = function (arry) {
            if (this.window.config && this.window.config.fx) {
                this.timeout(function () {
                    for (var i = 0; i < this.window.config.fx.length; i++) {
                        arry.push(this.window.config.fx[i]);
                    }
                }, 10);

                return;
            }
            this.refs.exResourceService.getAllFxrates(arry, undefined, undefined, undefined, undefined).then(function () {
            });
        };

        HttpDatasourceService.prototype.getLive = function (force, arry, exitemScope, bundle) {
            console.log("[hDS158] in getLive");
            if (force == false && this.window.config && this.window.config.exitems) {
                console.log("[hDS158] found " + this.window.config.exitems.length + " in config");
                this.timeout(function () {
                    for (var i = 0; i < this.window.config.exitems.length; i++) {
                        console.log("pushed " + this.window.config.exitems[i]['desc']);
                        arry.push(this.window.config.exitems[i]);
                    }
                    if (bundle) {
                        bundle.callback(bundle.parent);
                    }
                }, 10);

                return;
            }

            console.log("refreshing liveArray using ngR");
            var q = {};
            if (exitemScope) {
                q = { s: exitemScope };
            }
            this.refs.exResourceService.getAllExitems(arry, q, undefined, undefined, undefined).then(function () {
                bundle.callback(bundle.parent);
            });
        };

        HttpDatasourceService.prototype.fetchRecentCalendarEvents = function () {
            var _this = this;
            if (this.recentEventsArray) {
                return this.recentEventsArray;
            }

            try  {
                gapi.auth.getToken().access_token.length;
            } catch (ex) {
                setTimeout(function () {
                    _this.fetchRecentCalendarEvents();
                }, 5000);
                return;
            }
            var now = new Date();
            var maxDate = new Date(now.getTime() + (1000 * 60 * 60 * 24));
            var minDate = new Date(now.getTime() - (1000 * 60 * 60 * 24 * 60));
            var maxDateS = encodeURIComponent(maxDate.toISOString());
            var minDateS = encodeURIComponent(minDate.toISOString());
            var calUrl = "https://www.googleapis.com/calendar/v3/users/me/calendarList";
            var evUrla = "https://www.googleapis.com/calendar/v3/calendars/ID/events?timeMax=2012-07-22T11%3A30%3A00-07%3A00&timeMin=2011-07-22T11%3A30%3A00-07%3A00&key={YOUR_API_KEY}";
            var evUrl = "https://www.googleapis.com/calendar/v3/calendars/ID/events?timeMax=" + maxDateS + "&timeMin=" + minDateS + "&fields=items(start%2Csummary)%2Csummary";
            console.log(evUrl);

            var calArray = this.calendarArray = new Array;
            var evArray = this.evArray = new Array;
            var recentEventsArray = this.recentEventsArray = new Array;

            var bundle = { parent: this, callback: {} };
            bundle.callback = function () {
                for (var i = 0; i < calArray.length; i++) {
                    var evurla = evUrl.replace("ID", calArray[i].id);
                    var bundle2 = { parent: bundle.parent, callback: {} };
                    bundle2.callback = function () {
                        for (var j = 0; j < evArray.length; j++) {
                            if (!evArray[j]['processed']) {
                                recentEventsArray.push(evArray[j]);
                                evArray[j]['processed'] = true;
                            }
                        }
                    };

                    bundle.parent.httpGetArray(evurla, evArray, false, bundle2);
                }
            };
            this.httpGetArray(calUrl, calArray, false, bundle);
            return this.recentEventsArray;
        };

        HttpDatasourceService.prototype.putUpdatedItem = function (item) {
            var _this = this;
            this.refs.exResourceService.getExitemsResource().update({ id: item.id }, item).$promise.then(function () {
                _this.driveActive = false;
            }, function (resp) {
                console.error(resp);
                _this.driveError = true;
                if (resp.status == 401) {
                    window['auth'].fetchToken();
                    _this.timeout(function () {
                        _this.putUpdatedItem(item);
                    }, 5000);
                }
            });
        };

        HttpDatasourceService.prototype.deleteItem = function (item) {
            this.httpDelete(this.host + "/_ah/api/exitemendpoint/v1/exitem", item, false);
        };

        HttpDatasourceService.prototype.saveSettingsChanges = function (obj) {
            var _this = this;
            console.log("httpDatasource is saving ...");
            console.log(obj);

            if (obj['settings']) {
                var item = obj['settings'];
                this.refs.exResourceService.getSettingsResource().update({ id: item.orgnId }, item).$promise.then(function () {
                    _this.driveActive = false;
                }, function (resp) {
                    console.error(resp);
                    _this.driveError = true;
                    if (resp.status == 401) {
                        window['auth'].fetchToken();
                        _this.timeout(function () {
                            _this.saveSettingsChanges(obj);
                        }, 5000);
                    }

                    _this.driveError = true;
                });
            }

            var hashKeyMap = {};
            for (var i = 0; i < obj['emp'].length; i++) {
                if (hashKeyMap[obj['emp'][i]['$$hashKey']]) {
                    continue;
                }
                hashKeyMap[obj['emp'][i]['$$hashKey']] = "foo";
                var item2 = obj['emp'][i];
                this.refs.exResourceService.getEmpsResource().update({ id: item2.googleId }, item2).$promise.then(function () {
                    _this.driveActive = false;
                }, function () {
                    _this.driveError = true;
                });
            }

            var hashKeyMap = {};
            for (var i = 0; i < obj['fx'].length; i++) {
                if (hashKeyMap[obj['fx'][i]['$$hashKey']]) {
                    continue;
                }
                hashKeyMap[obj['fx'][i]['$$hashKey']] = "foo";
                var item3 = obj['fx'][i];
                this.refs.exResourceService.getFxratesResource().update({ id: item3.iso }, item3).$promise.then(function () {
                    _this.driveActive = false;
                }, function () {
                    _this.driveError = true;
                });
            }

            var hashKeyMap = {};
            for (var i = 0; i < obj['nom'].length; i++) {
                if (hashKeyMap[obj['nom'][i]['$$hashKey']]) {
                    continue;
                }
                var item4 = obj['nom'][i];
                this.refs.exResourceService.getNomsResource().update({ id: item4.mnemonic }, item4).$promise.then(function () {
                    _this.driveActive = false;
                }, function () {
                    _this.driveError = true;
                });
                hashKeyMap[obj['nom'][i]['$$hashKey']] = "foo";
            }
        };

        HttpDatasourceService.prototype.httpDelete = function (url, obj, isManager) {
            this.http({
                url: url,
                method: "DELETE",
                data: obj,
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.getAccessToken(isManager) }
            }).success(function (data, status, headers, config) {
                console.log("delete success");
            }).error(function (data, status, headers, config) {
                console.log("delete fail");
            });
        };

        HttpDatasourceService.prototype.httpPut = function (url, obj, isManager) {
            this.http({
                url: url,
                method: "PUT",
                data: obj,
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.getAccessToken(isManager) }
            }).success(function (data, status, headers, config) {
                console.log("success");
            }).error(function (data, status, headers, config) {
                console.log("fail");
            });
        };

        HttpDatasourceService.prototype.httpGetArray = function (url, arry, isManager, bundle) {
            this.http({
                url: url,
                method: "GET",
                headers: { 'Authorization': 'Bearer ' + this.getAccessToken(isManager) }
            }).success(function (data, status, headers, config) {
                console.log(data);
                if (data.items) {
                    for (var i = 0; i < data.items.length; i++) {
                        arry.push(data.items[i]);
                    }
                }

                if (bundle) {
                    bundle['callback'](bundle['parent']);
                }
            }).error(function (data, status, headers, config) {
                console.log("fail");
            });
        };
        HttpDatasourceService.prototype.httpGetObject = function (url, obj, isManager) {
            this.http({
                url: url,
                method: "GET",
                headers: { 'Authorization': 'Bearer ' + this.getAccessToken(isManager) }
            }).success(function (data, status, headers, config) {
                console.log("================");
                console.log(data.items[0]);
                Tools.transcribe(data.items[0], obj);
            }).error(function (data, status, headers, config) {
                console.log("fail");
            });
        };
        HttpDatasourceService.prototype.getAccessToken = function (isManager) {
            return gapi.auth.getToken().access_token;
        };
        HttpDatasourceService.$inject = ['$http', '$timeout', '$window', 'ExResourceService'];
        return HttpDatasourceService;
    })();
    DatasourceModule2.HttpDatasourceService = HttpDatasourceService;
})(DatasourceModule2 || (DatasourceModule2 = {}));

angular.module('exApp').service('HttpDatasourceService', DatasourceModule2.HttpDatasourceService);
//# sourceMappingURL=httpdatasourceservice2.js.map
