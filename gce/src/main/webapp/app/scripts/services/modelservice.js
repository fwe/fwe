var Model;
(function (Model) {
    var Exitem = (function () {
        function Exitem() {
        }
        return Exitem;
    })();
    Model.Exitem = Exitem;

    var Nominal = (function () {
        function Nominal() {
        }
        return Nominal;
    })();
    Model.Nominal = Nominal;

    var Fx = (function () {
        function Fx() {
        }
        return Fx;
    })();
    Model.Fx = Fx;

    var Emp = (function () {
        function Emp() {
        }
        return Emp;
    })();
    Model.Emp = Emp;

    var Settings = (function () {
        function Settings() {
        }
        return Settings;
    })();
    Model.Settings = Settings;

    var ModelService = (function () {
        function ModelService($rootScope, $timeOut, $location, storageService, datasourceService, driveService) {
            this.liveArray = new Array();
            this.outboxArray = [];
            this.newArray = [];
            this.queryArray = [];
            this.approvedArray = [];
            this.paidArray = [];
            this.rejectedArray = [];
            this.deletedArray = [];
            this.settings = new Settings();
            this.emp = [];
            this.eOrO = 'e';
            this.datasourceService = datasourceService;
            this.datasourceService = datasourceService;
            this.storageService = storageService;
            this.driveService = driveService;
            this.rootScope = $rootScope;
            this.timeOut = $timeOut;

            this.outboxArray = new Array();
            this.liveArray = new Array();
            this.newArray = new Array();
            this.queryArray = new Array();
            this.approvedArray = new Array();
            this.paidArray = new Array();
            this.rejectedArray = new Array();
            this.deletedArray = new Array();

            if ($location.absUrl().indexOf("manager") > -1) {
                this.eOrO = 'o';
            }

            try  {
                this.storageService.fetchOutbox(this.outboxArray);
            } catch (ex) {
                console.log("[ms204] no stored outbox. OK for mgr.");
            }

            if (!window['clexunittesting']) {
                this.refreshAllModelData(this.eOrO);
            }
        }
        ModelService.prototype.refreshAllModelData = function (eOrO) {
            var _this = this;
            try  {
                console.log(gapi.auth.getToken().access_token.length);
            } catch (ex) {
                console.log("no token yet");
                this.timeOut(function () {
                    _this.refreshAllModelData(eOrO);
                }, 500);
                return;
            }
            this.refreshExitems(false);

            this.datasourceService.getSettings(this.settings);
            this.emp = new Array();
            this.datasourceService.getEmp(this.emp);
            this.fxrates = new Array();
            this.datasourceService.getFxrates(this.fxrates);
            this.noms = new Array();
            this.datasourceService.getNoms(this.noms);
        };

        ModelService.prototype.refreshExitems = function (force) {
            this.liveArray.length = 0;
            this.datasourceService.getLive(force, this.liveArray, this.eOrO, { parent: this, callback: this.parseLiveArray });
        };

        ModelService.prototype.clearDownArrays = function (parent) {
            parent.newArray.length = 0;
            parent.queryArray.length = 0;
            parent.approvedArray.length = 0;
            parent.paidArray.length = 0;
            parent.rejectedArray.length = 0;
            parent.deletedArray.length = 0;
        };

        ModelService.prototype.parseLiveArray = function (parent) {
            console.log("in parselivearray callback");
            parent.clearDownArrays(parent);
            console.log(parent.liveArray);
            for (var i = 0; i < parent.liveArray.length; i++) {
                var item = parent.liveArray[i];
                switch (item.stat) {
                    case 'N':
                        parent.newArray.push(item);
                        break;
                    case 'Q':
                        parent.queryArray.push(item);
                        break;
                    case 'A':
                        parent.approvedArray.push(item);
                        break;
                    case 'P':
                        parent.paidArray.push(item);
                        break;
                    case 'R':
                        parent.rejectedArray.push(item);
                        break;
                }
            }
            console.log(parent.newArray);

            try  {
                Pace.stop();
            } catch (ex) {
            }
        };

        ModelService.prototype.lookupEmp = function (email) {
            if (!this.empMap) {
                this.empMap = {};
                for (var i = 0; i < this.emp.length; i++) {
                    if (!this.empMap[this.emp[i]['email']]) {
                        this.empMap[this.emp[i]['email']] = this.emp[i];
                    }
                }
            }
            return "" + this.empMap[email]['name'];
        };

        ModelService.prototype.lookupNom = function (nom) {
            if (!this.nomMap) {
                this.nomMap = {};
                for (var i = 0; i < this.noms.length; i++) {
                    if (!this.nomMap[this.noms[i]['mnemonic']]) {
                        this.nomMap[this.noms[i]['mnemonic']] = this.noms[i];
                    }
                }
            }
            return "" + this.empMap[nom]['desc'];
        };

        ModelService.prototype.getSettings = function () {
            return this.settings;
        };
        ModelService.prototype.getTagsAsArray = function () {
            if (this.settings && this.settings.tags) {
                return this.settings.tags.split(',');
            } else {
                return [];
            }
        };
        ModelService.prototype.getEmp = function () {
            return this.emp;
        };
        ModelService.prototype.getNoms = function () {
            return this.noms;
        };
        ModelService.prototype.getFxrates = function () {
            return this.fxrates;
        };
        ModelService.prototype.getOutbox = function () {
            return this.outboxArray;
        };
        ModelService.prototype.getNew = function () {
            return this.newArray;
        };
        ModelService.prototype.getQuery = function () {
            return this.queryArray;
        };
        ModelService.prototype.getApproved = function () {
            return this.approvedArray;
        };
        ModelService.prototype.getPaid = function () {
            return this.paidArray;
        };
        ModelService.prototype.getRejected = function () {
            return this.rejectedArray;
        };
        ModelService.prototype.getDeleted = function () {
            return this.deletedArray;
        };

        ModelService.prototype.getPaidDates = function () {
            if (!this.getPaid()) {
                return [];
            }

            var dateMap = {};

            for (var i = 0; i < this.getPaid().length; i++) {
                dateMap[this.getPaid()[i].dateP] = this.getPaid()[i];
            }

            var date;
            this.paidDatesArray = [];
            for (date in dateMap) {
                this.paidDatesArray.unshift(date);
            }
            return this.paidDatesArray;
        };

        ModelService.prototype.deleteItem = function (row, collection) {
            collection.splice(collection.indexOf(row), 1);
            this.liveArray.splice(this.liveArray.indexOf(row), 1);
            this.deletedArray.push(row);

            this.storageService.saveOutbox(this.outboxArray);
        };

        ModelService.prototype.unDeleteItem = function (row) {
            this.liveArray.push(row);
            this.deletedArray.splice(this.deletedArray.indexOf(row), 1);

            switch (row.stat) {
                case 'O':
                    this.outboxArray.push(row);
                    break;
                case 'N':
                    this.newArray.push(row);
                    break;
                case 'Q':
                    this.queryArray.push(row);
                    break;
                case 'A':
                    this.approvedArray.push(row);
                    break;
                case 'P':
                    this.paidArray.push(row);
                    break;
                case 'R':
                    this.rejectedArray.push(row);
                    break;
            }
            this.storageService.saveOutbox(this.outboxArray);
        };

        ModelService.prototype.newItem = function () {
            var exitem = new Model.Exitem();

            exitem.id = "" + new Date().getTime();
            exitem.orgnId = this.getSettings().orgnId;
            exitem.stat = "O";
            exitem.action = "N";
            exitem.currency = this.getSettings().baseiso;
            exitem.kmrate = this.getSettings().kmrate;
            exitem.tags = "";

            return exitem;
        };

        ModelService.prototype.saveOutbox = function (exitem) {
            if (exitem) {
                if (this.getOutbox().indexOf(exitem) < 0) {
                    this.getOutbox().push(exitem);
                }
            }
            this.storageService.saveOutbox(this.outboxArray);
        };

        ModelService.prototype.calculateVat = function (item) {
            console.log(1 * this.settings.defaultVatRate + 1);
            if (!this.settings.defaultVatRate || this.settings.defaultVatRate == 0) {
                item.net = item.amount;
                item.vat = 0;
            } else {
                item.net = Math.round(item.amount * 100 / (1 * this.settings.defaultVatRate + 1)) / 100;
                item.vat = Math.round((item.amount - item.net) * 100) / 100;
            }
        };

        ModelService.prototype.calculateFx = function (focusItem) {
            if (!focusItem.fx) {
                return;
            }
            focusItem.amount = focusItem.fxamount * focusItem.fx;
            focusItem.amount = (typeof focusItem.amount != "number" ? 0 : focusItem.amount);
            focusItem.net = focusItem.fxnet * focusItem.fx;
            focusItem.net = (typeof focusItem.net != "number" ? 0 : focusItem.net);
            focusItem.vat = focusItem.fxvat * focusItem.fx;
            focusItem.vat = (typeof focusItem.vat != "number" ? 0 : focusItem.vat);
        };

        ModelService.prototype.kmChanged = function (item) {
            item.amount = Math.round((item.km * item.kmrate) * 100) / 100;
            ;
            item.net = item.amount;
            item.vat = 0;
        };

        ModelService.prototype.processActionsAll = function () {
            this.processActionsEach(this.queryArray);
            this.processActionsEach(this.approvedArray);
            this.processActionsEach(this.newArray);
        };

        ModelService.prototype.processActionsEach = function (arry) {
            var isOutbox = false;
            var len = arry.length;
            for (var i = 0; i < len; i++) {
                var item = arry[0];
                arry.splice(0, 1);
                var currentStat = item.stat;
                item.stat = item.action;
                console.log("a=" + item['action']);

                switch (item['action']) {
                    case 'N':
                        this.newArray.push(item);
                        item.action = "A";
                        if (currentStat == 'O') {
                            isOutbox = true;
                        }
                        break;
                    case 'A':
                        this.approvedArray.push(item);
                        item.action = "P";
                        break;
                    case 'Q':
                        if (currentStat == 'Q') {
                            this.newArray.push(item);
                            item.action = "N";
                        } else {
                            this.queryArray.push(item);
                            item.action = "N";
                        }
                        break;
                    case 'R':
                        this.rejectedArray.push(item);
                        break;
                    case 'P':
                        this.paidArray.push(item);

                        item.dateP = new Date().toLocaleDateString();
                        break;
                }

                this.datasourceService.putUpdatedItem(Tools.clone(item));

                if (isOutbox) {
                    this.storageService.saveOutbox(arry);
                }
                console.log("TODO: update live array, recreate newArray and send updates to server");
                console.log(this.newArray);
            }
        };

        ModelService.prototype.putUpdatedItem = function (item) {
            console.warn(111);
            this.datasourceService.putUpdatedItem(Tools.clone(item));
        };

        ModelService.prototype.uploadImageThenExitem = function (exitem) {
            console.log("in uploadImageThenExitem");
            var parent = this;
            var title = exitem.desc;
            if (title == undefined) {
                title = "";
            }
            if (title.length > 20) {
                title = title.substr(0, 19);
            }
            title = title;

            var fetchImageFileCallback = function () {
                console.log("in fetchImageFileCallback with storageObject...");

                var fileObject = JSON.parse(this['result']);
                console.log("after all that, we have the original file object ...");

                parent.driveService.uploadFile(title, fileObject, function (file) {
                    console.log("in drivecallback with file...");
                    console.log(file);
                    exitem.rurl = file['webContentLink'].replace("&export=download", "");
                    console.log(exitem.rurl);

                    parent.datasourceService.putUpdatedItem(exitem);
                    parent.rootScope.$digest();
                });
            };
            this.storageService.fetchImageFile(exitem.rurl, fetchImageFileCallback);
        };

        ModelService.prototype.saveSettingsChanges = function (changesContainer) {
            this.datasourceService.saveSettingsChanges(Tools.clone(changesContainer));
        };
        ModelService.$inject = ['$rootScope', '$timeout', '$location', 'StorageService', 'HttpDatasourceService', 'DriveService'];
        return ModelService;
    })();
    Model.ModelService = ModelService;
})(Model || (Model = {}));

angular.module('exApp').service('ModelService', Model.ModelService);
//# sourceMappingURL=modelservice.js.map
