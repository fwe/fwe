/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts" />
/// <reference path="../../bower_components/pace/pace.d.ts" />
/// <reference path="httpDatasourceService.ts" />
/*
* (c) Roy  Smith 2013a. . . .
*/

module Model {

	export interface IModelService {
		getOutbox: () => {}[];
		getNew: () => {}[];
		getQuery: () => {}[];
		// saveQuery :()=>void
		getApproved: () => {}[];
		// saveApproved :()=>void
		getPaid: () => {}[];
		// savePaid :()=>void
		getRejected: () => {}[];
		// saveRejected :()=>void
		getSettings: () => Settings;
		getEmp: () => Array<Model.Emp>;
		getNoms: () => Array<Model.Nominal>;
		getFxrates: () => Array<Model.Fx>;
		getPaidDates: () => Array<string>;
		putUpdatedItem: (item: Model.Exitem) => void;
		newItem: () => Exitem;
		saveOutbox: () => void;
		refreshExitems: (force: boolean, eOrO:string) => void;
		calculateVat: (item: Model.Exitem) => void;

		processActionsEach: (arry: Array<Model.Exitem>) => void;
		processActionsAll: () => void;
		saveSettingsChanges: (changesContainer: {}) => void;

		lookupEmp: (email: string) => string;
		lookupNom: (nom: string) => string;

		deleteItem: (row: Model.Exitem, collection: Array<Model.Exitem>) => void;
		unDeleteItem: (row: Model.Exitem, collection: Array<Model.Exitem>) => void;



	}


	export class Exitem {
		/**
		* unique identifier
		*/
		id: string;
		/**
		* email. used as a unique identifier and foreign key to emps
		*/
		email: string;
		/**
		* type of item, Foreign, Local, Km
		*/
		type: string;
		/**
		* description of the expense item
		*/
		desc: string;
		/**
		* total amount in local currency
		*/
		amount: number;
		/**
		* net amount in local currency
		*/
		net: number;
		/**
		* vat amount in local currency
		*/
		vat: number;
		/**
		* status: N, A, Q, R, P
		*/
		stat: string;
		/**
		* nominal account mnemonic and lookup to nominal table
		*/
		nom: string;
		/**
		* comma separated tags eg. tag1,tag2
		*/
		tags: string
		/**
		* date incurred
		*/
		dateI: string;
		/**
		* date approved for payment. all items with same date form a payment batch
		*/
		dateP: string;
		/**
		* mileage
		*/
		km: number;
		/**
		* mileage rate this item
		*/
		kmrate: number;
		/**
		* currency expense was incurred in
		*/
		currency: string;
		/**
		* exch rate for this item
		*/
		fx: number;
		/**
		* amount in incurred currency
		*/
		fxamount: number;
		/**
		* net in incurred currency
		*/
		fxnet: number;
		/**
		* vat in incurred currency
		*/
		fxvat: number;
		/**
		* managers comment (usually a question)
		*/
		mcomment: string;
		/**
		* emp's comment (usually an answer)
		*/
		ecomment: string;
		/**
		* receipt url, may be a Driev id
		*/
		rurl: string;
		/**
		* action to be applied to this item
		* A, R, Q, P
		*/
		action: string;
		/**
		* orgn id
		*/
		orgnId: string;
	}


	// nominal: mnemonic, description
	export class Nominal {
		mnemonic: string;
		desc: string;
	}
	// fxrates: [{iso,rate}]
	export class Fx {
		iso: string;
		rate: number;
	}
	// emps: email, name, bank account number/sort, manager
	export class Emp {
		email: string;
		name: string;
		bank: string;
		manager; boolean;
		googleId: string;
		lang: string;
	}
	// mileage rate: rate
	export class Settings {
		kmrate: number;
		tags: string;
		coname: string;
		baseiso: string;
		orgnId: string;
		defaultVatRate: number;
		defaultLang: string;
	}



	export class ModelService implements IModelService {
		rootScope;
		timeOut;
		datasourceService:DatasourceModule2.IDatasourceService;
		driveService;
		storageService;

		liveArray = new Array<Model.Exitem>();
		outboxArray: Model.Exitem[] = [];
		newArray: Model.Exitem[] = [];
		queryArray: Model.Exitem[] = [];
		approvedArray: Model.Exitem[] = [];
		paidArray: Model.Exitem[] = [];
		rejectedArray: Model.Exitem[] = [];
		deletedArray: Model.Exitem[] = [];
		settings: Settings = new Settings();
		emp: Array<Model.Emp> = [];
		noms: Array<Model.Nominal>;
		fxrates: Array<Model.Fx>;
		// newArrayZ = ["q","b"];
		// lookup maps
		empMap;
		nomMap;

		paidDatesArray;
		
		eOrO:string='e'; // default is to fetch exitems for employee only  


		static $inject = ['$rootScope', '$timeout', '$location', 'StorageService', 'HttpDatasourceService', 'DriveService'];
		constructor($rootScope, $timeOut, $location:ng.ILocationService, storageService, datasourceService, driveService) {
			this.datasourceService = datasourceService;
			this.datasourceService = datasourceService;
			this.storageService = storageService;
			this.driveService = driveService;
			this.rootScope = $rootScope;
			this.timeOut = $timeOut;

			this.outboxArray = new Array<Model.Exitem>();
			this.liveArray = new Array<Model.Exitem>();
			this.newArray = new Array<Model.Exitem>();
			this.queryArray = new Array<Model.Exitem>();
			this.approvedArray = new Array<Model.Exitem>();
			this.paidArray = new Array<Model.Exitem>();
			this.rejectedArray = new Array<Model.Exitem>();
			this.deletedArray = new Array<Model.Exitem>();


			// if this is a manager page, fetch exitems for whole org
			if ($location.absUrl().indexOf("manager") > -1) {
				this.eOrO = 'o';
			}
			
			// load outbox from local storage
			try {
				this.storageService.fetchOutbox(this.outboxArray);
			} catch (ex) {
				console.log("[ms204] no stored outbox. OK for mgr.");
			}

			// fetch items (false to allow picking up items from the JSP window data)
			if (!window['clexunittesting']) {
				this.refreshAllModelData(this.eOrO);
			}
		}

		/**
		 * refreshes each of the data structures
		 * 
		 * if there is no access token yet, sleep and try again
		 */
		refreshAllModelData(eOrO:string) {
			try {
				console.log(gapi.auth.getToken().access_token.length);
			} catch (ex) {
				console.log("no token yet");
				this.timeOut(()=>{this.refreshAllModelData(eOrO)}, 500);
				return;
			}
			this.refreshExitems(false);
			/*
			*  fetch settings
			*/
//			debugger;
			this.datasourceService.getSettings(this.settings);
			this.emp = new Array<Model.Emp>();
			this.datasourceService.getEmp(this.emp);
			this.fxrates = new Array<Model.Fx>();
			this.datasourceService.getFxrates(this.fxrates);
			this.noms = new Array<Model.Nominal>();
			this.datasourceService.getNoms(this.noms);
		}

		/**
		 * refreshes all item arrays from server
		 */
		refreshExitems(force: boolean): void {
			this.liveArray.length = 0;
			this.datasourceService.getLive(force, this.liveArray, this.eOrO, { parent: this, callback: this.parseLiveArray });
		}

		/**
		 *
		 * @param parent
		 */
		clearDownArrays(parent: any): void {
			parent.newArray.length = 0;
			parent.queryArray.length = 0;
			parent.approvedArray.length = 0;
			parent.paidArray.length = 0;
			parent.rejectedArray.length = 0;
			parent.deletedArray.length = 0;
		}
		/**
		* iterates the live (ie. all) array and populates the individual arrays
		* this is invoked as a callback to the http get
		*/
		parseLiveArray(parent): void {
			console.log("in parselivearray callback");
			parent.clearDownArrays(parent);
			console.log(parent.liveArray);
			for (var i = 0; i < parent.liveArray.length; i++) {
				var item = parent.liveArray[i];
				switch (item.stat) {
					case 'N':
						parent.newArray.push(item);
						break;
					case 'Q':
						parent.queryArray.push(item);
						break;
					case 'A':
						parent.approvedArray.push(item);
						break;
					case 'P':
						parent.paidArray.push(item);
						break;
					case 'R':
						parent.rejectedArray.push(item);
						break;
				}
			}
			console.log(parent.newArray);
			// stop loading animation
			try {
				Pace.stop();
			} catch (ex) { }

		}
		// getNewZ() {
		// 	return this.newArrayZ;
		// }
		lookupEmp(email: string) {
			if (!this.empMap) {  // first time, make the map
				this.empMap = {};
				for (var i = 0; i < this.emp.length; i++) {
					if (!this.empMap[this.emp[i]['email']]) {
						this.empMap[this.emp[i]['email']] = this.emp[i];
					}
				}
			}
			return "" + this.empMap[email]['name'];
		}

		lookupNom(nom: string) {
			if (!this.nomMap) {  // first time, make the map
				this.nomMap = {};
				for (var i = 0; i < this.noms.length; i++) {
					if (!this.nomMap[this.noms[i]['mnemonic']]) {
						this.nomMap[this.noms[i]['mnemonic']] = this.noms[i];
					}
				}
			}
			return "" + this.empMap[nom]['desc'];
		}


		getSettings(): Settings {
			return this.settings;
		}
		getTagsAsArray() {
			if (this.settings && this.settings.tags) {
				return this.settings.tags.split(',');
			} else {
				return [];
			}
		}
		getEmp(): Array<Model.Emp> {
			return this.emp;
		}
		getNoms(): Array<Model.Nominal> {
			return this.noms;
		}
		getFxrates(): Array<Model.Fx> {
			return this.fxrates;
		}
		getOutbox() {
			return this.outboxArray;
		}
		getNew() {
			return this.newArray;
		}
		getQuery() {
			return this.queryArray;
		}
		getApproved() {
			return this.approvedArray;
		}
		getPaid() {
			return this.paidArray;
		}
		getRejected() {
			return this.rejectedArray;
		}
		getDeleted() {
			return this.deletedArray;
		}

		/**
		* returns an array of uniq dates from the paid array
		*/
		getPaidDates(): Array<string> {
			if (!this.getPaid()) {  // if no paid items yet, return an empty array
				return [];
			}
			/* following if stops newly paid dates appearing
			if (this.paidDatesArray) { // if array already built, return it
				return this.paidDatesArray;
			}
			*/
			/* here is no array, so create it */
			var dateMap = {};
			// iterate array and store in map
			for (var i = 0; i < this.getPaid().length; i++) {
				dateMap[this.getPaid()[i].dateP] = this.getPaid()[i];
			}
			// turn map back into an array
			var date;
			this.paidDatesArray = [];
			for (date in dateMap) {
				this.paidDatesArray.unshift(date);
				//this.paidDatesArray[this.paidDatesArray.length] = date;
			}
			return this.paidDatesArray;
		}

		/**
		* deletes item  by...
		*  deleting it from existing array
		*  deleting it from liveArray
		*  insert it into deleted array
		*  calling delete endpoint (maybe not, maybe only in Outbox)
		*/
		deleteItem(row: Model.Exitem, collection: Array<Model.Exitem>) {
			collection.splice(collection.indexOf(row), 1);
			this.liveArray.splice(this.liveArray.indexOf(row), 1);
			this.deletedArray.push(row);
			// this.datasourceService.deleteItem(row);	
			// TODO save outbox to sorage
			this.storageService.saveOutbox(this.outboxArray);
		}

		/**
		* undeletes item  by...
		*  adding it to liveArray
		*  add it to the array indicted by stat
		*  deleting it from deleted array
		*  
		*  calling insert endpoint (maybe not, maybe only in Outbox)
		*/
		unDeleteItem(row: Model.Exitem) {
			this.liveArray.push(row);
			this.deletedArray.splice(this.deletedArray.indexOf(row), 1);
			// this.datasourceService.putUpdatedItem(row);		
			switch (row.stat) {
				case 'O':
					this.outboxArray.push(row);
					break;
				case 'N':
					this.newArray.push(row);
					break;
				case 'Q':
					this.queryArray.push(row);
					break;
				case 'A':
					this.approvedArray.push(row);
					break;
				case 'P':
					this.paidArray.push(row);
					break;
				case 'R':
					this.rejectedArray.push(row);
					break;
			}
			this.storageService.saveOutbox(this.outboxArray);


		}


		/**
		* does everything for a new Expense Item
		* creates the object, adds it to array, stores in local storage
		*/
		newItem() {
			var exitem = new Model.Exitem();
			// provide some initial values, notably ID
			// the server will prepend substr(email,0,3) to enforce uniqueness
			exitem.id = "" + new Date().getTime();
			exitem.orgnId = this.getSettings().orgnId;
			exitem.stat = "O";  // starts in Outbox
			exitem.action = "N";  // starts in Outbox
			exitem.currency = this.getSettings().baseiso;
			exitem.kmrate = this.getSettings().kmrate;
			exitem.tags = "";
//			this.getOutbox().push(exitem); 21/7/14 I should only push to the array on a Save
			return exitem;
		}

		/**
		* called when an item inthe outbox has been updated
		* saves to local storeage
		* 
		* if item is provided, push this to the array before saving
		*/
		saveOutbox(exitem?: Model.Exitem) {
			if (exitem) { // if we have an item to save
				if (this.getOutbox().indexOf(exitem) < 0) { // and it's not a;ready in the array
					this.getOutbox().push(exitem); // then add it
				}
			}
			this.storageService.saveOutbox(this.outboxArray);
		}

		/**
		* applies the default vatrate to the amount to derive 
		* the net and vat figures
		* 
		* ifno defaultvat rate,vat is zero and net is amount
		*/
		calculateVat(item: Model.Exitem) {
			console.log(1 * this.settings.defaultVatRate + 1);
			if (!this.settings.defaultVatRate || this.settings.defaultVatRate == 0) {
				item.net = item.amount;
				item.vat = 0;
			} else {
				item.net = Math.round(item.amount * 100 / (1 * this.settings.defaultVatRate + 1)) / 100;
				item.vat = Math.round((item.amount - item.net) * 100) / 100;
			}

		}

		/**
		* applies the default vatrate to the amount to derive 
		* the net and vat figures
		*/
		calculateFx(focusItem: Model.Exitem) {
			if (!focusItem.fx) {
				return;   // do nothing if no rate
			}
			focusItem.amount = focusItem.fxamount * focusItem.fx;
			focusItem.amount = (typeof focusItem.amount != "number" ? 0 : focusItem.amount);
			focusItem.net = focusItem.fxnet * focusItem.fx;
			focusItem.net = (typeof focusItem.net != "number" ? 0 : focusItem.net);
			focusItem.vat = focusItem.fxvat * focusItem.fx;
			focusItem.vat = (typeof focusItem.vat != "number" ? 0 : focusItem.vat);
		}

		kmChanged(item: Model.Exitem) {
			item.amount = Math.round((item.km * item.kmrate) * 100) / 100;;
			item.net = item.amount;
			item.vat = 0;
		}


		processActionsAll() {
			this.processActionsEach(this.queryArray);
			this.processActionsEach(this.approvedArray);
			this.processActionsEach(this.newArray);
		}
		/**
		* itreates the given array and executes the action property by 
		* deleting from the current array and pushing onto the appropriate target array
		*
		* if current array is outbox, saves the newly emptied outbox to local storage
		*/
		processActionsEach(arry: Array<Model.Exitem>) {
			var isOutbox = false;
			var len = arry.length;
			for (var i = 0; i < len; i++) {
				// always zero coz we're deleting each iteration
				// this may seem odd,but remember that the action might re-add it back
				var item: Model.Exitem = arry[0];
				arry.splice(0, 1);  // delete from current array
				var currentStat = item.stat;
				item.stat = item.action; // update the item to its new status
				console.log("a=" + item['action']);
				// and add somewhere
				switch (item['action']) {
					case 'N':
						this.newArray.push(item);
						item.action = "A";  // default next action
						if (currentStat == 'O') {
							isOutbox = true;  // hey, this is the outbox
						}
						break;
					case 'A':
						this.approvedArray.push(item);
						item.action = "P";  // default next action
						break;
					case 'Q':
						if (currentStat == 'Q') { // it's already a Q, so this is an employee send action
							this.newArray.push(item);
							item.action = "N";  // default next action						
						} else {  // this is a manager making it a new query
							this.queryArray.push(item);
							item.action = "N";  // default next action						
						}
						break;
					case 'R':
						this.rejectedArray.push(item);
						break;
					case 'P':
						this.paidArray.push(item);
						// TODO - need to do all pay processing (ie. remittance and nominal posting ssheets)
						item.dateP = new Date().toLocaleDateString();
						break;
				}
				// upload the updated item
				this.datasourceService.putUpdatedItem(Tools.clone(item)); // TODO is this clone safe?
//				console.log(item);
				if (isOutbox) {  // if it's the outbox, save it to local
					this.storageService.saveOutbox(arry);
				/* this logic is about images to drive. Since I now simply encode the image into the rurl, it is redundant
//					this.uploadImageThenExitem(item); // and upload any image
				*/
				}
				console.log("TODO: update live array, recreate newArray and send updates to server");
				console.log(this.newArray);
			}
		}

		/**
		 * update the item by putting to the API
		 * @param exitem
		 */
		putUpdatedItem(item) {
			
			console.warn(111);
			this.datasourceService.putUpdatedItem(Tools.clone(item));
		}

		/**
		* retrieves the stored file object from local storage
		* then uploads it to drive.
		* On completion of the upload, grab the webUrl and update the exitem on appengine
		*/

		uploadImageThenExitem(exitem: Model.Exitem) {
			console.log("in uploadImageThenExitem");
			var parent = this;
			var title = exitem.desc;
			if (title == undefined) {
				title = "";
			}
			if (title.length > 20) {
				title = title.substr(0, 19);
			}
			title =  title;



			/* 
			* define a storage callback for when the file object has been fetched
			*/
			var fetchImageFileCallback = function() {
				console.log("in fetchImageFileCallback with storageObject...");
				// console.log(this);
				var fileObject = JSON.parse(this['result']);
				console.log("after all that, we have the original file object ...");
				// console.log(fileObject);
				parent.driveService.uploadFile(title, fileObject, function(file) {
					console.log("in drivecallback with file...");
					console.log(file);
					exitem.rurl = file['webContentLink'].replace("&export=download", "");
					console.log(exitem.rurl);
					// now we have a proper rurl, update the item on the server

					parent.datasourceService.putUpdatedItem(exitem);
					parent.rootScope.$digest(); // flush $http requests
				});
			};
			this.storageService.fetchImageFile(exitem.rurl, fetchImageFileCallback);
			// $scope.$apply(function() {        
			//     $scope.theFile = element.files[0];
			// })
		}
		/**
		* pushes changes to the settings object up to server
		* the first param (eg settings) goes into the URL so must match a server DTO name
		*/
		saveSettingsChanges(changesContainer: {}) {
//			debugger;
			this.datasourceService.saveSettingsChanges(Tools.clone(changesContainer));
		}
	}
}
// declare var angular;
angular.module('exApp')
	.service('ModelService', Model.ModelService);