/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="modelService.ts" />

var TService;
(function (TService) {
    var StorageService = (function () {
        function StorageService($timeout, $window, $rootScope) {
            console.log('storage openning...');
            this.window = $window;
            this.rootScope = $rootScope;
            var parent = this;
            try  {
                Lawnchair;
            } catch (ex) {
                return;
            }
            this.store = Lawnchair({
                adapter: 'indexed-db',
                table: 'CleverExpense'
            }, function (e) {
                console.log('...storage open');

                // parent.store['nuke']();
                parent.saveObject("foo", "bar");
                var targetObject = { a: "aa", callback: function () {
                        console.log("in callback with " + this.result);
                    } };
                parent.fetchObject("foo", targetObject);
                setTimeout(function () {
                    console.log("got ...");
                    console.log(targetObject);
                }, 500);
            });
        }
        /**
        * saves object under key, having prepended email from window.config
        */
        StorageService.prototype.saveObject = function (key, object) {
            console.log("saving " + key + " ");
            this.store.save({
                key: this.makeShardedKey(key),
                value: object
            });
        };

        StorageService.prototype.makeShardedKey = function (key) {
            console.warn("need to incorporate email into key");

            //			return this.window.config.e + ":"+key
            return key;
        };

        /**
        * fetches the storageobject identified by email:key
        * stores the result in object:result
        * if object has a callback (ie. object{cb:(result)}), it is called with the result
        */
        StorageService.prototype.fetchObject = function (key, object) {
            var parent = this;
            var targetObject = object;
            this.store['get'](this.makeShardedKey(key), function (doc) {
                // console.log("store returned "+doc.value);
                // console.log(deferred);
                console.log("...      fetched " + parent.makeShardedKey(key));
                var o;
                if (!doc) {
                    console.log("[SS48] key not found k=" + parent.makeShardedKey(key));
                    return;
                }

                // console.log(doc);
                console.log(targetObject);
                console.log(doc.key);

                // console.log(doc.value);
                parent.rootScope.$apply(function () {
                    targetObject.result = doc.value;
                    console.log(targetObject);
                    if (targetObject.callback) {
                        targetObject.callback();
                    }
                });
            });
        };

        /**
        * convenience method to fetch Outbox by calling fetchObject
        * xxpects to be called with a a key (the image name) and a callback
        */
        StorageService.prototype.fetchOutbox = function (arry) {
            var fetchObj = { callback: function () {
                    for (var i = 0; i < this.result.length; i++) {
                        console.log("fetched desc " + this.result[i]['desc']);
                        arry.push(this.result[i]);
                    }
                    console.log("ss93 fetched...");
                    console.log(arry);
                } };
            this.fetchObject("o", fetchObj);
        };

        /**
        * convenience method to save Outbox by calling saveObject
        */
        StorageService.prototype.saveOutbox = function (arry) {
            this.saveObject("o", arry);
        };

        /**
        * convenience method to delete a saved "file" object
        */
        StorageService.prototype.deleteImageFile = function (key) {
            console.log("in deleteImageFile");
            this.store['remove'](this.makeShardedKey(key));
        };

        /**
        * convenience method to fetch a saved "file" object for an image by calling fetchObject
        */
        StorageService.prototype.fetchImageFile = function (key, callback) {
            console.log("in fetchImageFile with key " + key);
            if (!key) {
                callback();
            }
            var fetchObj = { callback: callback };
            this.fetchObject(key, fetchObj);
        };

        /**
        * convenience method to save the media content containt in a "file"
        * object by calling saveObject
        * saves the base64 data uri in the exitem
        */
        StorageService.prototype.saveImageFile = function (file, exitem) {
            if (!exitem) {
                console.error("[sc140] undefined exitem in saveImageFile");
            }
            var reader = new FileReader();
            var parent = this;
            var title = file['name'];
            reader.onerror = function (ev) {
                console.log("filereader error...");
                console.log(ev);
            };
            reader.onload = function (e) {
                console.log("in reader onload with exitem ...");
                console.log(exitem);
                var contentType = file['type'] || 'application/octet-stream';
                var metadata = {
                    'title': title,
                    'mimeType': contentType
                };

                // store the media content in the file object
                file['base64Data'] = btoa(reader.result);

                // data:[mimetype][;base64],[data]
                exitem.rurl = "data:" + file['type'] + ";base64," + file['base64Data'];
                try  {
                    window['mainCtrl'].refs.rootScope.$digest();
                } catch (ex) {
                }

                // console.log(base64Data.length+";"+base64Data);
                // then save the fat file object
                // if I put the data uri in the exitem object, it will get saved automatically which saves me housekeeping two objects
                // parent.saveObject(file['name'], JSON.stringify(file));
                console.log("NOT SAVED !!!!! should be saved in item !!!saved as " + file['name']);
            };
            reader['readAsBinaryString'](file);
        };
        StorageService.$inject = ['$timeout', '$window', '$rootScope'];
        return StorageService;
    })();
    TService.StorageService = StorageService;
})(TService || (TService = {}));
;

// declare var angular;
angular.module('exApp').service('StorageService', TService.StorageService);
//# sourceMappingURL=storageservice.js.map
