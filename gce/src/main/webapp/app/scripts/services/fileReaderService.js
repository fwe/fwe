/**
* @copyright Roy Smith, 2014
*/
var FileReaderServiceModule;
(function (FileReaderServiceModule) {
    

    var FileReaderService = (function () {
        function FileReaderService($q, $log) {
            this.refs = {};
            this.refs = { q: $q, log: $log };
        }
        /**
        * this is the main method call (for text files at least)
        *
        * @param file element[0] of the filedata array
        * @param scope the scope of the input button
        *
        * @return a promise
        */
        FileReaderService.prototype.readAsText = function (file, scope) {
            var deferred = this.refs['q'].defer();
            console.log(file);
            var reader = this.getReader(deferred, scope);
            reader.readAsText(file);

            return deferred.promise;
        };

        /**
        * alternative method which returns a data url   (eg data::base64 sdfsdfcsdfcsfd)
        */
        FileReaderService.prototype.readAsDataURL = function (file, scope) {
            var deferred = this.refs['q'].defer();
            console.log(file);
            var reader = this.getReader(deferred, scope);
            reader.readAsDataURL(file);

            return deferred.promise;
        };

        FileReaderService.prototype.onLoad = function (reader, deferred, scope) {
            return function () {
                console.log(402);
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };

        FileReaderService.prototype.onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };

        FileReaderService.prototype.onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress", {
                    total: event.total,
                    loaded: event.loaded
                });
            };
        };

        FileReaderService.prototype.getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = this.onLoad(reader, deferred, scope);
            reader.onerror = this.onError(reader, deferred, scope);
            reader.onprogress = this.onProgress(reader, scope);
            return reader;
        };
        FileReaderService.$inject = ['$q', '$log'];
        return FileReaderService;
    })();
    FileReaderServiceModule.FileReaderService = FileReaderService;
    ;
})(FileReaderServiceModule || (FileReaderServiceModule = {}));

// declare var angular;
angular.module('exApp').service('FileReaderService', FileReaderServiceModule.FileReaderService);
//# sourceMappingURL=fileReaderService.js.map
