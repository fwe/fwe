/**
 * @copyright Roy Smith, 2014
 */

module FileReaderServiceModule {


	/**
	 * 
	 * creates a promise based interface to a file input type
	 * 
	 * typical usage is 
	 * 
	 * 
	 * /**
	 * called by onchange of the file input element
	 * 
	 * need to make sure the dialog with the file button is an ng-if. This will cause it to be recreated each time, 
	 * thus resetting the button for an onchange 
	 * 
	 * NB. the file input must be element [0] of the file element, thus 
	 * 
	 * 	<input type="file" 
	 *        onchange="angular.element(this).scope().vmi.setFile(this.files[0],angular.element(this).scope())"   
	 *        accept="image/*;capture=camera"  >

	   setFile(fileInput, scope) {
		// get the file's contents and a promise
		var p = this.refs.fileReaderService.readAsText(fileInput, scope);
		p.then(function (data) { /* when the file's contents have been read ... 
			
	}
	 * 
	 */
	export interface IFileReaderService {
	}

	export class FileReaderService implements IFileReaderService {

		refs = {};
		// constructor
		static $inject = ['$q', '$log'];
		constructor($q, $log) {
			this.refs = { q: $q, log: $log };
		}


		/**
		 * this is the main method call (for text files at least)
		 * 
		 * @param file element[0] of the filedata array 
		 * @param scope the scope of the input button
		 * 
		 * @return a promise
		 */
		readAsText(file, scope) {
			var deferred = this.refs['q'].defer();
			console.log(file);
			var reader = this.getReader(deferred, scope);
			reader.readAsText(file);

			return deferred.promise;
		}



		/**
		 * alternative method which returns a data url   (eg data::base64 sdfsdfcsdfcsfd)
		 */
		readAsDataURL(file, scope) {
			var deferred = this.refs['q'].defer();
			console.log(file);
			var reader = this.getReader(deferred, scope);
			reader.readAsDataURL(file);

			return deferred.promise;
		}


		onLoad(reader, deferred, scope) {
			return function() {
				console.log(402);
				scope.$apply(function() {
					deferred.resolve(reader.result);
				});
			};
		}

		onError(reader, deferred, scope) {
			return function() {
				scope.$apply(function() {
					deferred.reject(reader.result);
				});
			};
		}

		onProgress(reader, scope) {
			return function(event) {
				scope.$broadcast("fileProgress",
					{
						total: event.total,
						loaded: event.loaded
					});
			};
		}

		getReader(deferred, scope) {
			var reader = new FileReader();
			reader.onload = this.onLoad(reader, deferred, scope);
			reader.onerror = this.onError(reader, deferred, scope);
			reader.onprogress = this.onProgress(reader, scope);
			return reader;
		}



	};
}
// declare var angular;
angular.module('exApp')
	.service('FileReaderService', FileReaderServiceModule.FileReaderService);
