/**
 * @copyright CleverThinking Ltd 2014
 */
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/angular-resource.d.ts" />
/**
 * serves drive resource objects
 * 
 * 
 */
module ExResourceServiceModule {

	/**
	 * my window object to store auth related stuff
	 * 
	 * currently defined in driveq.js
	 */
	export interface IWinAuth {
		immediate: boolean; // sets the mode of any auth request, immediaetmeans background
		validToken: boolean; // true if the current token is valid
		tokenExpiryMilliseconds: number;
		gapiLoaded: boolean;  // true once gapi has fully loaded, set by onload=
		requestingToken: boolean; // used to stop retry loops around a login
		fetchToken:()=>void; // this is the way we get a new access token 
	}
	

	export interface IExitemsResource extends ng.resource.IResource<IExitemsResource> {
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface IExitemsResourceClass extends ng.resource.IResourceClass<IExitemsResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		list:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}

	export interface IEmpsResource extends ng.resource.IResource<IEmpsResource> {
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface IEmpsResourceClass extends ng.resource.IResourceClass<IEmpsResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		list:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}
	
	
	export interface INomsResource extends ng.resource.IResource<INomsResource> {
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface INomsResourceClass extends ng.resource.IResourceClass<INomsResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		list:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}
	
	
	export interface IFxratesResource extends ng.resource.IResource<IFxratesResource> {
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface IFxratesResourceClass extends ng.resource.IResourceClass<IFxratesResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		list:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}
	
	export interface ISettingsResource extends ng.resource.IResource<ISettingsResource> {
		$promise:ng.IPromise<any>;
	}

	// custom resource
	export interface ISettingsResourceClass extends ng.resource.IResourceClass<ISettingsResource> {
		// Overload get to accept our custom parameters
		update:(id:{},body:{})=>any;
		list:(id:{},body:{})=>any;
		insert:(id:{},body:{})=>any;
		$promise:ng.IPromise<any>;
	}

	export interface IPromisedResult {
		promise: {}
	}

	export interface IExResourceService {
		oauthAccessTokenGetterFunction: () => string
		DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION: () => string
		refs: {}
		getExitemsResource(): ng.resource.IResourceClass<ng.resource.IResource<any>>;
		getAllExitems:(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any)=> ng.IPromise<any>; 
		getSettingsResource(): ng.resource.IResourceClass<ng.resource.IResource<any>>;
		getAllSettings:(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any)=> ng.IPromise<any>; 
	
//		insertExitem(meta:Model.Exitem, content:string);
//		uploadMedia(id:string, mimeType:string, content:string);
		//		getAboutResource(): ng.resource.IResource<ng.resource.IResourceClass<any>>;
	}

	export class ExResourceService implements IExResourceService {
		oauthAccessTokenGetterFunction: () => string;
		
		// define the default means of getting an access token
		DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION = ():string => {
			console.log("in default fixouath");
			if (gapi.auth && gapi.auth.getToken() && gapi.auth.getToken().access_token) { // if it looks like we have a token in gapi
				console.log("returning "+gapi.auth.getToken().access_token);
				return gapi.auth.getToken().access_token // return it
			} 
			// else (no token poss because we've emerged from a sleep)
			try { //cn use only
				if(window['auth']) {
					window['auth'].validToken = false;
					window['auth'].fetchToken();  // fetch a new one. this will take time, so return null for now
				}
			} catch (ex) {};
			console.log('returning null');
			return null;
		};
		FWE_URL = '/api/v1/'
		EXITEMS_URL = this.FWE_URL + 'exitems';
		EMPS_URL = this.FWE_URL + 'emps';
		NOMS_URL = this.FWE_URL + 'noms';
		FXRATES_URL = this.FWE_URL + 'fxrates';
		SETTINGS_URL = this.FWE_URL + 'settings';
		ABOUT_URL = this.FWE_URL + 'about';
		CHANGE_URL = this.FWE_URL + 'changes';
		refs: { q: ng.IQService; resourceService: ng.resource.IResourceService; http: ng.IHttpService };

		// constructor
		static $inject = ['$q', '$http', '$resource', '$log'];
		constructor($q, $http, $resource: ng.resource.IResourceService, $log) {		   
			  $http.defaults.useXDomain = true;

//			var oauthurl = "https://accounts.google.com/o/oauth2/auth?foo=bar&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.file+email+openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.install+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Ftasks&state=%2Fprofile&redirect_uri=http%3A%2F%2Fwww.clevernote.co%2Focb&response_type=token&client_id=698624257995.apps.googleusercontent.com&include_granted_scopes=true&login_hint=roy%2Esmith%2Eesq%40gmail%2Ecom";
//			 $http({method: 'GET', url: oauthurl}).
//    success(function(data, status, headers, config) {
//		console.log("3 success");
//    }).
//    error(function(data, status, headers, config) {
//		console.log("3 fail"+status+data);
//    });
			this.refs = { q: $q, resourceService: $resource, http: $http, log: $log };
		}

		/**
		 * sets the function to be called when we need an access token
		 * if called with no params, set default. This has the effect of suppressing teh warning message.
		 */
		setOauthAccessTokenGetterFunction(_oauthAccessTokenGetterFunction?: () => string) {
			var self = this;
			if (_oauthAccessTokenGetterFunction) {
				this.oauthAccessTokenGetterFunction = _oauthAccessTokenGetterFunction;
			} else {
				this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
			}
		}

		/**
		 * get an Exitems resource
		 * 
		 */
		getExitemsResource(): IExitemsResourceClass {
			this.fixOauth();
			var File = <IExitemsResourceClass> this.refs.resourceService(this.EXITEMS_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST', params: {access_token: this.fixOauth()} }, // custom insert method
					update: { method: 'PUT', params: {access_token: this.fixOauth()} }, // custom update method
					list: { method: 'GET', params: {access_token: this.fixOauth()} }, // custom list method
					remove: { method: 'DELETE', params: {access_token: this.fixOauth()} }, // custom delete method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}



		/**
		  * get all exitems, with no q set, defaults to org
		  * 
		  * q can be s=e for just the employee, or s=9999 where 9999 is an allShardPin available from console
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllExitems(destArray: Array<any>, q: {}, maxResults: string, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var items: IExitemsResourceClass;
			if (nextLink) {
				this.fixOauth();
				items = <IExitemsResourceClass> this['refs'].resourceService(nextLink);
			} else {
				items = this.getExitemsResource();
			}
				var qq={};
				if (q) {
					qq=q;
				}
				items.list(qq, function(data) { // do the
				

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					this.getAllExitems(destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
		}



		/**
		 * get an Noms resource
		 * 
		 */
		getNomsResource(): INomsResourceClass {
			this.fixOauth();
			var File = <INomsResourceClass> this.refs.resourceService(this.NOMS_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST', params: {access_token: this.fixOauth()} }, // custom insert method
					update: { method: 'PUT', params: {access_token: this.fixOauth()} }, // custom update method
					list: { method: 'GET', params: {access_token: this.fixOauth()} }, // custom list method
					remove: { method: 'DELETE', params: {access_token: this.fixOauth()} }, // custom delete method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}



		/**
		  * get all files for user (subject to scope)
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllNoms(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var items: INomsResourceClass;
			if (nextLink) {
				this.fixOauth();
				items = <INomsResourceClass> this['refs'].resourceService(nextLink);
			} else {
				items = this.getNomsResource();
			}
			items.list({}, function(data) { // do the

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					this.getAllNoms(destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
		}

		/**
		 * get an Fxrates resource
		 * 
		 */
		getFxratesResource(): IFxratesResourceClass {
			this.fixOauth();
			var File = <IFxratesResourceClass> this.refs.resourceService(this.FXRATES_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST', params: {access_token: this.fixOauth()} }, // custom insert method
					update: { method: 'PUT', params: {access_token: this.fixOauth()} }, // custom update method
					list: { method: 'GET', params: {access_token: this.fixOauth()} }, // custom list method
					remove: { method: 'DELETE', params: {access_token: this.fixOauth()} }, // custom delete method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}



		/**
		  * get all files for user (subject to scope)
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllFxrates(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var items: IFxratesResourceClass;
			if (nextLink) {
				this.fixOauth();
				items = <IFxratesResourceClass> this['refs'].resourceService(nextLink);
			} else {
				items = this.getFxratesResource();
			}
			items.list({}, function(data) { // do the

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					this.getAllEmps(destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
		}

		/**
		 * get an Emps resource
		 * 
		 */
		getEmpsResource(): IEmpsResourceClass {
			this.fixOauth();
			var File = <IEmpsResourceClass> this.refs.resourceService(this.EMPS_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST', params: {access_token: this.fixOauth()} }, // custom insert method
					update: { method: 'PUT', params: {access_token: this.fixOauth()} }, // custom update method
					list: { method: 'GET', params: {access_token: this.fixOauth()} }, // custom list method
					remove: { method: 'DELETE', params: {access_token: this.fixOauth()} }, // custom delete method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}



		/**
		  * get all files for user (subject to scope)
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllEmps(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var items: IEmpsResourceClass;
			if (nextLink) {
				this.fixOauth();
				items = <IEmpsResourceClass> this['refs'].resourceService(nextLink);
			} else {
				items = this.getEmpsResource();
			}
			items.list({}, function(data) { // do the

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					this.getAllEmps(destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
		}

				/**
		 * get an Settings resource
		 * 
		 */
		getSettingsResource(): ISettingsResourceClass {
			this.fixOauth();
			var File = <ISettingsResourceClass> this.refs.resourceService(this.SETTINGS_URL + "/:id",{},
				//				{ "access_token": '' + window['gapi'].auth.getToken().access_token },/* default values */
				{
					/* add a custom update method (defaults are
					 { 'get':    {method:'GET'},
					 'save':   {method:'POST'},
					 'query':  {method:'GET', isArray:true},
					 'remove': {method:'DELETE'},
					 'delete': {method:'DELETE'} };*/
					insert: { method: 'POST', params: {access_token: this.fixOauth()} }, // custom insert method
					update: { method: 'PUT', params: {access_token: this.fixOauth()} }, // custom update method
					list: { method: 'GET', params: {access_token: this.fixOauth()} }, // custom list method
					remove: { method: 'DELETE', params: {access_token: this.fixOauth()} }, // custom delete method
					upload: {
						method: 'POST',
						params: { uploadType: "multipart" },
						headers: { enctype: 'multipart/related; boundary="foo_bar_baz"' }
					}
				});

			return File;
		}



		/**
		  * get all files for user (subject to scope)
		  *
		  * returns a promise, which on then returns the array of files
		  * @param destArray the viewmodel array that will be appended to
		  * @param self ptr to the scope
		  * @param nextLink passed from one recursion to the next
		  * @param deferred passed from one recursion to the next
		  * @returns {*}
		  */
		//		create a version that takes an object argument passed to get, eg [q: title = ''
		getAllSettings(destArray: Array<any>, q: string, maxResults: string, nextLink: string, deferred: any): ng.IPromise<any> {
			this.fixOauth();
			// if this is the first call, create a deferred object
			if (!deferred) {
				deferred = this.refs.q.defer();
			}
			// if there is a nextlink, that is the url, else we use the standard drive url
			var items: ISettingsResourceClass;
			if (nextLink) {
				this.fixOauth();
				items = <ISettingsResourceClass> this['refs'].resourceService(nextLink);
			} else {
				items = this.getSettingsResource();
			}
			items.list({}, function(data) { // do the

				destArray.push.apply(destArray, data['items']); // append files to a growing array
				//			       self['refs'].log.debug("cumulative count = "+destArray.length);
				if (data['nextLink']) { // if there is more, then recurse
					this.getAllSettings(destArray, undefined, undefined, self, data['nextLink'], deferred);
				} else { // if we're done, resolve the promise
					//                console.log("allfetched "+self['allFiles'].length);
					deferred['resolve'](self['allFiles']);
				}
			});

			return deferred['promise'];
	}	


		/**
		 * do whatever is needed to inject ouath
		 * this will prb need to become an injector which deals with 401
		 */
		fixOauth():string {
			console.log("adding oauth header");
//			debugger;
			if (!this.oauthAccessTokenGetterFunction) {
				console.warn("[dRS336] You should call setOauthAccessTokenGetterFunction with a getter function. Using default " + this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION);
				this.oauthAccessTokenGetterFunction = this.DEFAULT_OAUTH_ACCESS_TOKEN_GETTER_FUNCTION;
			}
			var at = this.oauthAccessTokenGetterFunction();
			this.refs.http.defaults.headers.common['Authorization'] = 'Bearer ' + at;
			return at;
		}

		/**
	 * clones an object structure into a pre-existing object.
	 * 
	 * @param obj
	 *          the object to clone
	 * @param temp
	 *          the target object to clone into
	 * @param exclusions
	 *          an array of property names to exclude from the cloning
	 * @returns the cloned object
	 */
		cloneInto(obj, temp, exclusions) {
			//		debugger; 
			if (obj == null || typeof (obj) != 'object')
				return obj;

			//		console.log(exclusions);

			for (var key in obj) {
				if (exclusions) {
					//				console.log("ex="+exclusions);
				}
				if (exclusions && (exclusions.indexOf(key) > -1)) {
					continue;
				}
				//			console.log(temp);
				//			console.log(key); 
				if (temp == undefined) {
					//				debugger;
				}
				temp[key] = this.clone(obj[key], undefined);
			}
			return temp;
		}
		/**
 * clones an object structure.
 * 
 * @param obj
 *          the object to clone
 * @param exclusions
 *          an array of property names to exclude from the cloning
 * @returns the cloned object
 */
		clone(obj, exclusions) {
			//		debugger; 
			if (obj == null || typeof (obj) != 'object')
				return obj;
			var temp = obj.constructor(); // changed
			if (!temp) {
				temp = {};
			}

			//		console.log(exclusions);

			for (var key in obj) {
				if (exclusions) {
					//				console.log("ex="+exclusions);
				}
				if (exclusions && (exclusions.indexOf(key) > -1)) {
					continue;
				}
				//			console.log(temp);
				//			console.log(key); 
				if (temp == undefined) {
					//				debugger;
				}
				temp[key] = this.clone(obj[key], undefined);
			}
			return temp;
		}
	}
	
}
// declare var angular;
angular.module('exApp').service('ExResourceService', ExResourceServiceModule.ExResourceService);
