'use strict';

console.log("==init 1b");

// var app = angular.module('exApp',
// ['ngSanitize','ngRoute','ngGrid','ui.bootstrap']);
var app = angular.module('exApp', [ 'ngResource', 'smartTable.table',
		'ui.bootstrap', 'decipher.tags' ]);

app
// .config(function ($routeProvider) {
.config(function() {
	console.log("==init 2");
	// $routeProvider
	// .when('/', {
	// templateUrl: 'views/main.html',
	// controller: 'MainCtrl'
	// })
	// .otherwise({
	// redirectTo: '/'
	// });

});

/*
 * gapi stuff
 */

window.auth = {
	immediate : false,  // first time through, immediate is false, subsequently true
	fetchToken: function (cback) {	
		gapi.auth.authorize(
				{
					client_id : "822097196702-jmapkqnikm7ql5mp4619748f33j7ev4l.apps.googleusercontent.com",
					scope : "email ",
					immediate : window.auth.immediate
				}, cback) 
		}
}

/**
 * Called when the client library is loaded to start the auth flow.
 */
function handleClientLoad() {
	console.log("inhclientload");
	window.config = {
		gapiready : true
	};
	window.auth.fetchToken(function() {
						console.log("authed "
								+ gapi.auth.getToken().access_token);
						// this.refs.exResourceService.fixOauth();
						// this.haveToken = true;
						// this.refs.scope.$apply(() => { this.haveToken = true
						// });
					});
	window.auth.immediate = true; // subsequent calls are immediate false
	// window.setTimeout(checkAuth, 1);
}