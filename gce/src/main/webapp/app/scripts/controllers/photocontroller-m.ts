/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/// <reference path="../services/smarttableService.ts"/>
/// <reference path="../services/smarttableService.ts"/>
/// <reference path="../../../lib/mediacontrol.d.ts"/>
/*
'use strict';

angular.module('exApp')
  .controller('MycCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.vm = {foo: "foobar"};
  });
*/

angular.module('exApp');


/*
* define an Interface which forms the boundary between the HTML and the JS
*/
interface IPhotoCtrl {
  caminfo:string;
}

interface Navigator {
  getUserMedia(
    options: { video?: any; audio?: any; }, 
    success: (stream: any) => void, 
    error?: (error: string) => void
    ) : void;
  webkitGetUserMedia(
    options: { video?: any; audio?: any; }, 
    success: (stream: any) => void, 
    error?: (error: string) => void
    ) : void;
}

// interface    HTMLElement{
//         src: string;
//         play: string;
//       }

class PhotoCtrl implements IPhotoCtrl{

 translateService ;
 modelService ;
 location ;

 cameraId:string ="foo";

 caminfo = "c=";

 setupCamera:any;
 cfn:any;
 fingerprint="PhotoCtrl";
 bar:any="s";

	static $inject = ['$scope', '$rootScope', '$location', 'TranslateService', 'ModelService'];  // this matches ...
	constructor($scope, $rootScope, $location, translateService, modelService) { // ... this, for minification
		$scope.vm = this;  // hook vm into class properties
    this.translateService = translateService;
    this.modelService = modelService;
    this.location = $location;
    console.log(modelService.getNew()[1]);

    var p = this;

    // don't understand why this only works if I declare setupCamera on teh window object,but it works
    // window['setupCamera'] =  function() {
    window['setupCamera'] =  function() {
    // Grab elements, create settings, etc.
    alert("setting up ");
    alert("setting up "+this.cameraId);
    var canvas:any = document.getElementById("canvas");
    var context = canvas.getContext("2d");
    var video:any = document.getElementById("video");
    var videoObj = { "video": {optional: [{sourceId: this.cameraId}]}};
    var errBack = function(error) {
      console.log("Video capture error: ", error.code); 
    };
    var win:any = window;

    // Put video listeners into place
    if(navigator.getUserMedia) { // Standard
      navigator.getUserMedia({ "video": {optional: [{sourceId: this.cameraId}]}}, function(stream) {
        video.src = stream;
        video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
      navigator.webkitGetUserMedia({ "video": {optional: [{sourceId: this.cameraId}]}}, function(stream){
        video.src = win.webkitURL.createObjectURL(stream);
        video.play();
        }, errBack);
    }

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function() {
      context.drawImage(video, 0, 0, 640, 480);
      });
  }



    if (typeof MediaStreamTrack === 'undefined'){
      alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
      } else {
        try {
          MediaStreamTrack.getSources(this.gotSources);
          } catch (ex) {
            alert(ex);
          }
      }
    } // end of constructor

  gotSources(sourceInfos) {
    // alert(parent.ss);
    for (var i = 0; i != sourceInfos.length; ++i) {
      var sourceInfo = sourceInfos[i];
      try {
        alert(this.cameraId+" "+sourceInfo.kind+" id="+sourceInfo.id);
        } catch (ex) {
          alert(ex);
        }
        var option = document.createElement("option");
        option.value = sourceInfo.id;
        if (sourceInfo.kind === 'audio') {
          } else if (sourceInfo.kind === 'video') {
            this.caminfo+= sourceInfo.kind+sourceInfo.label;
            this.cameraId = sourceInfo.id;
            } else {
              console.log('Some other kind of source: ', sourceInfo);
            }
          }
    // alert("abou to set up "+parent.cameraId+ " "+window.ss);
        try {
            window['setupCamera']();
          } catch (ex) {
            alert(ex);
          }
        }
}