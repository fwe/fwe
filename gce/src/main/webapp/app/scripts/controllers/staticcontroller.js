/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/// <reference path="../services/driveService.ts"/>
// class to deal with translations for static pages
'use strict';
/*
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
//var app = angular.module('exApp', []);
//console.log(app);
angular.module('exApp');


var StaticCtrl = (function () {
    function StaticCtrl($scope, $rootScope, $window, $location, translateService) {
        /* declare all view model variables here */
        this.lang = "en";
        // all these service refs need to be under a refs object which I then exclude from $digest
        this.refs = {};
        $scope.vm = this; // hook vm into class properties
        this.refs['translateService'] = translateService;

        /* watch the location hash and set this.lang if it changes */
        var self = this;
        $scope.$watch(function () {
            return window.location.hash;
        }, function (n, o) {
            self.lang = window.location.hash.replace("/", "").replace("#", "");
            self.refs['translateService'].setLang(self.lang);
        }, true);
    }
    StaticCtrl.$inject = ['$scope', '$rootScope', '$window', '$location', 'TranslateService'];
    return StaticCtrl;
})();
//# sourceMappingURL=staticcontroller.js.map
