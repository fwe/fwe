/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelservice.ts"/>
/// <reference path="../services/driveservice.ts"/>
/// <reference path="../services/translateservice.ts"/>

/*
'use strict';

angular.module('exApp')
  .controller('MycCtrl', function ($scope) {
	$scope.awesomeThings = [
	  'HTML5 Boilerplate',
	  'AngularJS',
	  'Karma'
	];
	$scope.vm = {foo: "foobar"};
  });
*/

angular.module('exApp');




class MainCtrlAbstract {

	image: any;
	refs: any;
	lang="en";
	showCalendarSelect=false;

	focusItem: Model.Exitem=undefined;   // current edit/magnify focus

	// reference to jsp config object
	config: any;

	// smart table
	globalConfig: any;
	rowCollection: any;
	columnCollection: any;
	outboxColumnCollection: Array<any>;
	newColumnCollection: Array<any>;
	hiddenColumnCollection: Array<any>;
	queryColumnCollection: Array<any>;
	queryColumnCollectionEmp: Array<any>;
	approvedColumnCollection: Array<any>;
	paidColumnCollection: Array<any>;
	rejectedColumnCollection: Array<any>;
	deletedColumnCollection: Array<any>;

	recentEventsArray: Array<any>;

	// tag editing  
	tagsArray: Array<string>=[];
	addedTag: string;

	types: Array<any>=[];

	saveQuery() {
		console.log("sq a");
		this.refs.modelService.processActionsEach(this.refs.modelService.getQuery());
	}


	getStyle(evDate: any) {
		console.log(evDate+"=="+this.focusItem.dateI);
		if(evDate.indexOf(this.focusItem.dateI)>-1) {
			return "*";
		}
		return "color:red";
	}
	deleteItem(row: Model.Exitem,collection) {
		console.log(collection);
		this.refs.modelService.deleteItem(row,collection)
  }

	unDeleteItem(row: Model.Exitem,collection) {
		console.log(collection);
		this.refs.modelService.unDeleteItem(row,collection)
}
	sendOutbox() {
		console.log("so");
		// process the items into their new arrays
		this.refs.modelService.processActionsEach(this.refs.modelService.getOutbox());
	}
	saveNew() {
		console.log("sn");
		this.refs.modelService.processActionsEach(this.refs.modelService.getNew());
	}
	saveApproved() {
		console.log("sn");
		this.refs.modelService.processActionsEach(this.refs.modelService.getApproved());
	}
	langSwitch(_lang) {
		// console.log('lang eng');
		if(_lang) {
			this.refs.translateService.setLang(_lang);
		} else {
			this.refs.translateService.setLang(this.lang);
		}
	}

	radioclick(obj) {
		console.log(obj);
	}

}