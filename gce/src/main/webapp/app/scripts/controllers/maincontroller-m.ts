/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelservice.ts"/>
/// <reference path="../services/driveservice.ts"/>
/// <reference path="../services/translateservice.ts"/>

/*
'use strict';

angular.module('exApp')
  .controller('MycCtrl', function ($scope) {
	$scope.awesomeThings = [
	  'HTML5 Boilerplate',
	  'AngularJS',
	  'Karma'
	];
	$scope.vm = {foo: "foobar"};
  });
*/

angular.module('exApp');


/*
* define an Interface which forms the boundary between the HTML and the JS
*/
interface IMainmCtrl {
	lang: string; // the selected language (maybe use langSwitch(lang) instead)
	langSwitch: (lang: string) => void;
	showmenu: boolean;
	mobile: boolean;
	//  refs:{location:ng.ILocationService ;rootScope:ng.IRootScopeService;translateService:TService.TranslateService;driveService:DriveServiceModule.DriveService;storageService:TService.StorageService;modelService:DriveServiceModule.DriveService };

	// refs.translateService:{};  // link to translation service in case view wants it  
	focusItem: Model.Exitem;   // current edit/magnify focus
	tagsArray: Array<string>;
	addedTag: string;

	deleteItem: (row: Model.Exitem, coll: Array<any>) => void;
	unDeleteItem: (row: Model.Exitem, coll: Array<any>) => void;
	zoomin: (row: Model.Exitem) => void;
	zoomout: () => void;
	showCalendarSelect: boolean;

	isRowEditable: boolean;

	recentEventsArray: Array<any>;


	currentTab: string;
	currentTabTitle: string;

	sendOutbox: () => void;
	saveNew: () => void;
	saveQuery: () => void;
	saveApproved: () => void;
	newExitem: () => void;
	tabClicked: (tab: string, back: boolean) => void;

	types: Array<any>;

}


class MainmCtrl extends MainCtrlAbstract implements IMainmCtrl {
	fingerprint = 'MainmCtrl';

	/* declare all view model variables here */
	mobile = true;
	showmenu = false;
	currentTab = 'O';
	currentTabTitle = "Outbox";
	// all these service refs need to be under a refs object which I then exclude from $digest
	//  refs={location:ng.ILocationService , rootScope:ng.IRootScopeService,translateService:TService.TranslateService,driveService:DriveServiceModule.DriveService,storageService:TService.StorageService,modelService:DriveServiceModule.DriveService };
	//  refs={location:{}, rootScope:{},translateService:{},driveService:{},storageService:{},modelService:{} };
	//  refs:any;
	// translateService;
	// driveService;
	// modelService;
	// location;   // $location


	number: string; // sets the input type in the form
	isRowEditable: boolean;  // switches if zoom is <input> or {{}}


	static $inject = ['$scope', '$rootScope', '$window', '$location', 'TranslateService', 'ModelService', 'HttpDatasourceService', 'DriveService', 'StorageService'];  // this matches ...
	constructor($scope, $rootScope, $window, $location, translateService, modelService, datasourceService, driveService, storageService) { // ... this, for minification
		super();
		// drop a handle to ourselves so input/file dreictive in app.js can find us
		window['mainCtrl'] = this;

		//    this.refs={};

		$scope.vm = this;  // hook vm into class properties
		this.refs = { location: $location, rootScope: $rootScope, translateService: translateService, driveService: driveService, storageService: storageService, modelService: modelService };

		//    this.refs.location = $location;
		//    this.refs.rootScope = $rootScope;
		//    this.refs.translateService = translateService;
		//    this.refs.driveService = driveService;
		//    this.refs.storageService = storageService;
		//    this.refs.modelService = modelService;
		$rootScope.translateService = translateService;
		this.config = $window.config;
//		console.log(modelService.getNew()[1]);

		var parent = this;

		this.globalConfig = {
			cleanColumns: true,
			isPaginationEnabled: false,
			isGlobalSearchActivated: true,
			itemsByPage: 10,
			selectionMode: 'none',
		};
		this.refs.datasourceService = datasourceService;
//		this.recentEventsArray = datasourceService.fetchRecentCalendarEvents();


		this.number = "number";
		// this.number="text";

		this.currentTabTitle = translateService.getString('Outbox');
		/*
		* create a listener on location to deal with back being clicked ona detail view
		*/
		$rootScope.$watch(function() {

			return $location.hash();
		},
			function(newValue, oldValue) {
				if (newValue != oldValue) {
					console.log(oldValue + "-->" + newValue + '<');
					if (oldValue == "menu" && newValue == "") {
						console.log("clear " + parent.focusItem);
						parent.showmenu = false;
					}
					if (oldValue == "detail" && newValue == "") {
						console.log("clear " + parent.focusItem);
						parent.focusItem = undefined;
					}
				}
				else {
				}

			},
			true);
		/*
		* watch for a newly added tag
		*/
		$rootScope.$watch(function() {return parent.addedTag }, function(newValue, oldValue) {
			if (!newValue) {
				return;
			}
			console.log(oldValue + "-->" + newValue + '<');
			parent.tagsArray.push(newValue);
			parent.addedTag = undefined;
		});

		// set language from config object
		this.langSwitch('en');
            // todo, these need to be language dependent
            this.types = [{ type: 'L', typeDesc: this.refs.translateService.getString('tl') }, { type: 'F', typeDesc: this.refs.translateService.getString('tf') }, { type: 'K', typeDesc: this.refs.translateService.getString('tk') }]


}// end of constructor

	/**
	* called whenever the currency changes to update the fx rate
	THIS IS DUPLICATED IN BOTH CONTROLLERS. THIS SHOULD BE SOMEHOW PARAMETERISED ON A PER CUSTOMER BASIS
	*/
	currencyChanged() {
		var fxrate: number;
		if (this.focusItem.currency == window['config'].settings.baseio) {
			fxrate = parseFloat("1");
		} else {
			for (var i = 0; i < window['config'].fx.length; i++) {
				if (window['config'].fx[i].iso.toUpperCase() == this.focusItem.currency.toUpperCase()) {
					fxrate = parseFloat(window['config'].fx[i].rate);
					break;
				}
			}
		}
		this.focusItem.fx = fxrate;
		this.refs.modelService.calculateFx(this.focusItem);
	}



	zoomin(row: Model.Exitem) {
		// decide if this row is of an editable status
		if (row.stat == "O" || row.stat == "Q") {
			this.isRowEditable = true;
		} else {
			this.isRowEditable = false;
		}
		this.focusItem = row; // set the focus item which triggers a show on the detail fragment
		// populate the tags array
		try {
			this.tagsArray = [];
			if (this.focusItem.tags && this.focusItem.tags.length > 1) {
				this.tagsArray = this.focusItem.tags.split(",");
			}
		} catch (ex) { // tags might be undefined
			this.tagsArray = [];
		}

		this.refs.location.hash("detail");
	}

	zoomout() {
		// pack the tags and clear down the array
		this.focusItem.tags = this.tagsArray.join();
		this.tagsArray = [];

		/*
		* if this is an outbox item, save the array
		*/
		console.log("zoomout");
		if (this.focusItem.stat == "O") {
			this.refs.modelService.saveOutbox(this.focusItem);
		}
		this.focusItem = undefined; // set the focus item which triggers a show on the detail fragment
		// do a history back
		window.history.back();
	}





	/*
	* on click handlers ...
	*/
	newExitem() {
		var newItem = this.refs.modelService.newItem();
		this.focusItem = newItem;
		this.isRowEditable = true;
		// mobile nav
		this.showmenu = false;
		// window.history.back();
		var parent = this;

		/*
		* need to let the current thread complete, so Back clears the hash
		* then we can set the new 'detail' hash to enable Back to return to main disaply.
		* need the digest to tell Angular about it
		*/
		setTimeout(function() {
			parent.refs.location.hash("detail");
			parent.refs.rootScope.$digest();
			// window.location.hash="detail";
		}, 200);

	}
	/**
	* invokes a click event on the hidden input type=file field
	*/
	doFileClick() {
		document.getElementById("imgb").click();
	}


	// doImgBchange(evt) {
	//   console.log(evt);
	//    var files = evt.target.files;
	//   if(files.length > 0  && files[0].type.indexOf("image/") == 0) {
	//     document.getElementById("img")['src'] = URL.createObjectURL(files[0]);
	//   }
	// }

	/**
	* invoked when a file/camera is selected in new item input form
	* figures out the file details, then saves them in local storage for later
	* uploading
	*/
	setFileOld(element) {
		console.log(this);
		console.log(element);
		console.log(element.files[0]);
		document.getElementById("rimg")['src'] = URL.createObjectURL(element.files[0]);

		var title = this.focusItem.desc;
		if (title == undefined) {
			title = "";
		}
		if (title.length > 20) {
			title = title.substr(0, 19);
		}
		title = this.config.e + "-" + title;

		// this.refs.storageService.
		var parent = this;
		this.refs.driveService.uploadFile(title, element.files[0], function(file) {
			console.log("in callback with file...");
			console.log(file);
			parent.focusItem.rurl = file['webContentLink'].replace("&export=download", "");
			console.log(parent.focusItem.rurl);
			// TODO update the focusitem with the webview url
		});
		// $scope.$apply(function() {        
		//     $scope.theFile = element.files[0];
		// })
	}

	/**
	* invoked when a file/camera is selected in new item input form
	* figures out the file details, then saves them in local storage for later
	* uploading
	*/
	setFile(element) {
		console.log(this);
		console.log(element);
		var file = element.files[0];
		console.log(window['URL'].createObjectURL(file));


		document.getElementById("rimg")['src'] = URL.createObjectURL(file);
		console.log(URL.createObjectURL(file));



		// save the filename (which is also the storage key) in the focusItem
		this.refs.storageService.saveImageFile(file, this.focusItem);


		/* testing */
		var cb = function(obj) {
			console.log("fetched...");
			console.log(obj);
		}
  this.refs.storageService.fetchImageFile(file['name'], cb);


	}


	//   console.log(evt);
	//    var files = evt.target.files;
	//   if(files.length > 0  && files[0].type.indexOf("image/") == 0) {
	//     document.getElementById("img")['src'] = URL.createObjectURL(files[0]);
	//   }
	// }


	toggleNav() {
		console.log("ffff");
		if (!this.showmenu) {
			this.showmenu = true;
			this.refs.location.hash("menu");
		} else {
			window.history.back();
			this.showmenu = false;
		}
	}

	/**
	*  swap to the selected "tab"
	* if called from the dropdown, back will be true to do a Back . Otherwise if called from the sidemenu,no need
	*/
	tabClicked(tab: string, back: boolean) {
		console.log("punter wants " + tab);
		this.showmenu = false;
		if (back == true) {
			window.history.back();
		}
		this.currentTab = tab;
		switch (tab) {
			case 'O':
				this.currentTabTitle = this.refs.translateService.getString('Outbox');
				break;
			case 'N':
				this.currentTabTitle = this.refs.translateService.getString('Awaiting');
				break;
			case 'Q':
				this.currentTabTitle = this.refs.translateService.getString('Question');
				break;
			case 'A':
				this.currentTabTitle = this.refs.translateService.getString('Approved');
				break;
			case 'P':
				this.currentTabTitle = this.refs.translateService.getString('Paid');
				break;
			case 'R':
				this.currentTabTitle = this.refs.translateService.getString('Rejected');
				break;

		}

	}

}