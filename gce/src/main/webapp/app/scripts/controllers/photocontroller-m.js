/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/// <reference path="../services/smarttableService.ts"/>
/// <reference path="../services/smarttableService.ts"/>
/// <reference path="../../../lib/mediacontrol.d.ts"/>
/*
'use strict';
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
angular.module('exApp');


// interface    HTMLElement{
//         src: string;
//         play: string;
//       }
var PhotoCtrl = (function () {
    function PhotoCtrl($scope, $rootScope, $location, translateService, modelService) {
        this.cameraId = "foo";
        this.caminfo = "c=";
        this.fingerprint = "PhotoCtrl";
        this.bar = "s";
        $scope.vm = this; // hook vm into class properties
        this.translateService = translateService;
        this.modelService = modelService;
        this.location = $location;
        console.log(modelService.getNew()[1]);

        var p = this;

        // don't understand why this only works if I declare setupCamera on teh window object,but it works
        // window['setupCamera'] =  function() {
        window['setupCamera'] = function () {
            // Grab elements, create settings, etc.
            alert("setting up ");
            alert("setting up " + this.cameraId);
            var canvas = document.getElementById("canvas");
            var context = canvas.getContext("2d");
            var video = document.getElementById("video");
            var videoObj = { "video": { optional: [{ sourceId: this.cameraId }] } };
            var errBack = function (error) {
                console.log("Video capture error: ", error.code);
            };
            var win = window;

            // Put video listeners into place
            if (navigator.getUserMedia) {
                navigator.getUserMedia({ "video": { optional: [{ sourceId: this.cameraId }] } }, function (stream) {
                    video.src = stream;
                    video.play();
                }, errBack);
            } else if (navigator.webkitGetUserMedia) {
                navigator.webkitGetUserMedia({ "video": { optional: [{ sourceId: this.cameraId }] } }, function (stream) {
                    video.src = win.webkitURL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }

            // Trigger photo take
            document.getElementById("snap").addEventListener("click", function () {
                context.drawImage(video, 0, 0, 640, 480);
            });
        };

        if (typeof MediaStreamTrack === 'undefined') {
            alert('This browser does not support MediaStreamTrack.\n\nTry Chrome Canary.');
        } else {
            try  {
                MediaStreamTrack.getSources(this.gotSources);
            } catch (ex) {
                alert(ex);
            }
        }
    }
    PhotoCtrl.prototype.gotSources = function (sourceInfos) {
        for (var i = 0; i != sourceInfos.length; ++i) {
            var sourceInfo = sourceInfos[i];
            try  {
                alert(this.cameraId + " " + sourceInfo.kind + " id=" + sourceInfo.id);
            } catch (ex) {
                alert(ex);
            }
            var option = document.createElement("option");
            option.value = sourceInfo.id;
            if (sourceInfo.kind === 'audio') {
            } else if (sourceInfo.kind === 'video') {
                this.caminfo += sourceInfo.kind + sourceInfo.label;
                this.cameraId = sourceInfo.id;
            } else {
                console.log('Some other kind of source: ', sourceInfo);
            }
        }

        try  {
            window['setupCamera']();
        } catch (ex) {
            alert(ex);
        }
    };
    PhotoCtrl.$inject = ['$scope', '$rootScope', '$location', 'TranslateService', 'ModelService'];
    return PhotoCtrl;
})();
//# sourceMappingURL=photocontroller-m.js.map
