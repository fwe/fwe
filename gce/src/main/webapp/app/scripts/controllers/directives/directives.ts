

window['app'].directive('uppercase', function() {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (!inputValue) {
            inputValue="";
          }
           var capitalized = inputValue.toUpperCase();
           if(capitalized !== inputValue) {
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
            }         
            return capitalized;
         }
         modelCtrl.$parsers.push(capitalize);
         capitalize(scope[attrs.ngModel]);  // capitalize initial value
     }
   };
});


window['app'].directive('fileChange', [
    function() {
        return {
            link: function(scope, element, attrs) {
                element[0].onchange = function() {
                  console.log(scope[attrs]);
                  scope[attrs['fileChange']](element[0])

                }
            }
            
        }
    }
])
