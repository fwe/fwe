/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/*
'use strict';
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
angular.module('exApp');


var MaineCtrl = (function () {
    function MaineCtrl($scope, translateService, modelService, smarttableService) {
        /* declare all view model variables here */
        this.foo4 = "foobar 4444 a";
        this.lang = "en";
        this.focusItem = undefined;
        $scope.vm = this; // hook vm into class properties
        this.translateService = translateService;
        this.modelService = modelService;
        console.log(modelService.getNew()[1]);

        this.globalConfig = {
            cleanColumns: true,
            isPaginationEnabled: false,
            isGlobalSearchActivated: true,
            itemsByPage: 10,
            selectionMode: 'none'
        };

        this.newColumnCollection = smarttableService.newColumnCollection;
        this.queryColumnCollection = smarttableService.queryColumnCollection;
        this.approvedColumnCollection = smarttableService.approvedColumnCollection;
        this.paidColumnCollection = smarttableService.paidColumnCollection;
        this.rejectedColumnCollection = smarttableService.rejectedColumnCollection;
        // TODO here I need to adjust the displayed columns according to the status, eg. Apprioved should have Pay in place of Approve
    }
    MaineCtrl.prototype.zoomin = function (row) {
        console.log(row);

        // decide if this row is of an editable status
        if (row.stat == "N" || row.stat == "Q" || row.stat == "A") {
            this.isRowEditable = true;
        } else {
            this.isRowEditable = false;
        }
        this.focusItem = row;
    };

    MaineCtrl.prototype.saveNew = function () {
        console.log("sn");
        this.modelService.processActionsEach(this.modelService.getNew());
    };
    MaineCtrl.prototype.saveQuery = function () {
        console.log("sn");
        this.modelService.processActionsEach(this.modelService.getQuery());
    };
    MaineCtrl.prototype.saveApproved = function () {
        console.log("sn");
        this.modelService.processActionsEach(this.modelService.getApproved());
    };

    MaineCtrl.prototype.langSwitch = function (_lang) {
        // console.log('lang eng');
        if (_lang) {
            this.translateService.setLang(_lang);
        } else {
            this.translateService.setLang(this.lang);
        }
    };

    MaineCtrl.prototype.radioclick = function (obj) {
        console.log(obj);
    };

    MaineCtrl.prototype.showHiddenColumns = function () {
        this.newColumnCollection.push(this.hiddenColumnCollection);
    };
    MaineCtrl.prototype.hideOneColumn = function () {
        this.newColumnCollection.splice(0, 1);
    };
    MaineCtrl.$inject = ['$scope', 'TranslateService', 'ModelService', 'SmarttableService'];
    return MaineCtrl;
})();
//# sourceMappingURL=maineontroller.js.map
