/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/// <reference path="../services/driveService.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/**
* Controller for PC manager and emp ..
*/
/*
'use strict';
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
angular.module('exApp');


var MainCtrl = (function (_super) {
    __extends(MainCtrl, _super);
    function MainCtrl($scope, $rootScope, $window, $location, translateService, modelService, smarttableService, datasourceService, driveService, storageService) {
        var _this = this;
        _super.call(this);
        this.mobile = false;
        this.isItemChanged = false;
        this.buttonLabel = "Back";
        this.isManager = false;
        this.signup = false;

        // drop a handle to ourselves so input/file dreictive in app.js can find us
        window['mainCtrl'] = this;
        window['mainCtrlScope'] = $scope;

        this.refs = {};

        $scope.vm = this; // hook vm into class properties
        this.refs.location = $location;
        this.refs.translateService = translateService;
        this.refs.driveService = driveService;
        this.refs.storageService = storageService;
        this.refs.modelService = modelService;
        $rootScope.translateService = translateService;
        this.config = $window.config;
        console.log(modelService.getNew()[1]);

        var parent = this;

        this.globalConfig = {
            cleanColumns: true,
            isPaginationEnabled: false,
            isGlobalSearchActivated: true,
            itemsByPage: 10,
            selectionMode: 'none'
        };

        this.recentEventsArray = datasourceService.fetchRecentCalendarEvents();

        this.outboxColumnCollection = smarttableService.outboxColumnCollection;
        this.newColumnCollection = smarttableService.newColumnCollection;
        this.queryColumnCollection = smarttableService.queryColumnCollection;
        this.approvedColumnCollection = smarttableService.approvedColumnCollection;
        this.paidColumnCollection = smarttableService.paidColumnCollection;
        this.rejectedColumnCollection = smarttableService.rejectedColumnCollection;
        this.deletedColumnCollection = smarttableService.deletedColumnCollection;

        // if URL includes 'manager' set manager,
        if ($location.absUrl().toLowerCase().indexOf("manager") > -1) {
            this.isManager = true;
            if (document.location.hash && document.location.hash.indexOf("signup") > -1) {
                this.signup = true;
            }
        } else {
            this.newColumnCollection = this.loseEmail(this.loseRadioColumns(this.newColumnCollection));
            this.queryColumnCollection = this.loseEmail(this.loseRadioColumns(this.queryColumnCollection));
            this.approvedColumnCollection = this.loseEmail(this.loseRadioColumns(this.approvedColumnCollection));
            this.paidColumnCollection = this.loseEmail(this.paidColumnCollection);
            this.rejectedColumnCollection = this.loseEmail(this.rejectedColumnCollection);
        }

        /*
        * create a listener on location to deal with back being clicked ona detail view
        */
        $rootScope.$watch(function () {
            return $location.hash();
        }, function (newValue, oldValue) {
            if (newValue != oldValue) {
                console.log(oldValue + "-->" + newValue + '<');
                if (oldValue == "detail" && newValue == "") {
                    console.log("clear " + parent.focusItem);
                    parent.focusItem = undefined;
                }
            } else {
            }
        }, true);
        var parent = this;

        /*
        * watch for a newly added tag
        */
        $rootScope.$watch(function () {
            return parent.addedTag;
        }, function (newValue, oldValue) {
            if (!newValue) {
                return;
            }
            console.log(oldValue + "-->" + newValue + '<');
            parent.tagsArray.push(newValue);
            parent.addedTag = undefined;
        });

        // nb how this watch is constructed. Took a lot of fiddling to achieve it
        $scope.$watch("vm.focusItem", function (newValue, oldValue) {
            if (!oldValue || !newValue) {
                return;
            }
            console.log(oldValue);
            _this.itemFieldOnchange();
        }, true);

        // set language from config object
        this.lang = window.location.hash.replace("/", "").replace("#", "");

        //    if (window['config'].settings.defaultLang != undefined && window['config'].settings.defaultLang.length > 1) {
        //        this.lang = window['config'].settings.defaultLang;
        //    }
        this.langSwitch(this.lang);

        // todo, these need to be language dependent
        this.types = [{ type: 'L', typeDesc: this.refs.translateService.getString('tl') }, { type: 'F', typeDesc: this.refs.translateService.getString('tf') }, { type: 'K', typeDesc: this.refs.translateService.getString('tk') }];
    }
    MainCtrl.prototype.getPaidDates = function () {
        return this.refs.modelService.getPaidDates();
    };

    /**
    * called whenever the currency changes to update the fx rate
    THIS IS DUPLICATED IN BOTH CONTROLLERS. THIS SHOULD BE SOMEHOW PARAMETERISED ON A PER CUSTOMER BASIS
    */
    MainCtrl.prototype.currencyChanged = function () {
        var fxrate;
        if (this.focusItem.currency == window['config'].settings['baseio']) {
            fxrate = parseFloat("1");
        } else {
            for (var i = 0; i < window['config'].fx.length; i++) {
                if (window['config'].fx[i].iso.toUpperCase() == this.focusItem.currency.toUpperCase()) {
                    fxrate = parseFloat("" + window['config'].fx[i].rate);
                    break;
                }
            }
        }
        this.focusItem.fx = fxrate;
        this.refs.modelService.calculateFx(this.focusItem);
    };

    /**
    * called whenever the miles or mileqage rate changes to update the fx rate
    THIS IS DUPLICATED IN BOTH CONTROLLERS. THIS SHOULD BE SOMEHOW PARAMETERISED ON A PER CUSTOMER BASIS
    */
    /**
    * scan the provided array and lose any columns which have a cellTemplateUrl: 'views/ radio A.html'
    */
    MainCtrl.prototype.loseRadioColumns = function (arry) {
        var temp = [];
        for (var i = 0; i < arry.length; i++) {
            if (arry[i]['cellTemplateUrl'] && arry[i]['cellTemplateUrl'].indexOf('radio') > -1) {
                continue;
            }
            temp.push(arry[i]); // else copy it
        }
        return temp;
    };

    /**
    * scan the provided array and lose the email column
    */
    MainCtrl.prototype.loseEmail = function (arry) {
        var temp = [];
        for (var i = 0; i < arry.length; i++) {
            if (arry[i]['map'] && arry[i]['map'] == 'email') {
                continue;
            }
            temp.push(arry[i]); // else copy it
        }
        return temp;
    };

    /**
    * onchange handler to mark an item as edited
    */
    MainCtrl.prototype.itemFieldOnchange = function () {
        this.isItemChanged = true;
        this.buttonLabel = "save";
    };

    MainCtrl.prototype.zoomin = function (row) {
        this.isItemChanged = false;
        this.buttonLabel = "Back";

        // decide if this row is of an editable status
        if (row.stat == "O" || row.stat == "N" || row.stat == "Q" || row.stat == "A") {
            this.isRowEditable = true;
        } else {
            this.isRowEditable = false;
        }
        this.focusItem = row; // set the focus item which triggers a show on the detail fragment

        // populate the tags array
        this.tagsArray = [];
        if (this.focusItem.tags && this.focusItem.tags.length > 1) {
            this.tagsArray = this.focusItem.tags.split(",");
        }

        this.refs.location['hash']("detail");
    };

    MainCtrl.prototype.zoomout = function () {
        // pack the tags and clear down the array
        this.focusItem.tags = this.tagsArray.join();
        this.tagsArray = [];
        if (this.isItemChanged == true) {
            console.log("save updated item.");
            this.refs.modelService.putUpdatedItem(this.focusItem);

            /*
            * if this is an outbox item, save the array
            */
            if (this.focusItem.stat == "O") {
                this.refs.modelService.saveOutbox(this.focusItem);
            }
        }

        //  console.log(11111);
        //  this.isItemChanged = false;
        //  console.log(this.buttonLabel);
        //  this.buttonLabel = "Back";
        //  console.log(this.buttonLabel);
        this.focusItem = undefined; // set the focus item which triggers a show on the detail fragment
    };

    /*
    * on click handlers ...
    */
    MainCtrl.prototype.newExitem = function () {
        var newItem = this.refs.modelService.newItem();
        this.focusItem = newItem;
        this.isRowEditable = true;
        this.buttonLabel = "Back";
        this.isItemChanged = false;
        try  {
            document.getElementById('imgb')['value'] = '';
        } catch (ex) {
        }
        ;
    };

    // doImgBchange(evt) {
    //   console.log(evt);
    //    var files = evt.target.files;
    //   if(files.length > 0  && files[0].type.indexOf("image/") == 0) {
    //     document.getElementById("img")['src'] = URL.createObjectURL(files[0]);
    //   }
    // }
    /**
    * invoked when a file/camera is selected in new item input form
    * figures out the file details, then saves them in local storage for later
    * uploading
    */
    MainCtrl.prototype.setFileOld = function (element) {
        console.log(this);
        console.log(element);
        console.log(element.files[0]);
        var file = element.files[0];
        document.getElementById("rimg")['src'] = URL.createObjectURL(element.files[0]);
        console.log(URL.createObjectURL(file));

        var title = this.focusItem.desc;
        if (title == undefined) {
            title = "";
        }
        if (title.length > 20) {
            title = title.substr(0, 19);
        }
        title = this.config.e + "-" + title;

        // this.refs.storageService.
        var parent = this;
        this.refs.driveService.uploadFile(title, element.files[0], function (file) {
            console.log("in callback with file...");
            console.log(file);
            parent.focusItem.rurl = file['webContentLink'].replace("&export=download", "");
            console.log(parent.focusItem.rurl);
            // TODO update the focusitem with the webview url
        });
        // $scope.$apply(function() {
        //     $scope.theFile = element.files[0];
        // })
    };

    /**
    * invoked when a file/camera is selected in new item input form
    * figures out the file details, then saves them in local storage for later
    * uploading
    */
    MainCtrl.prototype.setFile = function (element) {
        console.log(this);
        console.log(element);
        var file = element.files[0];
        console.log(window['URL'].createObjectURL(file));

        document.getElementById("rimg")['src'] = URL.createObjectURL(file);

        // save the filename (which is also the storage key) in the focusItem
        this.focusItem.rurl = file.name;
        console.log(JSON.stringify(file));
        this.refs.storageService.saveImageFile(file, this.focusItem);

        /* testing */
        var cb = function (obj) {
            console.log("fetched...");
            console.log(obj);
        };
        this.refs.storageService.fetchImageFile(file['name'], cb);
    };

    //   console.log(evt);
    //    var files = evt.target.files;
    //   if(files.length > 0  && files[0].type.indexOf("image/") == 0) {
    //     document.getElementById("img")['src'] = URL.createObjectURL(files[0]);
    //   }
    // }
    MainCtrl.prototype.showHiddenColumns = function () {
        this.newColumnCollection.push(this.hiddenColumnCollection);
    };
    MainCtrl.prototype.hideOneColumn = function () {
        this.newColumnCollection.splice(0, 1);
    };
    MainCtrl.$inject = ['$scope', '$rootScope', '$window', '$location', 'TranslateService', 'ModelService', 'SmarttableService', 'HttpDatasourceService', 'DriveService', 'StorageService'];
    return MainCtrl;
})(MainCtrlAbstract);
//# sourceMappingURL=maincontroller.js.map
