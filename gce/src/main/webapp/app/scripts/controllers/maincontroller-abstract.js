/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelservice.ts"/>
/// <reference path="../services/driveservice.ts"/>
/// <reference path="../services/translateservice.ts"/>
/*
'use strict';
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
angular.module('exApp');

var MainCtrlAbstract = (function () {
    function MainCtrlAbstract() {
        this.lang = "en";
        this.showCalendarSelect = false;
        this.focusItem = undefined;
        // tag editing
        this.tagsArray = [];
        this.types = [];
    }
    MainCtrlAbstract.prototype.saveQuery = function () {
        console.log("sq a");
        this.refs.modelService.processActionsEach(this.refs.modelService.getQuery());
    };

    MainCtrlAbstract.prototype.getStyle = function (evDate) {
        console.log(evDate + "==" + this.focusItem.dateI);
        if (evDate.indexOf(this.focusItem.dateI) > -1) {
            return "*";
        }
        return "color:red";
    };
    MainCtrlAbstract.prototype.deleteItem = function (row, collection) {
        console.log(collection);
        this.refs.modelService.deleteItem(row, collection);
    };

    MainCtrlAbstract.prototype.unDeleteItem = function (row, collection) {
        console.log(collection);
        this.refs.modelService.unDeleteItem(row, collection);
    };
    MainCtrlAbstract.prototype.sendOutbox = function () {
        console.log("so");

        // process the items into their new arrays
        this.refs.modelService.processActionsEach(this.refs.modelService.getOutbox());
    };
    MainCtrlAbstract.prototype.saveNew = function () {
        console.log("sn");
        this.refs.modelService.processActionsEach(this.refs.modelService.getNew());
    };
    MainCtrlAbstract.prototype.saveApproved = function () {
        console.log("sn");
        this.refs.modelService.processActionsEach(this.refs.modelService.getApproved());
    };
    MainCtrlAbstract.prototype.langSwitch = function (_lang) {
        // console.log('lang eng');
        if (_lang) {
            this.refs.translateService.setLang(_lang);
        } else {
            this.refs.translateService.setLang(this.lang);
        }
    };

    MainCtrlAbstract.prototype.radioclick = function (obj) {
        console.log(obj);
    };
    return MainCtrlAbstract;
})();
//# sourceMappingURL=maincontroller-abstract.js.map
