/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>
/*
'use strict';
angular.module('exApp')
.controller('MycCtrl', function ($scope) {
$scope.awesomeThings = [
'HTML5 Boilerplate',
'AngularJS',
'Karma'
];
$scope.vm = {foo: "foobar"};
});
*/
angular.module('exApp');


var SettingsCtrl = (function () {
    function SettingsCtrl($scope, translateService, modelService) {
        /* declare all view model variables here */
        this.fingerprint = "SettingsCtrl";
        this.pendingChanges = "disabled";
        this.settingsChanged = -2;
        this.empsChanged = -1;
        this.nomsChanged = -1;
        this.fxratesChanged = -1;
        $scope.vm = this; // hook vm into class properties
        this.translateService = translateService;
        this.modelService = modelService;
        this.settings = modelService.getSettings();
        this.empsChanged = -1;
        this.parentVm = $scope.$parent.vm;
        console.log(this.empsChanged);
        $scope.$watch('vm.settings', function (newValue, oldValue) {
            console.log("settings changed");
            if (++$scope.vm.settingsChanged > 0) {
                $scope.vm.pendingChanges = "enabled";
            }
        }, true);

        $scope.$on("updateDataRow", function (event, args) {
            console.log("emitted ...");
            console.log(event);
            console.log(args);
            console.log(args.item);
            console.log(args.item.$$hashKey);
            $scope.vm.rowChanged(args);
        });

        // scope.$emit('updateDataRow', {item: scope.displayedCollection[index]});
        this.settingsConfig = {
            isPaginationEnabled: false,
            isGlobalSearchActivated: true,
            itemsByPage: 10,
            selectionMode: 'none'
        };

        this.resetChangesContainer();

        this.empColumnCollection = [
            { label: 'Name', map: 'name', isEditable: true },
            { label: 'Bank details', map: 'bank', isEditable: true },
            { label: 'Is a manager', map: 'manager', isEditable: true, cellTemplateUrl: 'views/checkManager.html' },
            { label: 'Language', map: 'lang', isEditable: true },
            { label: 'Email', map: 'email', isEditable: true, headerClass: 'wid5' }
        ];

        this.nomsColumnCollection = [
            { label: 'Mnemonic code', map: 'mnemonic', isEditable: true },
            { label: 'Description', map: 'desc', isEditable: true }
        ];
        this.fxratesColumnCollection = [
            { label: 'Currency code', map: 'iso', isEditable: true },
            { label: 'Rate per one local currency', map: 'rate', isEditable: true }
        ];
        // TODO here I need to adjust the displayed columns according to the status, eg. Apprioved should have Pay in place of Approve
    }
    SettingsCtrl.prototype.newEmp = function () {
        var o = new Model.Emp();
        o.email = "email";
        o.name = "name";
        o.bank = "bank details";
        o.manager = false;
        o.lang = this.modelService.getSettings().defaultLang;
        this.modelService.getEmp().push(o);
    };
    SettingsCtrl.prototype.newRate = function () {
        var o = new Model.Fx();
        o.iso = "currency code";
        o.rate = 1;
        this.modelService.getFxrates().push(o);
    };
    SettingsCtrl.prototype.newNom = function () {
        var o = new Model.Nominal();
        o.mnemonic = "mnemonic";
        o.desc = "description";
        this.modelService.getNoms().push(o);
    };

    /**
    * check if settings have changed,if so add to changesContainer
    * pass changesContainer to model
    * clear down changes counts and changesContainer
    * NB model cloned the object so it's safe to clear down here
    */
    SettingsCtrl.prototype.saveChanges = function () {
        if (this.settingsChanged > 0) {
            this.changesContainer["settings"] = this.settings;
            this.settingsChanged = 0;
        }
        this.modelService.saveSettingsChanges(this.changesContainer);
        this.resetChangesContainer();

        try  {
            this.parentVm['signup'] = false;
        } catch (ex) {
        }
        ;
    };

    /**
    * clears down the changedContainer. Called on startup and after a server save
    */
    SettingsCtrl.prototype.resetChangesContainer = function () {
        this.changesContainer = {
            settings: undefined,
            emp: [],
            nom: [],
            fx: []
        };
        this.pendingChanges = 'disabled';
    };

    /**
    * called by the $on when smart table emits a dataRowChanged
    * need to figure out what kind of row it was, then prepend to a changes array
    * @param row the updated row object
    */
    SettingsCtrl.prototype.rowChanged = function (row) {
        var theActualRow;

        if (row.item) {
            theActualRow = row.item;
        } else {
            theActualRow = row;
        }
        console.log(theActualRow);
        if (theActualRow.email) {
            this.changesContainer['emp'].unshift(theActualRow); // nb ynshift instead of push to prepend the new item
            this.empsChanged++; // this is to help deduplicate when saving
        }
        if (theActualRow.iso) {
            this.changesContainer['fx'].unshift(theActualRow);
            this.fxratesChanged++;
        }
        if (theActualRow.mnemonic) {
            this.changesContainer['nom'].unshift(theActualRow);
            this.empsChanged++;
        }
        console.log(this.changesContainer);
        this.pendingChanges = "enabled";
    };
    SettingsCtrl.$inject = ['$scope', 'TranslateService', 'ModelService'];
    return SettingsCtrl;
})();
//# sourceMappingURL=settingscontroller.js.map
