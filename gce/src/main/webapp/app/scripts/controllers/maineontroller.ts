/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>

/*
'use strict';

angular.module('exApp')
  .controller('MycCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.vm = {foo: "foobar"};
  });
*/

angular.module('exApp');


/*
* define an Interface which forms the boundary between the HTML and the JS
*/
interface IMaineCtrl {
  foo4 :string;  // foo3 is type string dummy
  lang:string; // the selected language (maybe use langSwitch(lang) instead)
  langSwitch:(lang:string)=>void;
  showHiddenColumns:()=>void;
  hideOneColumn:()=>void;
  focusItem:Model.Exitem;   // current edit/magnify focus
  zoomin:(row:Model.Exitem)=>void;
  // smart table views
  newColumnCollection: Array<any>;
  queryColumnCollection: Array<any>;
  approvedColumnCollection: Array<any>;
  paidColumnCollection: Array<any>;
  rejectedColumnCollection: Array<any>;

  isRowEditable:boolean;

  saveNew:()=>void;
  saveQuery:()=>void;
  saveApproved:()=>void;

}


class MaineCtrl implements IMaineCtrl{

  /* declare all view model variables here */
  
  foo4 :string = "foobar 4444 a";
  lang = "en";
  translateService;
  modelService;

  focusItem:Model.Exitem = undefined;   // current edit/magnify focus



// smart table
  rowCollection;
  columnCollection;
  newColumnCollection: Array<any>;
  hiddenColumnCollection:Array<any> ;
  queryColumnCollection: Array<any>;
  approvedColumnCollection: Array<any>;
  paidColumnCollection: Array<any>;
  rejectedColumnCollection: Array<any>;

  isRowEditable:boolean;  // switches if zoom is <input> or {{}}


  globalConfig;


	static $inject = ['$scope', 'TranslateService', 'ModelService', 'SmarttableService'];  // this matches ...
	constructor($scope, translateService, modelService, smarttableService) { // ... this, for minification
		$scope.vm = this;  // hook vm into class properties
    this.translateService = translateService;
    this.modelService = modelService;
    console.log(modelService.getNew()[1]);



    this.globalConfig = {
      cleanColumns: true,
      isPaginationEnabled: false,
      isGlobalSearchActivated: true,
      itemsByPage: 10,
      selectionMode: 'none',
    };



    this.newColumnCollection = smarttableService.newColumnCollection;
    this.queryColumnCollection = smarttableService.queryColumnCollection;
    this.approvedColumnCollection = smarttableService.approvedColumnCollection;
    this.paidColumnCollection = smarttableService.paidColumnCollection;
    this.rejectedColumnCollection = smarttableService.rejectedColumnCollection;




// TODO here I need to adjust the displayed columns according to the status, eg. Apprioved should have Pay in place of Approve

}
  zoomin(row:Model.Exitem) {
    console.log(row);
    // decide if this row is of an editable status
    if (row.stat == "N" || row.stat == "Q" || row.stat == "A") {
      this.isRowEditable = true; 
    } else {
      this.isRowEditable = false; 
    }
    this.focusItem = row;
  }


  saveNew() {
    console.log("sn");
    this.modelService.processActionsEach(this.modelService.getNew());
  }
  saveQuery() {
    console.log("sn");
    this.modelService.processActionsEach(this.modelService.getQuery());
  }
  saveApproved() {
    console.log("sn");
    this.modelService.processActionsEach(this.modelService.getApproved());
  }


  langSwitch(_lang) {
    // console.log('lang eng');
    if (_lang) {
      this.translateService.setLang(_lang);
    } else {
      this.translateService.setLang(this.lang);
    }
  }

  radioclick(obj) {
    console.log(obj);
  }

  showHiddenColumns() {
    this.newColumnCollection.push(this.hiddenColumnCollection);
  }
  hideOneColumn() {
    this.newColumnCollection.splice(0,1);
  }
}