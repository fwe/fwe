/// <reference path="../../../lib/angular.d.ts" />
/// <reference path="../common/tools.ts"/>
/// <reference path="../services/modelService.ts"/>

/*
'use strict';

angular.module('exApp')
  .controller('MycCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.vm = {foo: "foobar"};
  });
*/

angular.module('exApp');


/*
* define an Interface which forms the boundary between the HTML and the JS
*/
interface ISettingsCtrl {
  settings: {};
  saveChanges:()=>void;
  newEmp:()=>void;
  newRate:()=>void;
  newNom:()=>void;
  rowChanged:(row:{})=>void;
  translateService;
  pendingChanges:string;
}


class SettingsCtrl implements ISettingsCtrl{

  /* declare all view model variables here */

  fingerprint="SettingsCtrl";
  settings: Model.Settings;  

// TODO. storing the service references here places them in the scope, which means digest will scan them
  translateService;
  modelService:Model.IModelService;

  pendingChanges = "disabled";

  settingsChanged:number = -2; // tis is a kludge to allow the watch changes during init to be absorbed
  empsChanged:number = -1;
  nomsChanged:number = -1;
  fxratesChanged:number = -1;
// smart table
  rowCollection;
  columnCollection;
  empColumnCollection: Array<Model.Emp>;
  nomsColumnCollection: Array<Model.Nominal>;
  fxratesColumnCollection: Array<Model.Fx>;
  settingsConfig;
  parentVm;

  /**
  *holds settings, emps etc of any changed rows
  */ 
  changesContainer:{};


	static $inject = ['$scope', 'TranslateService', 'ModelService'];  // this matches ...
	constructor($scope, translateService, modelService) { // ... this, for minification
		$scope.vm = this;  // hook vm into class properties
    this.translateService = translateService;
    this.modelService = modelService;
    this.settings = modelService.getSettings();
    this.empsChanged = -1;
	this.parentVm = $scope.$parent.vm;
    console.log(this.empsChanged);
    $scope.$watch('vm.settings', function(newValue, oldValue) { 
          console.log("settings changed");  
          if (++$scope.vm.settingsChanged > 0) { 
            $scope.vm.pendingChanges = "enabled"
          }
        }, true);

    $scope.$on("updateDataRow", function (event, args) {
      console.log("emitted ...");
      console.log(event);
      console.log(args);
      console.log(args.item);
      console.log(args.item.$$hashKey);
      $scope.vm.rowChanged(args);
      });
    // scope.$emit('updateDataRow', {item: scope.displayedCollection[index]});

    this.settingsConfig = {
      isPaginationEnabled: false,
      isGlobalSearchActivated: true,
      itemsByPage: 10,
      selectionMode: 'none',
    };

    this.resetChangesContainer();


    this.empColumnCollection = <Array<Model.Emp>>[
      {label: 'Name', map: 'name', isEditable: true},
      {label: 'Bank details', map: 'bank', isEditable: true},
      {label: 'Is a manager', map: 'manager', isEditable: true, cellTemplateUrl: 'views/checkManager.html'},
      {label: 'Language', map: 'lang', isEditable: true},
      {label: 'Email', map: 'email', isEditable: true, headerClass: 'wid5'},
    ];


    this.nomsColumnCollection = <Array<Model.Nominal>>[
      {label: 'Mnemonic code', map: 'mnemonic', isEditable: true},
      {label: 'Description', map: 'desc', isEditable: true},
    ];
    this.fxratesColumnCollection = <Array<Model.Fx>>[
      {label: 'Currency code', map: 'iso', isEditable: true},
      {label: 'Rate per one local currency', map: 'rate', isEditable: true},
    ];


// TODO here I need to adjust the displayed columns according to the status, eg. Apprioved should have Pay in place of Approve

  }

  newEmp () {
    var o = new  Model.Emp();
    o.email = "email";
    o.name = "name";
    o.bank = "bank details";
    o.manager = false;
    o.lang = this.modelService.getSettings().defaultLang;
    this.modelService.getEmp().push(o);
  }
  newRate() {
    var o = new  Model.Fx();
    o.iso = "currency code";
    o.rate = 1;
    this.modelService.getFxrates().push(o);
  }
  newNom() {
    var o = new  Model.Nominal();
    o.mnemonic = "mnemonic";
    o.desc = "description";
    this.modelService.getNoms().push(o);
  }
  /**
  * check if settings have changed,if so add to changesContainer
  * pass changesContainer to model
  * clear down changes counts and changesContainer
  * NB model cloned the object so it's safe to clear down here
  */
  saveChanges() {
    if (this.settingsChanged > 0) {
      this.changesContainer["settings"] = this.settings;
      this.settingsChanged = 0;
    }
    this.modelService.saveSettingsChanges(this.changesContainer);
    this.resetChangesContainer();
	  
	  //tell parent that settings have been saved
	try {
		this.parentVm['signup'] = false;
	} catch (ex) {};
  }


  /**
  * clears down the changedContainer. Called on startup and after a server save
  */
  resetChangesContainer() {
    this.changesContainer={
      settings:undefined,
      emp:[],
      nom:[],
      fx:[]
    }
    this.pendingChanges = 'disabled';
  }

  /**
  * called by the $on when smart table emits a dataRowChanged
  * need to figure out what kind of row it was, then prepend to a changes array
  * @param row the updated row object
  */

  rowChanged(row) {
    var theActualRow;

    if (row.item) {
      theActualRow = row.item;
    } else {
      theActualRow = row;
    }
    console.log(theActualRow);
    if (theActualRow.email) {
      this.changesContainer['emp'].unshift(theActualRow); // nb ynshift instead of push to prepend the new item
      this.empsChanged++;                                 // this is to help deduplicate when saving
    }
    if (theActualRow.iso) {
      this.changesContainer['fx'].unshift(theActualRow);
      this.fxratesChanged++;
    }
    if (theActualRow.mnemonic) {
      this.changesContainer['nom'].unshift(theActualRow);
      this.empsChanged++;
    }
    console.log(this.changesContainer);
    this.pendingChanges = "enabled";

  }

}