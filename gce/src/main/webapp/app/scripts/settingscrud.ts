/**
 * (c) Roy Smith 2014
 * 
 * Drive CRUD for AngularJS
 * 
 * 
 */

/// <reference path="services/driveResourceService.ts"/>
/// <reference path="services/fileReaderService.ts"/>
/// <reference path="../../lib/angular.d.ts"/>
/// <reference path="../../lib/gapi.d.ts"/>
/// <reference path="../../lib/angular-resource.d.ts" />


declare var angular: ng.IAngularStatic;


interface ISettingscrudCtrl {
	dorigin:string;
	haveToken: boolean;
	driveActive: boolean;
	driveError: boolean;
	drivescope: string;
	clientId: string;
	q: string;
	maxResults: string;
//	parentsListToJson: (list?: string) => void;
//	parentsJsonToList: () => void;
	parentsList: string;
	doList: () => void;
	doInsert: () => void;
	doUpdate: () => void;
	doDelete: () => void;
	listResults: Array<DriveResourceServiceModule.IDriveFile>;
	focussedItem: DriveResourceServiceModule.IDriveFile;
}

class SettingscrudCtrl implements ISettingscrudCtrl {
	dorigin = <string> document.location['origin'];
	haveToken: boolean;
	driveActive: boolean;
	driveError: boolean;
	parentsList: string;
	drivescope = "email https://www.googleapis.com/auth/drive.file";
//	clientId = '700995682262-2kk81vdcu8j83j0ahjmk84u1drcbg5di.apps.googleusercontent.com'; // drivecrud
	clientId = '822097196702-jmapkqnikm7ql5mp4619748f33j7ev4l.apps.googleusercontent.com'; // fwe
	focussedItem: Model.Settings;
	listResults: Array<DriveResourceServiceModule.IDriveFile> = [];
	refs: { scope: ng.IScope; exResourceService: ExResourceServiceModule.ExResourceService; fileReaderService: FileReaderServiceModule.FileReaderService };
	//	refs:{scope:ng.IScope; driveResourceService: DriveResourceServiceModule.DriveResourceService};
	lang = "lang";
	q = "title contains 'qz' and trashed=false";
	maxResults = "100";

	static $inject = ['$scope', 'FileReaderService', 'ExResourceService'];
	constructor($scope, FileReaderService, ExResourceService) {
		//	static $inject = ['$scope', 'DriveResourceService'];
		//	constructor($scope, DriveResourceService) {
		//	static $inject = ['$scope', 'FileReaderService'];
		//	constructor($scope, FileReaderService) {

		console.log(ExResourceService);
		this.refs = { scope: $scope, exResourceService: ExResourceService, fileReaderService: FileReaderService };
		//		this.refs = {scope:$scope, driveResourceService: DriveResourceService};
		$scope.vm = this;

		$scope.online = true;
	}

//	/**
//	 * takes a list of parents and creates a JSON object in focussedItem.parents
//	 * called onchange of the form field
//	 */
//	parentsListToJson(list?: string) {
//		if (!list) {
//			list = this.parentsList;
//		}
//		var listArray = list.replace(/ /g, "").split(",");
//		this.focussedItem.parents = [];
//		for (var i = 0; i < listArray.length; i++) {
//			this.focussedItem.parents.push({ id: listArray[i] });
//		}
//	}
//	/**
//	 * takes a the JSON object in focussedItem.parents and strips it to a comma separated list in parentsList
//	 */
//	parentsJsonToList() {
//		this.parentsList = "";
//		for (var i = 0; i < this.focussedItem.parents.length; i++) {  		// foreach parent
//			this.parentsList += ", " + this.focussedItem.parents[i].id;  	// append to string
//		}
//		this.parentsList = this.parentsList.substr(1);  // lose the first comma
//	}

	/**
	 * uses gapi to authorise the app using the scope provided by the input field
	 * upon receipt of a token, passes it into driveResource to stuff into a header
	 */
	doAuth() {
		var scope = "https://www.googleapis.com/auth/drive"
		scope = document.getElementById('scope').getAttribute('value');
		console.log("authing " + this.clientId + " " + this.drivescope);
		gapi.auth.authorize({
			client_id: this.clientId,
			scope: [this.drivescope],
			immediate: false
		}, () => {
				console.log("authed " + gapi.auth.getToken().access_token);
				this.refs.exResourceService.fixOauth();
				this.haveToken = true;
				this.refs.scope.$apply(() => { this.haveToken = true });
			});
	}

	/**
	 * use the q input to generate and run a files.list
	 */
	doList() {
		this.driveActive = true;
		this.driveError = false;
		console.log("doList with q=" + this.q);
		this.refs.exResourceService
			.getAllSettings(this.listResults, this.q, this.maxResults, undefined, undefined)
			.then(() => { this.driveActive = false; console.log('fetched ' + this.listResults.length + ' items') }, () => { this.driveError = true });
	}
	/**
	 * called when a file is selected from the list 
	 */
	doSelect(f) {
		this.focussedItem = f;				// store in focussed Item (form)
//		this.parentsJsonToList();			// unpack parents
	}
	/**
	 * clears down the array of list results
	 */
	doClearResults() {
		this.listResults = [];
	}
	/**
	 * called to update the current file 
	 */
	doUpdate() {
		this.driveError = false;
		this.driveActive = true;
		this.refs.exResourceService.getSettingsResource()
			.update({ 'id': this.focussedItem.orgnId }, this.focussedItem)
			.$promise.then(() => { this.driveActive = false }, () => { this.driveError = true });
	}
	/**
	 * called to inserta new file (NB no media content)
	 */
	doInsert() {
		this.driveError = false;
		this.driveActive = true;
		this.focussedItem.orgnId = undefined;
		this.refs.exResourceService.getSettingsResource()
			.insert({}, this.focussedItem)
			.$promise.then(() => { this.driveActive = false }, () => { this.driveError = true });
	}
	/**
	 * called to delete a file (NB, not trash)
	 */
	doDelete() {
		this.driveError = false;
		this.driveActive = true;
		this.refs.exResourceService.getSettingsResource()
			.remove({ 'id': this.focussedItem.orgnId }, {})
			.$promise.then(() => { this.driveActive = false }, () => { this.driveError = true });
	}


	/** 
 	* called by onchange of the file input element
 	* does a multipart POST or PUT depending on whether there is an id in focussedItem
 	* 
 	*/
//	fileOnChange(fileInput, scope) {
//		// get the file's contents and a promise
//		if (!this.focussedItem || !this.focussedItem.currency) {
//			alert("You must specify a mime type");
//			document.getElementById("fileForm")['reset']();
//			return;
//		}
//		var p = this.refs.fileReaderService.readAsDataURL(fileInput, scope);
//		document.getElementById("fileForm")['reset']();
//		p.then((data) => { /* when the file's contents have been read ... */
//			//			console.log(data);
//			// insert the file, which returns a dfile object with a promise
//			data = data.replace(/.*base64,/i, "");
//			this.focussedItem = this.refs.exResourceService.insertSettings(this.focussedItem, data);
//			this.focussedItem['$promise'].then((dfile) => {
//				console.log("file uploaded "+this.focussedItem.id);
//			});
//		});
//	}
}