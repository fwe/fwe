'use strict';

describe('Service: SmarttableService', function () {

  // load the service's module
  beforeEach(module('exApp'));

  // instantiate service
  var SmarttableService;
  beforeEach(inject(function (_SmarttableService_) {
    SmarttableService = _SmarttableService_;
  }));

  it('should do something', function () {
    expect(!!SmarttableService).toBe(true);
  });

});
