'use strict';
window['clexunittesting']= true;  // suppress the usual fetching at startup

describe('Service: ModelService', function () {

  // load the service's module
  beforeEach(module('exApp'));

  // instantiate service
  var ModelService;
  window['clexunittesting']= true;  // suppress the usual fetching at startup

  beforeEach(inject(function (_ModelService_) {
    ModelService = _ModelService_;
  }));

  it('should do something', function () {
    expect(!!ModelService).toBe(true);
  });

});
