'use strict';

describe('Service: DriveService', function () {

  // load the service's module
  beforeEach(module('exApp'));

  // instantiate service
  var DriveService;
  beforeEach(inject(function (_DriveService_) {
    DriveService = _DriveService_;
  }));

  it('should do something', function () {
    expect(!!DriveService).toBe(true);
  });

});
