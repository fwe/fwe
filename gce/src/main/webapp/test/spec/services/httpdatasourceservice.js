'use strict';

describe('Service: HttpDatasourceService', function () {

  // load the service's moduleHttpDatasourceService
  beforeEach(module('exApp'));

  // instantiate service
  var HttpDatasourceService;
  window['clexunittesting']= true;  // suppress the usual fetching at startup
  beforeEach(inject(function (_HttpDatasourceService_) {
  	HttpDatasourceService = _HttpDatasourceService_;
  }));

  it('should do something', function () {
    expect(!!HttpDatasourceService).toBe(true);
  });

});
