'use strict';

describe('Service: DummyDatasourceService', function () {

  // load the service's module
  beforeEach(module('exApp'));

  // instantiate service
  var DummyDatasourceService;
  beforeEach(inject(function (_DummyDatasourceService_) {
    DummyDatasourceService = _DummyDatasourceService_;
  }));

  it('should do something', function () {
    expect(!!DummyDatasourceService).toBe(true);
  });

});
