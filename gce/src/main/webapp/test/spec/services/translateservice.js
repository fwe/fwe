'use strict';

describe('Service: TranslateService', function () {

  // load the service's module
  beforeEach(module('exApp'));

  // instantiate service
  var TranslateService;
  beforeEach(inject(function (_TranslateService_) {
    TranslateService = _TranslateService_;
  }));

  it('should do something', function () {
    expect(!!TranslateService).toBe(true);
  });

  it('setlang should set fr', function () {
  	TranslateService.setLang('fr');
    expect(TranslateService.lang).toBe("fr");
  });

  it('Go should be Aller', function () {
  	TranslateService.setLang('fr');
    expect(TranslateService.getString('go')).toBe("Aller");
  });

});
