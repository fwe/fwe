'use strict';

describe('Controller: MainmCtrl', function () {

  // load the controller's module
  window['clexunittesting']= true;  // suppress the usual fetching at startup

  var MainmCtrl,
    scope;
  var $scope, $location, $rootScope, modelService, translateService, httpDatasourceService, createController, driveService, storageService;
  // Initialize the controller and a mock scope
//  beforeEach(inject(function ($controller, $scope, $rootScope, $window, $location, TranslateService, ModelService, HttpDatasourceService,DriveService, StorageService) {
  beforeEach(inject(function ($controller, $rootScope,$location) {
  window['clexunittesting']= true;  // suppress the usual fetching at startup
    scope = $rootScope.$new();
    // mocks
    translateService = {setLang:function (){},getString:function (){}};
    modelService = {};
    httpDatasourceService ={fetchRecentCalendarEvents:function (){}};
    driveService={};
    window['config']={settings:{}};
    storageService={};
    
    
    MainmCtrl = $controller('MainmCtrl', {
      $scope: scope,
      TranslateService: translateService,
      ModelService: modelService,
      HttpDatasourceService: httpDatasourceService,
      DriveService: driveService,
      StorageService: storageService
    });
  }));

  it('should have fingerprint MainmCtrl', function () {
    expect(scope.vm.fingerprint).toBe('MainmCtrl');
  });
});
